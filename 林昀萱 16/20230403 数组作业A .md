## 编码题

**1.定义一个含有五个元素的数组,并为每个元素赋值,求数组中所有元素的最小值**

**操作步骤：**

​	1.定义5个元素数组

​	2.可以使用初始化数组的两种方式之一为数组元素赋值

​	3.遍历数组求数组中的最小值

```java
     int[] num={5,2,3,1,6};
        int min=num[0];
        for (int i = 1; i < num.length; i++) {
            if(min>num[i]){
                min=num[i];
            }
        }
        System.out.println("最小值"+min);
```



**2．需求：求出数组中索引与索引对应的元素都是奇数的元素**

**分析：**

​	1、遍历数组

​	2、判断索引是否是奇数（索引 % 2 != 0）

​	3、判断索引对应的元素是否是奇数(arr[索引] % 2 != 0)

​	4、满足条件输出结果

```java
        int[] num={5,2,3,1,6};
        for (int i = 1; i < num.length; i++) {
            if(i%2!=0 && num[i]%2!=0){
                System.out.println(num[i]);
            }
        }
```



**3.按要求在main方法中完成以下功能：**

​	a.   定义一个长度为5的int型数组arr，提示用户输入5个1-60之间的数字作为数组元素

​	b.  生成2-10（范围包含2和10）之间的随机数num

​	c.   遍历数组arr,筛选出数组中不是num倍数的元素并输出

​	**PS：输入的数组元素范围包括1和60，不需要代码判断**

```java
int[] arr = new int[5];
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入五个1到60间的数字");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.nextInt();
        }
        Random r = new Random();
        int num = r.nextInt(9)+2;
        System.out.println("num的值"+num);
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]%num!=0){
                System.out.println(arr[i]);
            }
        }
```

**4.有一个数组int[] arr = {9,1,3,4,54,56,23,22,20,43,45,78};,要求打印数组中能被6整除的元素。**

```java
int[] arr = {9,1,3,4,54,56,23,22,20,43,45,78};
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]%6==0){
                System.out.println(arr[i]);
            }
        }
```

**5.定义一个长度为20的数组，元素为20-40的随机数，要求判断指定元素在数组中出现的次**数，指定元素为键盘录入范围为20-40之间。



```java
        int[] arr = new int[20];
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入你要查看20到40哪个数字出现的次数");
        int a = sc.nextInt();
        for (int i = 0; i < arr.length; i++) {
            arr[i]=r.nextInt(21)+20;;
            System.out.print(arr[i]+" ");
        }
        int c=0;
        for (int i = 0; i < arr.length; i++) {
            if (a==arr[i]){
                c+=1;
            }
        }
        System.out.println();
        System.out.println(a+"这个数字出现"+c+"次");
```

