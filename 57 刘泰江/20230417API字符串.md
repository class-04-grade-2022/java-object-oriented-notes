

```java
import java.util.Scanner;

public class Login {
    public static void main(String[] args) {
        String name = "张三";
        String password= "123456";
        for (int i = 0; i < 3; i++) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入你的用户名：");
        String inputName = sc.next();
        System.out.println("请输入你的密码：");
        String inputPassword = sc.next();
            if (name.equals(inputName) && password.equals(inputPassword)){
                System.out.println("登录成功");
                break;
            }else{
                if ((3-i)==0){
                    System.out.println("用户名或密码错误！请重试");
                }else{
                    System.out.println("还剩"+(2-i)+"次机会");
                }
            }
        }
    }
}

```

```java
需求：键盘录入一个字符串，使用程序实现在控制台遍历该字符串
思路：
键盘录入一个字符串，用 Scanner 实现
遍历字符串，首先要能够获取到字符串中的每一个字符
 public char charAt(int index)：返回指定索引处的char值，字符串的索引也是从0开始的
遍历字符串，其次要能够获取到字符串的长度
 public int length()：返回此字符串的长度
 数组的长度：数组名.length
 字符串的长度：字符串对象.length()
遍历
for(int i=0; i<s.length(); i++) {
  s.charAt(i);  // 就是指定索引处的字符值
}
```

```java
import java.util.Arrays;
import java.util.Scanner;

public class Char {
    public static void main(String[] args) {
        Scanner s1 = new Scanner(System.in);
        System.out.println("请输入字符串");
        String arr = s1.next();
        char[] chars = new char[arr.length()];
        for(int i=0; i<arr.length(); i++) {
            chars[i]=arr.charAt(i);
        }
        System.out.println(Arrays.toString(chars));

    }
}

```



```java
需求：键盘录入一个字符串，使用程序实现在控制台遍历该字符串
思路：
键盘录入一个字符串，用 Scanner 实现
将字符串拆分为字符数组
 public char[] toCharArray( )：将当前字符串拆分为字符数组并返回
遍历字符数组
```

```java
import java.util.Scanner;

public class Char2 {
public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("输入一个字符串");
    String a = sc.next();
    for (int i = 0; i < a.length(); i++) {
        System.out.println(a.charAt(i));
    }

}

}

```

//        需求：已知用户名和密码，请用程序实现模拟用户登录。总共给三次机会，登录之后，给出相应的提示
//        思路：
//        1、已知用户名和密码，定义两个字符串表示即可
//        2、键盘录入要登录的用户名和密码，用 Scanner 实现
//        3、拿键盘录入的用户名、密码和已知的用户名、密码进行比较，给出相应的提示。字符串的内容比较，用equals() 方法实现
//        4、用循环实现多次机会，这里的次数明确，采用for循环实现，并在登录成功的时候，使用break结束循环

```java
import java.util.Scanner;

public class Login {
    public static void main(String[] args) {
        String name = "张三";
        String password= "123456";
        for (int i = 0; i < 3; i++) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入你的用户名：");
        String inputName = sc.next();
        System.out.println("请输入你的密码：");
        String inputPassword = sc.next();
            if (name.equals(inputName) && password.equals(inputPassword)){
                System.out.println("登录成功");
                break;
            }else{
                if ((3-i)==0){
                    System.out.println("用户名或密码错误！请重试");
                }else{
                    System.out.println("还剩"+(2-i)+"次机会");
                }
            }
        }
    }
}
```



```java
需求：键盘录入一个字符串，统计该字符串中大写字母字符，小写字母字符，数字字符出现的次数(不考虑其他字符)
思路：
键盘录入一个字符串，用 Scanner 实现
要统计三种类型的字符个数，需定义三个统计变量，初始值都为0
遍历字符串，得到每一个字符
判断该字符属于哪种类型，然后对应类型的统计变量+1
假如ch是一个字符，我要判断它属于大写字母，小写字母，还是数字，直接判断该字符是否在对应的范围即可
大写字母：ch>='A' && ch<='Z'	
小写字母： ch>='a' && ch<='z'
数字： ch>='0' && ch<='9'
输出三种类型的字符个数
```

```java
import java.util.Scanner;

public class T4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个字符串");
        String a = sc.next();
        int big = 0;
        int small = 0;
        int num = 0;
        for (int i = 0; i < a.length(); i++) {
            char ch= a.charAt(i);
            if (ch>='A' && ch<='Z'){
                big++;
            } else if (ch>='a' && ch<='z') {
                small++;
            } else if (ch>='0' && ch<='9') {
                num++;
            }
        }
        System.out.println("大写字母："+big+"个" +" 小写字母："+small+"个"+" 数字："+num+"个");

    }
}

```

```java
需求：以字符串的形式从键盘接受一个手机号，将中间四位号码屏蔽
最终效果为：156****1234
思路：
键盘录入一个字符串，用 Scanner 实现
截取字符串前三位
String substring(int beginIndex, int endIndex) ：从beginIndex索引位置开始截取，截取到endIndex索引位置，得到新字符串并返回（包含头，不包含尾）
String s1 = telString.substring(0,3);
截取字符串后四位
String substring(int beginIndex) : 从传入的索引位置处，向后截取，一直截取到末尾，得到新的字符串并返回
String s2 = telString.substring(7);
将截取后的两个字符串，中间加上****进行拼接，输出结果
```

```java
import java.util.Scanner;

public class T5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入手机号");
        String a = sc.next();
        String s1 = a.substring(0,3);
        String s2 = a.substring(7);
        System.out.println(s1+"****"+s2);
    }
}

```

![敏感词替换](E:\java2023上\2023-04-17\20230417题目\敏感词替换.png)

```java
需求：键盘录入一个 字符串，如果字符串中包含（TMD），则使用***替换
思路：
键盘录入一个字符串，用 Scanner 实现
替换敏感词
String replace(CharSequence target, CharSequence replacement) 
将当前字符串中的target (被替换的旧值 )内容，使用replacement (替换的新值) 进行替换
返回新的字符串
输出结果
```

```java
import java.util.Scanner;

public class T6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个字符串");
        String a = sc.next();
        System.out.println(a.replace("TMD","***"));
    }
}
```

![切割字符串](E:\java2023上\2023-04-17\20230417题目\切割字符串.png)

```java
需求：以字符串的形式从键盘录入学生信息，例如：“张三 , 23”从该字符串中切割出有效数据
封装为Student学生对象
思路：
编写Student类，用于封装数据
键盘录入一个字符串，用 Scanner 实现
根据逗号切割字符串，得到（张三）（23）
String[] split(String regex) ：根据传入的字符串作为规则进行切割，将切割后的内容存入字符串数组中，并将字符串数组返回
从得到的字符串数组中取出元素内容，通过Student类的有参构造方法封装为对象
调用对象getXxx方法，取出数据并打印。
```

```java
import java.util.Arrays;
import java.util.Scanner;

public class Student {

    private String[]arr;

    public Student(){
    }

    public Student(String[] arr) {
        this.arr = arr;
    }

    public String[] getArr() {
        return arr;
    }

    public void setArr(String[] arr) {
        this.arr = arr;
    }

    @Override
    public String toString() {
        return "Student{" +
                "arr=" + Arrays.toString(arr) +
                '}';
    }

}

```

```java
import java.util.Arrays;
import java.util.Scanner;

public class cs {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String a = sc.next();
        String[] arr=a.split(",");
        Student b= new Student();
        System.out.println(Arrays.toString(b.getArr()));
    }
}

```