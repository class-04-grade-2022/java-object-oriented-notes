~~~java
```java
public class UIteacher extends Employee implements Draw{
    public void ui(){
        System.out.println("UIteacher: "+"  name:"+getName()+"  gender:"+getGender()+"  age:"+getAge());
    }

    public UIteacher() {
    }

    public UIteacher(String name, String gender, int age) {
        super(name, gender, age);
    }

    void work(){
        System.out.println(getName()+"讲UI设计课");
    }
    void drawing(){
        System.out.println(getName()+"画画");
    }

}
```

javateacher类

```java
public class Jteacher extends Employee {
    public void jt(){
        System.out.println("javateacher: "+"name:"+getName()+"  gender:  "+getGender()+"  age:"+getAge());
    }

    public Jteacher() {

    }
    public Jteacher(String name, String gender, int age) {
        super(name, gender, age);
    }

    void work() {
        System.out.println(getName()+"讲java课");
    }
}

```

接口Draw

```java
public interface Draw{
    public static void darwing(){

    }
}
```

Employee类

```java
public abstract class  Employee {
    private String name;
    private String gender;
    private int age;

    public Employee() {

    }
    public Employee(String name, String gender, int age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    abstract void work();
    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                '}';
    }
}

```

测试

```java
public class Test {
    public static void main(String[] args) {
        Jteacher jt=new Jteacher("小明","男",25);
        jt.jt();
        jt.work();
        UIteacher ui=new UIteacher("小红","女",18);
        ui.ui();
        ui.work();
        ui.drawing();
    }
}
```
~~~

