~~~Java
```java

public class ElectronicInformationEngineering {
    private String brand;
    private String model;
    private String color;
    private double price;

    public ElectronicInformationEngineering(String brand, String model, String color, double price) {
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}

```

子类：冰箱类
属性：品牌、型号、颜色、售价、门款式、制冷方式

```java
package test0424;

public class Refrigerator extends ElectronicInformationEngineering {
private String Door;
private String cooling;

    public Refrigerator(String brand, String model, String color, double price, String door, String cooling) {
        super(brand, model, color, price);
        Door = door;
        this.cooling = cooling;
    }

    public String getDoor() {
        return Door;
    }

    public void setDoor(String door) {
        Door = door;
    }

    public String getCooling() {
        return cooling;
    }

    public void setCooling(String cooling) {
        this.cooling = cooling;
    }
}
```

子类：洗衣机类
属性：品牌、型号、颜色、售价、电机类型、洗涤容量

```java

public class WashingMachine extends ElectronicInformationEngineering {
    private String motorType;
    private double washingCapacity;

    public WashingMachine(String brand, String model, String color, double price, String motorType, double washingCapacity) {
        super(brand, model, color, price);
        this.motorType = motorType;
        this.washingCapacity = washingCapacity;
    }

    public String getMotorType() {
        return motorType;
    }

    public void setMotorType(String motorType) {
        this.motorType = motorType;
    }

    public double getWashingCapacity() {
        return washingCapacity;
    }

    public void setWashingCapacity(double washingCapacity) {
        this.washingCapacity = washingCapacity;
    }
}
```

子类：电视类
属性：品牌、型号、颜色、售价、屏幕尺寸、分辨率

```java

public class Television extends ElectronicInformationEngineering{
    private double screenSize;
    private int resolution;

    public Television(String brand, String model, String color, double price, double screenSize, int resolution) {
        super(brand, model, color, price);
        this.screenSize = screenSize;
        this.resolution = resolution;
    }

    public double getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(double screenSize) {
        this.screenSize = screenSize;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }
}
```

## 题目2

我们计划为一个动物园制作一套信息管理系统，根据与甲方沟通，我们归纳了有以下几种动物需要记录到系统中：

```java
鸟类：		 鹦鹉、猫头鹰、喜鹊
哺乳类：	大象、狼、长颈鹿
爬行类：	鳄鱼、蛇、乌龟
```



**要求：**

请根据以上需求，使用“继承”设计出三层的类结构

```java
|--动物类
    |--鸟类：
        |--鹦鹉类
        |--猫头鹰类
        |--喜鹊类
  
    |--哺乳类：
        |--大象类
        |--狼类
        |--长颈鹿类
  
    |--爬行类：
        |--鳄鱼类
        |--蛇类
        |--乌龟类
```

作为父类的类都应该定义一些共性内容，每种子类也都有一些特定的内容。 重点是类的层次结构，类成员简单表示即可。



**答案：**

父类：动物园

```java

public class Zoo {
    private String name;
    private String age;

    public Zoo(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}

```

二级子类： |--鸟类：

```java

public class Bird extends Zoo{
    public Bird(String name, String age) {
        super(name, age);
    }
}
```

|--鹦鹉类

```java


public class Parrot extends Bird{
    public Parrot(String name, String age) {
        super(name, age);
    }
}
```

 |--猫头鹰类

```java

public class Owl extends Bird{
    public Owl(String name, String age) {
        super(name, age);
    }
}
```

 |--喜鹊类

```java


public class Magpie extends Bird{
    public Magpie(String name, String age) {
        super(name, age);
    }
}
```

二级子类 |--哺乳类：

```java


public class Lactation extends Zoo{
    public Lactation(String name, String age) {
        super(name, age);
    }
}
```

  |--大象类

```java


public class Elephant extends Lactation{
    public Elephant(String name, String age) {
        super(name, age);
    }
}
```

|--狼类

```java


public class Wolf extends Lactation{
    public Wolf(String name, String age) {
        super(name, age);
    }
}
```

|--长颈鹿类

```java

public class Giraffe extends Lactation{
    public Giraffe(String name, String age) {
        super(name, age);
    }
}
```

 二级子类|--爬行类：

```java


public class Crawl extends Zoo{
    public Crawl(String name, String age) {
        super(name, age);
    }
}
```

|--鳄鱼类

```java


public class Crocodile extends Crawl{
    public Crocodile(String name, String age) {
        super(name, age);
    }
}
```

 |--蛇类

```java


public class Snake extends Crawl{
    public Snake(String name, String age) {
        super(name, age);
    }
}
```

   |--乌龟类

```java


public class Tortoise extends Crawl{
    public Tortoise(String name, String age) {
        super(name, age);
    }
}
~~~

