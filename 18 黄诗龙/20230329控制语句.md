```java
//  % 取余 获取的是两个数据做除法的余数
//  +符号做连接符 能算则算，不能算就在一起
//  ++ 自增 变量自身的值加1； -- 自减 变量自身的值减1；
//  ++、-- 如果不是单独使用（如在表达式中，或同时有其他操作），放在变量前后会存在明显区别
//  放在变量前面，先对变量进行+1、-1，再拿变量进行运算
			int a = 10;
			int rs = ++a; //rs=11,a=11
//  放在变量后面，先拿变量的值进行运算，再对变量的值进行+1、-1,
			int b =10;
			int rs = b++;//rs=10,b=11
		关系运算符
//  == a==b，判断a和b的值是否相等，成立为true，不成立为false
//  != a!=b，判断a和b的值是否不相等，成立为true，不成立为false
//   >    a>b，  判断a是否大于b，成立为true，不成立为false
//   >=   a>=b，判断a是否大于等于b，成立为true，不成立为false
//   <    a<b，  判断a是否小于b，成立为true，不成立为false
//   <=   a<=b，判断a是否小于等于b，成立为true，不成立为false

作业
package zy;

public class cllly {
    public static void main(String[] args) {
//# 巩固题
//
//## 1、判断5的倍数
//
//        从键盘输入一个整数，判断它是否是5的倍数
        Scanner clyyy= new Scanner(System.in);
        System.out.println("输入一个整数");
        int a=clyyy.nextInt();
        if (a%5==0)
            System.out.println("是5的倍数");
        else
            System.out.println("不是5的倍数");
        
//
//## 2、判断字符类型
//
//        从键盘输入一个字符，判断它是字母还是数字，还是其他字符
//
         Scanner ccyy= new Scanner(System.in);
        System.out.println("输入一个字符");
        char b=ccyy.next().charAt(0);
        if(b>='0' && b<='9'){
            System.out.println(b+"是数字");
        }else if(b>='a' && b<='z'){
            System.out.println(b+"是字母");
        }else{
            System.out.println(b+"是其他字符");
        } 
//## 3、计算折扣后金额
//
//        从键盘输入订单总价格totalPrice（总价格必须>=0），根据优惠政策计算打折后的总价格。
//
//        编写步骤：
//
//        1. 判断当`totalPrice >=500` ,discount赋值为0.8
//        2. 判断当`totalPrice >=400` 且`<500`时,discount赋值为0.85
//        3. 判断当`totalPrice >=300` 且`<400`时,discount赋值为0.9
//        4. 判断当`totalPrice >=200` 且`<300`时,discount赋值为0.95
//        5. 判断当`totalPrice >=0` 且`<200`时,不打折，即discount赋值为1
//        6. 判断当`totalPrice<0`时，显示输入有误
//        7. 输出结果
//
           Scanner cll=new Scanner(System.in);
        System.out.println("输入订单总价格");
        int totalPrice=cll.nextInt();
        if (totalPrice>=500){
            System.out.println("折扣后总价为："+totalPrice*0.8);
        } else if (totalPrice >=400 && totalPrice<500) {
            System.out.println("折扣后总价为："+totalPrice*0.85);
        } else if (totalPrice >=300 && totalPrice<400) {
            System.out.println("折扣后总价为："+totalPrice*0.9);
        } else if (totalPrice >=200 && totalPrice<300) {
            System.out.println("折扣后总价为："+totalPrice*0.95);
        } else if (totalPrice >=0 && totalPrice<200) {
            System.out.println("折扣后总价为："+totalPrice*1);
        }else
            System.out.println("输入有误");
        
        
//## 4、输出月份对应的英语单词
//
//        从键盘输入月份值，输出对应的英语单词
//
                  Scanner ly=new Scanner(System.in);
        System.out.println("输入月份值");
        int month=ly.nextInt();
        if (month==1) {
            System.out.println("January");
        } else if (month==2) {
            System.out.println("February");
        }else if (month==3)  {
            System.out.println("March");
        }else if (month==4) {
            System.out.println("April");
        } else if (month==5) {
            System.out.println("May");
        } else if (month==6) {
            System.out.println("June");
        } else if (month==7) {
            System.out.println("July");
        } else if (month==8) {
            System.out.println("August");
        } else if (month==9) {
            System.out.println("September");
        } else if (month==10) {
            System.out.println("October");
        } else if (month==11) {
            System.out.println("November");
        } else if (month==12) {
            System.out.println("December");
        }else{
            System.out.println("输入有误");
        }
//## 5、计算今天是星期几
//
//        定义变量week赋值为上一年12月31日的星期值（可以通过查询日历获取），定义变量year、month、day，分别赋值今天日期年、月、日值。计算今天是星期几。
//
		
//# 拔高题
//
//## 1、判断年、月、日是否合法
//
//        从键盘输入年、月、日，要求年份必须是正整数，月份范围是[1,12]，日期也必须在本月总天数范围内，如果输入正确，输出“年-月-日”结果，否则提示输入错误。
//
            
//## 2、判断打鱼还是晒网
//
//        从键盘输入年、月、日，假设从这一年的1月1日开始执行三天打鱼两天晒网，那么你输入的这一天是在打鱼还是晒网。
//

//## 3、判断星座
//
//        声明变量month和day，用来存储出生的月份和日期，判断属于什么星座，各个星座的日期范围如下：
//
//        ![1558000604568](03_Java流程控制语句结构作业图片/1558000604568.png)
//
            
//# 简答题
//
//        1、switch是否能作用在byte上，是否能作用在long上，是否能作用在String上？


    }

}
```

