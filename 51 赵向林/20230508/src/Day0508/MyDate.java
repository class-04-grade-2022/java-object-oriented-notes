package Day0508;

public class MyDate {
    private int year;//年
    private int month;//月
    private int day;//日
    public String getInfo(){
        return year+"年"+month+"月"+day+"日";
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
