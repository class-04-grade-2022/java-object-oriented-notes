package Day0508;

public class SalaryEmployee extends Employee{
    private double pay;//薪资
    private MyDate birth;//生日
    public SalaryEmployee(double pay, MyDate birth) {
        this.pay = pay;
        this.birth = birth;
    }

    public SalaryEmployee() {
    }

    public double getPay() {
        return pay;
    }

    public void setPay(double pay) {
        this.pay = pay;
    }

    public MyDate getBirth() {
        return birth;
    }

    public void setBirth(MyDate birth) {
        this.birth = birth;
    }

    @Override
    public double earning() {
        return pay=earning();
    }

    @Override
    public String getInfo() {
        return getName()+"姓名"+earning()+"实发工资"+birth+"生日";
    }
}
