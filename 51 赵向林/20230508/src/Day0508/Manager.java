package Day0508;

public class Manager extends SalaryEmployee{
    private double Bonus;//奖金比例

    public double getBonus() {
        return Bonus;
    }

    public void setBonus(double bonus) {
        Bonus = bonus;
    }

    @Override
    public double earning() {
        return (1+Bonus)*getPay();
    }

    @Override
    public String getInfo() {
        return getName()+"姓名"+earning()+"实发工资"+getBirth()+"生日"+Bonus+"奖金比例";
    }
}
