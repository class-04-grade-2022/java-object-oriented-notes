package Day0508;

public class HourEmployee extends Employee{
    double Working;//工作小时数
    double hours;//每小时多少钱

    @Override
    public double earning() {
        return hours*Working;
    }

    @Override
    public String getInfo() {
        return getName()+"姓名"+earning()+"实发工资"+hours+"时薪"+Working+"工作小时数";
    }
}
