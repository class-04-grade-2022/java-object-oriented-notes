package Day0426;

public class Durian extends Botany{
    //榴莲：颜色；重量；可以吃(){榴莲酥}；可以使用(){砸人}
    @Override
    public void use() {
        System.out.println("我用榴莲砸闪电");
    }

    @Override
    public void eat() {
        System.out.println("榴莲酥");
    }
}
