package Day0426;

public class Test {
    public static void main(String[] args) {
        Cucumber c=new Cucumber();
        c.setColor("绿色");
        c.setWeight(250);
        System.out.println(c.getColor()+","+c.getWeight());
        c.eat();
        c.use();
        System.out.println("==========");
        Eggplant e=new Eggplant();
        e.setColor("紫色");
        e.setWeight(666);
        System.out.println(e.getColor()+","+e.getWeight());
        e.eat();
        e.use();
        System.out.println("==========");
        Banana b=new Banana();
        b.setColor("黄色");
        b.setWeight(1314);
        System.out.println(b.getColor()+","+b.getWeight());
        b.eat();
        b.use();
        System.out.println("==========");
        Durian d=new Durian();
        d.setColor("黄色");
        d.setWeight(520);
        System.out.println(d.getColor()+","+d.getWeight());
        d.eat();
        d.use();
        System.out.println("==========");

    }
}
