package Day0426;

public abstract class Botany extends Food {
    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public abstract void use();
}
