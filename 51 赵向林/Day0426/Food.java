package Day0426;

public abstract class Food {
    //创建类描述黄瓜、茄子、香蕉、榴莲；它们各自拥有的属性和功能如下：（后面带小括号的是功能）
    //
    //黄瓜：颜色；重量；可以吃(){黄瓜炒蛋}；可以使用(){做面膜}
    //
    //茄子：颜色；重量；可以吃(){油焖茄子}；可以使用(){做中药}
    //
    //香蕉：颜色；重量；可以吃(){脆皮香蕉}；可以使用(){做香蕉面膜}
    //
    //榴莲：颜色；重量；可以吃(){榴莲酥}；可以使用(){砸人}
    //
    //请用继承的思想设计设计以上类型。
    private double weight;
    public abstract void eat();

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
