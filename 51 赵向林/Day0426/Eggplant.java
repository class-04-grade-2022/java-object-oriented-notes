package Day0426;

public class Eggplant extends Botany {
    //茄子：颜色；重量；可以吃(){油焖茄子}；可以使用(){做中药}

    @Override
    public void use() {
        System.out.println("闪电肾虚，用茄子做中药");
    }

    @Override
    public void eat() {
        System.out.println("油焖茄子");
    }
}
