```java
public class d1 {
    private String num;
    private String name;
    private int age;
    private double money;

    public void setNum(String num){
       this.num=num;
    }
    public String getNum(){
        return num;
    }
    public  void setname(String name){
        this.name=name;
    }
    public String getname(){
        return name;
    }
    public void setAge(int age){
        this.age=age;
    }
    public int getAge(){
        return age;
    }
    public void setmoney(double money){
        this.money=money;
    }
    public double getMoney(){
        return money;
    }
    public d1(String num,String name,int age,double money){
        this.num=num;
        this.name=name;
        this.age=age;
        this.money=money;
    }
    public String toString(){
        return "编号"+num+"代号"+name+",年龄"+age+",工资"+money;
    }
}
```

```java
public class d2 {
    public static void main(String[] args) {
        d1 a=new d1("001","李逵",50,10000);
        d1 b =new d1("002","李红",33,20000);
        d1 c =new d1("003","力宏",22,20000);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }

}
```
