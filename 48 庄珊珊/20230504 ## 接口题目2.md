## 接口题目2

1. 请定义“员工(类)”：
   属性：姓名、性别、年龄(全部私有)
   行为：工作(抽象)
   无参、全参构造方法
   get/set方法

2. 请定义“绘画(接口)”
   抽象方法：绘画
3. 请定义“Java讲师类”继承自“员工类”
4. 请定义”UI讲师类”，继承自“员工类”，并实现“绘画”接口。

**要求**：

1. 请按上述要求设计出类结构，并实现相关的方法。
2. 测试类中创建对象测试，属性可自定义：
   - 创建一个Java讲师类对象，调用工作的方法。
   - 创建一个UI讲师类对象，调用工作方法，和绘画方法。

**答案：**

```java
员工类
    package test0504;

public class Employee {
    private String name;
    private String sex;
    private int age;

    public Employee() {
    }

    public Employee(String name, String sex, int age) {
        this.name = name;
        this.sex = sex;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void work() {
        System.out.println(name + "在工作");
    }

    public interface Draw {
        abstract void draw();
    }

    static class javateacher extends Employee {
        public javateacher() {
        }

        public javateacher(String name, String sex, int age) {
            super(name, sex, age);
        }

        @Override
        public String toString() {
            return getName() + "讲java课";
        }
    }

    static class UIteacher extends Employee implements Draw {
        public UIteacher() {
        }

        public UIteacher(String name, String sex, int age) {
            super(name, sex, age);
        }

        @Override
        public String toString() {
            return getName() + "讲UI设计课";
        }

        @Override
        public void draw() {
            System.out.println(getName() + "在画画");
        }
    }
}

```

test类

```java
package test0504;

public class test1 extends Employee {
    public static void main(String[] args) {
        javateacher javateacher = new javateacher("小明", "男", 25);
        System.out.println(javateacher);
        UIteacher uIteacher = new UIteacher("小红", "女", 18);
        System.out.println(uIteacher);
        uIteacher.draw();
    }
}
```

**运行结果：**

```java
JavaTeacher{name='小明', gender='男', age=25}
小明讲Java课
UITeacher{name='小红', gender='女', age=18}
小红讲UI设计课
小红画画
```



## ppt题目

图中有四学生，合理使用类和接口表示。
学习男同学：姓名，年龄，学习
学习女同学：姓名，年龄，学习
男篮同学：姓名，年龄，学习，打篮球
女篮同学：姓名，年龄，学习，打篮球
男同学不爱学习 , 女同学爱学习

```java
package test0504;

/*
图中有四学生，合理使用类和接口表示。
学习男同学：姓名，年龄，学习
学习女同学：姓名，年龄，学习
男篮同学：姓名，年龄，学习，打篮球
女篮同学：姓名，年龄，学习，打篮球
男同学不爱学习 , 女同学爱学习
 */
class People {
    String name = "";
    int age = 0;

    public People(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

interface study {
    void study();
}

interface baskball {
    void playbaskball();
}

class boy extends People implements study {
    public boy(String name, int age) {
        super(name, age);
    }

    @Override
    public void study() {
        System.out.println("男同学" + name + "不爱学习");
    }
}

class girl extends People implements study {
    public girl(String name, int age) {
        super(name, age);
    }

    @Override
    public void study() {
        System.out.println("女同学" + name + "喜欢学习");
    }
}


class baskballgirl extends girl implements baskball {
    public baskballgirl(String name, int age) {
        super(name, age);
    }

    @Override
    public void playbaskball() {
        System.out.println("女同学" + name + "正在打篮球");
    }
}

class baskballboy extends boy implements baskball {
    public baskballboy(String name, int age) {
        super(name, age);
    }

    @Override
    public void playbaskball() {
        System.out.println("男同学" + name + "正在打篮球");
    }
}
```

Test类

```java
package test0504;

public abstract class test extends People {
    public test(String name, int age) {
        super(name, age);
    }

    public static void main(String[] args) {
        boy boy = new boy("散兵", 22);
        boy.study();

        girl girl = new girl("纳西妲", 18);
        girl.study();

        baskballboy ikun = new baskballboy("ikun", 66);
        ikun.study();
        ikun.playbaskball();

        baskballgirl kun = new baskballgirl("坤坤", 24);
        kun.study();
        kun.playbaskball();
    }
}
```