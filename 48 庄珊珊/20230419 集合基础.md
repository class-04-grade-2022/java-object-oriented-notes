# 第一题**案例：存储字符串并遍历**

需求：创建一个存储字符串的集合，存储3个字符串元素，使用程序实现在控制台遍历该集合

思路：

①创建集合对象

②往集合中添加字符串对象

③遍历集合，首先要能够获取到集合中的每一个元素，这个通过get(int index)方法实现

④遍历集合，其次要能够获取到集合的长度，这个通过size()方法实现

⑤遍历集合的通用格式

●

for(int i=0; i<集合对象.size(); i++) {

 集合对象.get(i) 就是指定索引处的元素

}

```java
package test0419;

import java.lang.reflect.Array;
import java.util.ArrayList;

//需求：创建一个存储字符串的集合，存储3个字符串元素，使用程序实现在控制台遍历该集合
public class StringCollection {
    public static void main(String[] args) {
        ArrayList<String> list=new ArrayList<String>();
        list.add("散兵");
        list.add("纳西妲");
        list.add("雷电将军");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
```

# 第二题**案例：存储学生对象并遍历**

需求：创建一个存储学生对象的集合，存储3个学生对象，使用程序实现在控制台遍历该集合

思路：

①定义学生类

②创建集合对象

③创建学生对象

④添加学生对象到集合中

⑤遍历集合，采用通用遍历格式实现

```java
package test0419;

import java.util.ArrayList;

//创建一个存储学生对象的集合，存储3个学生对象，使用程序实现在控制台遍历该集合
//思路：
//定义学生类
//创建集合对象
//创建学生对象
//添加学生对象到集合中
//遍历集合，采用通用遍历格式实现
public class student1 {
    private String name;
    private int age;
    private String gender;

    public student1(String name, int age, String gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public student1() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "学生姓名为："
                + name +
                ", 年龄：" + age +
                ", 性别为：" + gender;
    }

    public static void main(String[] args) {
        ArrayList<student1> list = new ArrayList<student1>();
        student1 student1 = new student1("散兵", 22, "男");
        student1 student2 = new student1("纳西妲", 16, "女");
        student1 student3 = new student1("雷电将军", 28, "女");
        list.add(student1);
        list.add(student2);
        list.add(student3);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
```

# 第三题**案例：集合删除元素**

需求：创建一个存储String的集合，内部存储（test，张三，李四，test，test）字符串

删除所有的test字符串，删除后，将集合剩余元素打印在控制台

思路：

①创建集合对象

②调用add方法，添加字符串

③遍历集合，取出每一个字符串元素

④加入if判断，如果是test字符串，调用remove方法删除

⑤打印集合元素

```java
package test0419;

import java.util.ArrayList;
//需求：创建一个存储String的集合，内部存储（test，张三，李四，test，test）字符串
//删除所有的test字符串，删除后，将集合剩余元素打印在控制台
public class RemoveText {
    public static void main(String[] args) {
        ArrayList<String> list=new ArrayList<String>();
        list.add("test");
        list.add("张三");
        list.add("李四");
        list.add("test");
        list.add("test");

        for (int i = 0; i < list.size(); i++) {
            String str=list.get(i);
            if (str.equals("test")){
                list.remove(i);
                i--;// 删除元素后，需要将i减1，否则会漏删
            }
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.println("list = " + list);
            System.out.println("list = " + list.get(i));
        }
    }
}
```

# 第四题**案例：集合元素筛选**

需求：定义一个方法，方法接收一个集合对象（泛型为Student），方法内部将年龄低于18的学生对象找出

并存入新集合对象，方法返回新集合。

思路：

①定义方法，方法的形参定义为ArrayList<Student> list

②方法内部定义新集合，准备存储筛选出的学生对象 ArrayList<Student> newList

③遍历原集合，获取每一个学生对象

④通过学生对象调用getAge方法获取年龄，并判断年龄是否低于18

⑤将年龄低于18的学生对象存入新集合

⑥返回新集合

⑦main方法中测试该方法



```java
package test0419;

import java.util.ArrayList;

//需求：定义一个方法，方法接收一个集合对象（泛型为Student），方法内部将年龄低于18的学生对象找出
//并存入新集合对象，方法返回新集合。
public class StudentAgeFilter {


    public static ArrayList<Student> AgeBelow18(ArrayList<Student> list) {
        ArrayList<Student> newlist = new ArrayList<Student>();
        for (Student s : list) { // 如果 s（姓名，年龄）<18
            if (s.getAge() < 18) {
                newlist.add(s); //集合则加入这个学生的信息
            }
        }
        return newlist;
    }

    public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<Student>();
        list.add(new Student("散兵", 22));
        list.add(new Student("纳西妲", 16));
        list.add(new Student("雷电将军", 28));

        ArrayList<Student> list1 = AgeBelow18(list);// 将list中below18岁的重新用另外一个集合放置
        System.out.print("原始集合：");
        for (Student s : list) {
            System.out.print(s.getName() + "," + s.getAge());
        }
        System.out.println(" ");
        System.out.println("-------------------------------------------------------------------------");
        System.out.print("新集合：");
        for (Student s1 : list1) {
            System.out.println(s1.getName() + "," + s1.getAge());
        }
    }

}

 class Student {
    private String name;
    private int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "姓名：" + name +
                ", 年龄：" + age;
    }
}
```

# 第五题案例 ：拼接字符串

需求：定义一个方法，把 int 数组中的数据按照指定的格式拼接成一个字符串返回，调用该方法，并在控制台输出结果。例如，数组为int[] arr = {1,2,3}; ，执行方法后的输出结果为：[1, 2, 3]

思路：
定义一个 int 类型的数组，用静态初始化完成数组元素的初始化
定义一个方法，用于把 int 数组中的数据按照指定格式拼接成一个字符串返回。返回值类型 String，参数列表 int[] arr
在方法中用 StringBuilder 按照要求进行拼接，并把结果转成 String 返回
调用方法，用一个变量接收结果
输出结果

```java
package test0419;

//需求：定义一个方法，把 int 数组中的数据按照指定的格式拼接成一个字符串返回，调用该方法，
// 并在控制台输出结果。例如，数组为int[] arr = {1,2,3}; ，执行方法后的输出结果为：[1, 2, 3]
public class StringBuild {
    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 3};
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < arr.length; i++) {
            sb.append(arr[i]);
            if (i != arr.length - 1) {
                sb.append(",");
            }
        }
        sb.append("]");
        System.out.println(sb);
    }
}
```

需求：定义一个方法，把 int 数组中的数据按照指定的格式拼接成一个字符串返回，调用该方法，并在控制台输出结果。例如，数组为int[] arr = {1,2,3}; ，执行方法后的输出结果为：[1, 2, 3，3，2，1]

```java
方法一：
package test0419;

//需求：定义一个方法，把 int 数组中的数据按照指定的格式拼接成一个字符串返回，
// 调用该方法，并在控制台输出结果。例如，数组为int[] arr = {1,2,3}; ，执行方法后的输出结果为：[1, 2, 3]
public class StringBuild01 {
    public static String stringbuild(int[] arr1, int[] arr2) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < arr1.length; i++) {
            sb.append(arr1[i]);
            if (i != arr1.length - 1) {
                sb.append(",");
            }
        }
        sb.append(",");
        for (int i = arr2.length - 1; i >= 0; i--) {
            sb.append(arr2[i]);
            if (i!=0){
                sb.append(",");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public static void main(String[] args) {
        int[] arr1={1,2,3};
        int[] arr2={1,2,3};
        String result =stringbuild(arr1,arr2);
        System.out.println(result);
    }

}
方法二：
    package test0419;

public class StringBuild02 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("1,2,3");
//        sb.append(1);
//        sb.append(2);
//        sb.append(3);
        sb.reverse();
        String str = sb.toString();
        System.out.println(str);
    }
}

```