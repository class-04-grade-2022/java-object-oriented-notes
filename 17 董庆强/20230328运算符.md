## 作业

```java
1、需要判断一个字符是否是数字字符，首先需要提供一个字符数据

​    2、字符是否为数字字符： 数字字符的范围 0 - 9 之间都属于数字字符，因此提供的字符只要大于或等于字符0，并且还要下于或等于字符9即可。
​    3、判断完成之后，打印判断的结果

public static void main(String[] args) {
        int a;
        Scanner sc = new Scanner(System.in);
        System.out.println("输入一个数");
        x=sc.nextInt();
        if(0<a && a<9){
            System.out.println("是");}
        else{
            System.out.println("不是");
        }
    }


```

```java

1、需要判断一个字符是否是字母字符，首先需要提供一个字符数据

​    2、字符是否为字母字符： 数字字符的范围 a - z 或者 A - Z 之间都属于字母字符，因此提供的字符只要大于或等于a，并且还要下于或等于z 或者 大于或等于A，并且还要下于或等于Z

​    3、判断完成之后，打印判断的结果。

public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        char a = s.next().charAt(0);
        if ((int)a>=65 & (int)a<=90 ||(int)a>=97 & (int)a<=122){
            System.out.println(a+"是字母字符");
        }else{
            System.out.println(a+"不是字母字符");
        }
    }
```

```java
1、闰年的判断公式为：能被4整除，但是不能被100整除 或者 能被400整除

​    2、首先需要提供一个需要判断的年份，判断完成之后，打印判断的结果。

public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int a=s.nextInt();
        if (a%4==0 & a%100!=0 || a%400==0){
            System.out.println(a+"是闰年");
        }else{
            System.out.println(a+"不是闰年");
        }
    }
```

```java
水仙花是指3位数字，表示的是每位上的数字的3次幂相加之后的和值和原数相等，则为水仙花数，
public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();
        if (a < 100 && a > 999){
            System.out.println("不是三位数");
        }else{
            int a1= (int) Math.pow(a%10,3);
            int a2=(int) Math.pow(a/10%10,3);
            int a3=(int) Math.pow(a/100,3);
            if (a1+a2+a3==a){
                System.out.println(a+"是水仙花数");
            }else{
                System.out.println(a+"不是水仙花数");
            }
        }
    }
```

```java
五位数的回文数是指最高位和最低位相等，次高位和次低位相等。如：12321  23732  56665 
public static void main(String[] args) {
            System.out.println("请输入一个5位数：");
            Scanner sc = new Scanner(System.in);
            int num = sc.nextInt();
            if (9999 < num && num < 100000) {
                int num1 = num / 10000;
                int num2 = num / 1000 % 10;
                int num3 = num / 100 % 10;
                int num4 = num / 10 % 10;
                int num5 = num % 10;
                if (num1 == num5 && num2 == num4) {
                    System.out.println(num + "是回文数");
                } else {
                    System.out.println(num + "不是回文数。");
                }
            } else {
                System.out.println("输入错误：请输入一个5位数");

            }
        }
```
