# 笔记

1.成员

​    1.成员变量(局部变量)：类中方法外就是成员变量

​     2.成员方法

1. ```java
   2.toString() 方法：将对象以字符串的形式返回，这样之后，外界直接打印对象名，就可以得到对象的属性
   
   3.private关键词，它是一个权限修饰符，被它修饰的成员，就只限于本类使用
   
   1.如果外界需要访问私有化的成员，需要类中提供两个通道（setter.getter)
   
   1. 
   ```

   赋值通道

   ```java
   public void setAge(int age){
       this.age=age;
   }
   ```

   2.取值通道

   

```java
public int getAge(){
    reture this.age;
}
```

this:用来区分成员变量和局部变量的一个关键词，它放在成员的前面



# 作业

题目：1，创建一个员工类（标准类），2.在测试类中用无参和有参两种构造器。分别创建一个对象。并打印对象



```java
public class Employee {
    private String serial;
    private String name;
    private int age;
    private double pay;

    public Employee(){
    }

    public Employee(String serial,String name,int age,double pay){
        this.serial=serial;
        this.name=name;
        this.age=age;
        this.pay=pay;
    }

    public String toString(){
       return "第"+serial+"个员工的编号："+"姓名："+name+"，年龄："+age+"，薪资："+pay;
    }
}

```

## main 函数

```java
    public static void main(String[] args) {
        Employee employee=new Employee("1","张三",23,10000);
        System.out.println(employee.toString());
        Employee employee1=new Employee("2","李四",22,11000);
        System.out.println(employee1.toString());
    }
```

