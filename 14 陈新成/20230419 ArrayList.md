需求：创建一个存储字符串的集合，存储3个字符串元素，使用程序实现在控制台遍历该集合

```java
        ArrayList<String> list = new ArrayList<>();
        list.add("张三");
        list.add("李四");
        list.add("王五");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
```



需求：创建一个存储学生对象的集合，存储3个学生对象，使用程序实现在控制台遍历该集合

```java
public class Student {
    String name;
    int age;
    String sex;

    public Student(String name, int age, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public String toString() {
        return name +
                ", " + age;
    }
}

```



```java
        ArrayList<Student> list = new ArrayList<>();
        Student s1 = new Student("张三");
        Student s2 = new Student("李四");
        Student s3 = new Student("王五");
        list.add(s1);
        list.add(s2);
        list.add(s3);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(String.valueOf(list.get(i)));
        }
```



需求：创建一个存储学生对象的集合，存储3个学生对象，使用程序实现在控制台遍历该集合

​     学生的姓名和年龄来自于键盘录入

```java
public class Student {
    String name;
    int age;
    String sex;

    public Student(String name, int age, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public String toString() {
        return name +
                ", " + age;
    }
}

```



```java
        Scanner sc=new Scanner(System.in);
        Student s1 = new Student(sc.next(),sc.nextInt());
        Student s2 = new Student(sc.next(),sc.nextInt());
        Student s3 = new Student(sc.next(),sc.nextInt());
        ArrayList<Student> list = new ArrayList<>();
        list.add(s1);
        list.add(s2);
        list.add(s3);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(String.valueOf(list.get(i)));
        }
```



需求：创建一个存储String的集合，内部存储（test，张三，李四，test，test）字符串

删除所有的test字符串，删除后，将集合剩余元素打印在控制台

```java
ArrayList<String> list = new ArrayList<>();
        list.add("test");
        list.add("张三");
        list.add("李四");
        list.add("test");
        list.add("test");
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals("test")){
                list.remove("test");
                i--;
            }
        }
        System.out.printf(String.valueOf(list));
```



定义一个方法，方法接收一个集合对象（泛型为Student），方法内部将年龄低于18的学生对象找出

并存入新集合对象，方法返回新集合。



```java
import java.util.ArrayList;

public class Student {
    String name;
    private int age;
    String sex;

    public Student(String name, int age, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {

        this.age = age;
    }
    public static ArrayList list2(ArrayList<Student> list){
        ArrayList<Student> list2 = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).age<18){
                list2.add(list.get(i));
            }
        }
        return list2 ;
    }

    @Override
    public String toString() {
        return name +
                ", " + age;
    }
}

```



```java
        Student s1 = new Student("张三",15);
        Student s2 = new Student("李四",18);
        Student s3 = new Student("王五",24);
        Student s4 = new Student("撒旦",12);
        ArrayList<Student> list = new ArrayList<>();
        list.add(s1);
        list.add(s2);
        list.add(s3);
        list.add(s4);
        System.out.println(Student.list2(list));
```

