```java
public abstract class Food {
    private String color;
    private double weight;
    public void eat(){
        System.out.println("可以吃");
    }
    public void use(){
        System.out.println("可以使用");
    }

    public Food(String color, int weight) {
        this.color = color;
        this.weight = weight;
    }

    public Food() {
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

}
    class cucumber extends Food{
    @Override
    public void eat() {
        System.out.println("可以吃黄瓜炒蛋");
    }

    @Override
    public void use() {
        System.out.println("可以使用做面膜");
    }
    static class aubergine extends Food{
        @Override
        public void eat() {
            System.out.println("可以吃油焖茄子");
        }

        @Override
        public void use() {
            System.out.println("可以使用做中药");
        }
    }
    static class banana extends Food{
        @Override
        public void eat() {
            System.out.println("可以吃脆皮香蕉");
        }

        @Override
        public void use() {
            System.out.println("可以使用做香蕉面膜");
        }
    }
    static class durian extends Food{
        @Override
        public void eat() {
            System.out.println("可以吃榴莲酥");
        }

        @Override
        public void use() {
            System.out.println("可以使用砸人");
        }
    }

        public static void main(String[] args) {
            cucumber cu = new cucumber();
            cu.setColor("黄色");
            cu.setWeight(220);
            System.out.println("黄瓜颜色："+cu.getColor()+" 黄瓜重量："+cu.getWeight()+"克");
            cu.eat();
            cu.use();
            aubergine au = new aubergine();
            au.setColor("紫色");
            au.setWeight(200);
            System.out.println("茄子颜色："+au.getColor()+" 茄子重量："+au.getWeight()+"克");
            au.eat();
            au.use();
            banana ba = new banana();
            ba.setColor("黄色");
            ba.setWeight(120);
            System.out.println("香蕉颜色："+ba.getColor()+" 香蕉重量："+ba.getWeight()+"克");
            ba.eat();
            ba.use();
            durian du = new durian();
            du.setColor("青黄色");
            du.setWeight(500);
            System.out.println("榴莲颜色："+cu.getColor()+" 榴莲重量："+cu.getWeight()+"克");
            du.eat();
            du.use();
        }
}
```

