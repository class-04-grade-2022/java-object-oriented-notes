接口题目2

1. 请定义“员工(类)”：
   属性：姓名、性别、年龄(全部私有)
   行为：工作(抽象)
   无参、全参构造方法
   get/set方法

2. 请定义“绘画(接口)”
   抽象方法：绘画
3. 请定义“Java讲师类”继承自“员工类”
4. 请定义”UI讲师类”，继承自“员工类”，并实现“绘画”接口。

**要求**：

1. 请按上述要求设计出类结构，并实现相关的方法。
2. 测试类中创建对象测试，属性可自定义：
   - 创建一个Java讲师类对象，调用工作的方法。
   - 创建一个UI讲师类对象，调用工作方法，和绘画方法。

**答案：**

```java
package D1;

public abstract class Staff {
    private String name;
    private String sex;
    private int age;

    public abstract void work();

    public Staff() {
    }

    public Staff(String name, String sex, int age) {
        this.name = name;
        this.sex = sex;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
interface Drawing {
    public void draw();
}

class JavaTeacher extends Staff{
    public JavaTeacher() {
    }

    public JavaTeacher(String name, String sex, int age) {
        super(name, sex, age);
    }

    public String toString() {
        return "JavaTeacher{" +
                "name='" + getName() + '\'' +
                ", sex='" + getSex() + '\'' +
                ", age=" + getAge() +
                '}';
    }

    @Override
    public void work() {
        System.out.println(getName()+"讲Java课");
    }



}
class UITeacher extends Staff implements Drawing{
    public UITeacher() {
    }

    public UITeacher(String name, String sex, int age) {
        super(name, sex, age);
    }

    public String toString() {
        return "JavaTeacher{" +
                "name='" + getName() + '\'' +
                ", sex='" + getSex() + '\'' +
                ", age=" + getAge() +
                '}';
    }
    @Override
    public void work() {
        System.out.printf(getName()+"讲UI设计课");
    }
    @Override
    public void draw() {
        System.out.println(getName()+"画画");
    }


}
class Test{
    public static void main(String[] args) {
        JavaTeacher java = new JavaTeacher("小明","男",25);
        System.out.println(java);
        java.work();
        UITeacher ui = new UITeacher("小红", "女", 18);
        System.out.println(ui);
        ui.work();
        ui.draw();
    }

}
```



**运行结果：**

```java
JavaTeacher{name='小明', gender='男', age=25}
小明讲Java课
UITeacher{name='小红', gender='女', age=18}
小红讲UI设计课
小红画画
```