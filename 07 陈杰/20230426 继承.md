作业规范【必读】

命名要求：
	1. 类名，接口名，枚举名，注解名使用大驼峰
	2. 变量名，方法名，包名均使用小驼峰
	3. 常量名全大写，多个单词下划线分割
	4. 名字要见名知意，如果不知道对应的英文，可以使用拼音代替。不可使用无意义字符

代码规范：
	格式要良好，使用IDEA格式化缩进（快捷键：Ctrl+Alt+L）

答题规范：
	1. 每道题完整代码请贴入对应题目中的代码区。
    2. 如果有运行结果的，请把代码运行结果放到文档中
```
# 【继承】

## 题目1

我们计划为一个电器销售公司制作一套系统，公司的主要业务是销售一些家用电器，例如：电冰箱、洗衣机、电视机产品。

类的设计为：
```
冰箱类
	属性：品牌、型号、颜色、售价、门款式、制冷方式

洗衣机类
	属性：品牌、型号、颜色、售价、电机类型、洗涤容量

电视类
	属性：品牌、型号、颜色、售价、屏幕尺寸、分辨率
```Java

**要求：**

请根据上述类的设计，抽取父类“电器类”，并代码实现父类“电器类”、子类“冰箱类”，“洗衣机类”、“电视类”



答题：
// 父类
public class Electrical {
    String brand;// 品牌
    String model;// 型号
    String color;// 颜色
    double price;// 售价
}
// 洗衣机类
public class Laundry_Machine extends Electrical{
    String generator_style;// 电机类型
    int Water_Capacity;// 洗涤容量

}
// 冰箱类
public class Refrigerator extends Electrical {
    String door_style;// 门款式
    String Cooling_mode;// 制冷方式

}
// 电视类
public class TV extends Electrical{
    String screen_size;// 屏幕尺寸
    String resolution_ratio;// 分辨率

}
```



## 题目2

我们计划为一个动物园制作一套信息管理系统，根据与甲方沟通，我们归纳了有以下几种动物需要记录到系统中：

```java
鸟类：		 鹦鹉、猫头鹰、喜鹊
哺乳类：	大象、狼、长颈鹿
爬行类：	鳄鱼、蛇、乌龟
```



**要求：**

请根据以上需求，使用“继承”设计出三层的类结构

```java
|--动物类
    |--鸟类：
        |--鹦鹉类
        |--猫头鹰类
        |--喜鹊类
  
    |--哺乳类：
        |--大象类
        |--狼类
        |--长颈鹿类
  
    |--爬行类：
        |--鳄鱼类
        |--蛇类
        |--乌龟类
```

作为父类的类都应该定义一些共性内容，每种子类也都有一些特定的内容。 重点是类的层次结构，类成员简单表示即可。



**答案：**

```java
public class Animal {// 动物类
    String breathe;
}
    class Bird extends Animal{// 鸟类
        String animal_heat;
    }
        class Parrot extends Bird{// 鹦鹉
        }
        class Owl extends Bird{// 猫头鹰
        }
        class Magpie extends Bird{// 喜鹊
        }
    class Mammal extends Animal{// 哺乳类
        String physical_trait;
    }
        class Elephant extends Mammal{// 大象
        }
        class Wolf extends Mammal{// 狼
        }
        class Giraffe extends Mammal{// 长颈鹿
        }
    class Reptilian extends Animal{// 爬行类
        String exercise_mode;
    }
        class Crocodile extends Reptilian{// 鳄鱼
        }
        class Snake extends Reptilian{// 蛇
        }
        class Tortoise extends Reptilian{// 乌龟
        }