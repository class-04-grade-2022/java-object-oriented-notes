# 1.笔记

```java
1、类笔记
public class 类名{
    // 成员变量
    数据类型 变量名;
    // 成员方法
    public viod 方法名(){
        方法代码块
    }
}

2、测试类笔记
// 每new一次会在堆内存占用一个空间
类名 对象名=new 类名;
// 输出对象在堆内存的地址
System.out.println(对象名);
// 运行类的代码
类名.方法名();
```

# 2.作业

要求： 1.定义工具类，可以用来求和，求最大值，最小值，给数组从小到大排序，给数组从大到小到排序。从数组中找一个元素的位置，

```java
public class Ini {
    public int Max(int[] arr) {
        int max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
            }
        }
        int getmax = max;
        return getmax;
    }
    public int Min(int[] arr) {
        int min = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (min > arr[i]) {
                min = arr[i];
            }
        }
        int getMin = min;
        return getMin;
    }
    public int getSum(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }
    public void sortSmalltoBig(int[] arr){
        int a;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < i; j++) {
                if (arr[i]>arr[j]){
                    a=arr[i];
                    arr[i]=arr[j];
                    arr[j]=a;
                }
            }
        }
    }
    public void sortBigtoSmall(int[] arr){
        int a;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < i; j++) {
                if (arr[i]<arr[j]){
                    a=arr[i];
                    arr[i]=arr[j];
                    arr[j]=a;
                }
            }
        }
    }
    public void print(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]+"\t");
        }
    }
}
```

2.再定义一个测试类，测试上面的方法

```java
import java.util.Arrays;

public class Utils {
    public static void main(String[] args){
        int[] arr = {2,3,5,7,11,13,17,19,23,29,31,37};
        Ini utils = new Ini();

        int max = utils.Max(arr);
        System.out.println("max = " + max);

        int min = utils.Min(arr);
        System.out.println("min = " + min);

        utils.sortSmalltoBig(arr);
        System.out.println(" arr ="+ Arrays.toString(arr));

        utils.sortBigtoSmall(arr);
        System.out.println(" arr ="+ Arrays.toString(arr));

    }
}
```

