# 笔记

显示当前日期和时间

date 日期

println  

```java
Date date=new Date();
System.out.println(date.toString());
```



# 巩固题

## 1、判断5的倍数

从键盘输入一个整数，判断它是否是5的倍数

```java
 Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        if (a%5==0)
            System.out.println(a+"是5的倍数");
        else
            System.out.println(a+"不是5的倍数");
```



## 2、判断字符类型

从键盘输入一个字符，判断它是字母还是数字，还是其他字符

```java
 Scanner sc=new Scanner(System.in);
        String a1=sc.next();
        char a=a1.charAt(0);
        if (a1.length()==1) {
            if (a >= 'a' && a <= 'z' || a >= 'A' && a <= 'Z') {
                System.out.println(a+"是字母");
            }
            else if (a >= '0' && a <= '9' ) {
                System.out.println(a+"是数字");
            }
            else {
                System.out.println(a+"是符号");
            }
        }
        else{
            System.out.println("只需要一个字符");

        }
```

## 3、计算折扣后金额

从键盘输入订单总价格totalPrice（总价格必须>=0），根据优惠政策计算打折后的总价格。

编写步骤：

1. 判断当`totalPrice >=500` ,discount赋值为0.8
2. 判断当`totalPrice >=400` 且`<500`时,discount赋值为0.85
3. 判断当`totalPrice >=300` 且`<400`时,discount赋值为0.9
4. 判断当`totalPrice >=200` 且`<300`时,discount赋值为0.95
5. 判断当`totalPrice >=0` 且`<200`时,不打折，即discount赋值为1
6. 判断当`totalPrice<0`时，显示输入有误
7. 输出结果

```java
		Scanner sc=new Scanner(System.in);
        int totalPrice=sc.nextInt();
        double discoun=0.0;
        if (totalPrice>=0){
           if (totalPrice>=500){
                discoun=0.8;
           }
           else if (totalPrice>=400) {
               discoun=0.85;
           }
           else if (totalPrice>=300) {
               discoun=0.9;
           }
           else if (totalPrice>=200) {
               discoun=0.95;
           }
           else{
               discoun=1.0;
           }
        }
        else{
            System.out.println("输入有误");
        }
        double a;
        a=totalPrice*discoun;
        System.out.println("打折后的总价格为"+a);
```



## 4、输出月份对应的英语单词

从键盘输入月份值，输出对应的英语单词

```java
Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        if (a<=12 && a>=0){
            switch (a){
                case 1:
                    System.out.println("January");
                    break;
                case 2:
                    System.out.println("February");
                    break;
                case 3:
                    System.out.println("March");
                    break;
                case 4:
                    System.out.println("April");
                    break;
                case 5:
                    System.out.println("May");
                    break;
                case 6:
                    System.out.println("June");
                    break;
                case 7:
                    System.out.println("July");
                    break;
                case 8:
                    System.out.println("August");
                    break;
                case 9:
                    System.out.println("September");
                    break;
                case 10:
                    System.out.println("October");
                    break;
                case 11:
                    System.out.println("November");
                    break;
                case 12:
                    System.out.println("December");
                    break;
            }
        }
        else{
            System.out.println("超出月份范围");
        }
```



## 5、计算今天是星期几

定义变量week赋值为上一年12月31日的星期值（可以通过查询日历获取），定义变量year、month、day，分别赋值今天日期年、月、日值。计算今天是星期几。



```java
    public static String getWeekday(String date){//必须yyyy-MM-dd
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdw = new SimpleDateFormat("E");
        Date d = null;
        try {
            d = sd.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdw.format(d);
```



# 拔高题

## 1、判断年、月、日是否合法

从键盘输入年、月、日，要求年份必须是正整数，月份范围是[1,12]，日期也必须在本月总天数范围内，如果输入正确，输出“年-月-日”结果，否则提示输入错误。

## 2、判断打鱼还是晒网

从键盘输入年、月、日，假设从这一年的1月1日开始执行三天打鱼两天晒网，那么你输入的这一天是在打鱼还是晒网。

## 3、判断星座

声明变量month和day，用来存储出生的月份和日期，判断属于什么星座，各个星座的日期范围如下：

![1558000604568](03_Java流程控制语句结构作业图片/1558000604568.png)

# 简答题

1、switch是否能作用在byte上，是否能作用在long上，是否能作用在String上？

