#  笔记

```java
成员
    1.1成员变量:类中方法外的就是成员变量
       局部变量:类中方法内的就是局部变量
2.区别
  1.类中位置不同：
    1>成变：类中方法外
    2>局变：方法外或者方法申明上（形参）
  2.内存中位置不同：
    1>成变：堆内存
    2>局变：栈内存
  3.生命周期不同：
    1>成变：随着对象的存在而存在，随着对象的消失而消失
    2>局变：随着方法的调用而存在，随着方法的调用完毕而消失
  4：初始化值不同：
    1>成变：有默认的初始化值
    2>局变：没有默认的初始化值，必需先定义，赋值，才能使用
3.封装 
  private 关键字：1.是一个权限修饰符；2.可以修饰成员（成员变量和成员方法）；3.被 private 修饰的成员只能在本类中才能访问
  针对 private 修饰的成员变量，如果需要被其他类使用，提供相应操作
    1>提供 "get变量名()"方法，用于获取成员的变量值，方法用public修饰
    2>提供"set变量名(参数)"方法，用于设置成员变量的值，方法用public修饰
  private 关键字使用
    一个标准类编写：
    1>把成员变量用private修饰
    2>提供对应的setXxx()/getXxx()方法
    赋值通道,set例子:
           public void setAge(int age){
               this.age=age;
           }
    取值通道,get例子:
           public int getAge(){
               return this.age;
           }
  this 关键字  
    *作用：用来区分成员变量和局部变量的一个关键字，放在成员前1.成员
    1.成员变量：
    2.局部变量：
```

# 作业

```java
package qwe;

public class Employee {
    // 题目：1，创建一个员工类（标准类），
    int id;
    String name;
    int age;
    int  salary;
    public Employee(){}
    public Employee(int id,String name,int age,int salary){
        this.id=id;
        this.name=name;
        this.age=age;
        this.salary=salary;

    }
    public String toString(){
        String str="编号,"+id+"姓名:"+name+"年龄,"+age+"年薪,"+salary;
        return str;
    }
}

// 2.在测试类中用无参和有参两种构造器。分别创建一个对象。并打印对象

package qwe;

public class test2 {
    public static void main(String[] args) {
        Employee employee = new Employee();
        Employee a=new Employee();
        a.id=1;
        a.name="张三";
        a.age=23;
        a.salary=10000;
        System.out.println(a);
        Employee xx = new Employee(2, "李四", 22, 11000);
        System.out.println(xx);

    }
}

```

