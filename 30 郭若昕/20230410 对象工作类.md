# 笔记

```java
先写一个类来写属性和行为比如
    public class xxx{
        里面定义属性，行为 比如
        String name;
        int id;
        
        下面的是行为
        public void sleep(){
            
        }
    }
然后再写一个类用new会有一个堆内存，把一个类似的东西存进去自己赋予它的属性
```

要求： 
1.定义工具类，可以用来求和，求最大值，最小值，给数组从小到大排序，给数组大小到排序。从数组中找一个元素的位置，

```java
public class Goj {
    public void sum(int a,int b){
        System.out.println(a+b);
    }
    public void max(int[] x){
        int max = x[0];
        for (int i : x) {
            if (max<i){
                max=i;
            }
        }
        System.out.println("最大值:"+max);
    }
    public void min(int[] x){
        int min = x[0];
        for (int i : x) {
            if (min>i){
                min=i;
            }
        }
        System.out.println("最小值:"+min);
    }
    public void stb(int[] x){
        int a;
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x.length-1; j++) {
                if (x[j]>x[j+1]){
                    a=x[j];x[j]=x[j+1];x[j+1]=a;
                }
            }
        }
        System.out.println("从小到大"+Arrays.toString(x));
    }
    public void bts(int[] x){
        int a;
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x.length-1; j++) {
                if (x[j]<x[j+1]){
                    a=x[j];x[j]=x[j+1];x[j+1]=a;
                }
            }
        }
        System.out.println("从大到小"+Arrays.toString(x));
    }
    public void find(int[] x){
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入数组中数字");
        int a = sc.nextInt();
        for (int i = 0; i < x.length; i++) {
            if (x[i]==a){
                System.out.println("它的位置是第"+i+"个");
                break;
            }
        }
    }
}
```

2.再定义一个测试类，测试上面的方法

```java
public class Test {
    public static void main(String[] args) {
        int[] arr = {50,132,19,633,12,62};
        Goj gj = new Goj();
        gj.sum(20,6);
        gj.max(arr);
        gj.min(arr);
        gj.stb(arr);
        gj.bts(arr);
        gj.find(arr);
    }
}
```