用户登录

```java
import java.util.Scanner;

public class testneo {
    public static void main(String[] args) {
       Scanner sc=new Scanner(System.in);
        System.out.println("请设置用户名");
        String user= sc.next();
        System.out.println("请设置密码");
        String pwd= sc.next();
        for(int i=1;i<4;i++){
            System.out.println("请输入用户名");
            String user1= sc.next();
            System.out.println("请输入密码");
            String pwd1= sc.next();
            if(pwd1.equals(pwd)&&user1.equals(user)){
                System.out.println("密码和用户名输入正确");
                break;
            }else {
                System.out.println("密码或者用户名不正确，请再次输入"+"\n还有"+(3-i)+"次机会");
            }
        }
    }
}

```

遍历字符串

```java
import java.util.Arrays;
import java.util.Scanner;

public class testtwo {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入字符串");
        String str= sc.next();
        for(int i=0;i<str.length();i++){
            char z=str.charAt(i);
            System.out.println(z);
        }
    }
}

```

统计字符串出现次数

```java
package test02;
import java.util.Arrays;
import java.util.Scanner;
public class test04 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入字符串");
        String str=sc.next();
        char[] arr=new char[str.length()];
        for(int i=0;i<str.length();i++){
            arr[i]=str.charAt(i);
        }
        System.out.println(Arrays.toString(arr));
        int dxzm=0;
        int xszm=0;
        int num=0;
        char a;
        for(int i=0;i< arr.length;i++){
           a=arr[i];
           if(a>=48 && a<=57){
               num++;
           }else if(a>=97 && a<=122){
               xszm++;
           }else if(a>=65 && a<=90){
               dxzm++;
           }
        }
        System.out.println("\n数字出现的次数:"+num+"\n大写字母出现的次数:"+dxzm+"\n小写字母出现次数:"+xszm);
        }


    }


```

手机号屏蔽

```java
package test02;

import java.util.Scanner;

public class test5 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入的电话号码");
        String tel= sc.next();
        String hand=tel.substring(0,3);
        String end=tel.substring(7);
        System.out.println("你的电话号码是:"+hand+"*****"+end);
    }
}

```

敏感词替换

```java
package test02;

public class test6 {
    public static void main(String[] args) {
        String a="我记得很好，我觉得还行，我觉得非常好";
        System.out.println(a);
        System.out.println("替换后");
        String b= a.replace('我','他');
        System.out.println(b);

    }
}

```

字符串切割

```java
package test02.tese6;
import java.util.Arrays;
import java.util.Scanner;
public class teststudent {
    public static void main(String[] args) {
        System.out.println("请输入您的学生信息");
        Scanner sc=new Scanner(System.in);
        String stu= sc.next();
        String[] a=stu.split(",");
        System.out.println(Arrays.toString(a));

    }
}

```

