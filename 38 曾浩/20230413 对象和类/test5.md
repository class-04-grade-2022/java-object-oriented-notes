employee

```java
//声明另一个Employee类型，
//有属性：姓名（String类型），生日（MyDate类型）
//增加一个void setBirthday(int year, int month, int day)方法，用于给员工生日赋值
//增加一个String getEmpInfo()方法，用于返回员工对象信息，例如：姓名：xx，生日：xx年xx月xx日
public class employee {
    String name;
    Mydate birthday;
    public employee(){

    }
    public employee(String name,Mydate birthday){
        this.name=name;
        this.birthday=birthday;
    }
    public void setBirthday(Mydate birthday){
        this.birthday=birthday;
    }
    public Mydate getBirthday(){
        return birthday;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    public void setBirthday(int year,int months ,int day){
        this.birthday=new Mydate(year, months,day);
    }
    public String getEmpInfo(){
        return "名字是:"+name+"  "+"生日是:"+birthday;
    }
    public String toString(){
        return getEmpInfo();
    }
}

```

Mydate

```java
//声明一个MyDate类型
//有属性：年，月，日
//增加一个String getDateInfo()方法，用于返回日期对象信息，例如：xx年xx月xx日
public class Mydate {
    int year;
    int months;
    int day;
    public Mydate(){

    }
    public Mydate(int year,int months,int day){
        this.year=year;
        this.months=months;
        this.day=day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonths() {
        return months;
    }

    public void setMonths(int months) {
        this.months = months;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
    public String getdateinfo(){
        return year+"年 "+months+"月 "+day+"日";
    }
    public String toString(){
        return getdateinfo();
    }
}

```

test

```java
public class test {
    public static void main(String[] args) {
        employee a=new employee("张三",new Mydate(2003,2,23));
        System.out.println(a);
        employee b=new employee();
        b.setName("李四");
        b.setBirthday(2009,6,19);
        System.out.println("名字是:"+b.getName()+"  "+"生日是"+b.getBirthday());

    }
}

```

