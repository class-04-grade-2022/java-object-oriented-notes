circle

```java
import java.text.DecimalFormat;

//（1）声明一个圆的图形类，包含实例变量/属性：半径
//（2）将圆求面积、求周长、返回圆对象信息分别封装为3个方法
//double area()：求面积
//double perimeter()：求周长
//string getInfo()：返回圆对象信息，例如："半径：xx，周长：xx，面积：xx"
//3）在测试类的main中，创建2个圆类的对象，并给两个圆对象的半径属性赋值，最后显示两个圆的半径值、周长和面积值
//提示：圆周率可以使用Math.PI
public class circle {
    double r;
    DecimalFormat a=new DecimalFormat("0.00");
    public circle(){

    }
    public circle(double r){
        this.r=r;
    }
    public void setR(double r){
        this.r=r;
    }
    public double getR(){
        return r;
    }
    public double area(){
       return  r*r*Math.PI;
    }
    public double perimeter(){
        return 2*r*Math.PI;
    }
    public String getinfo(){
    return "圆的半径是:"+r+"  周长是:"+a.format(this.perimeter())+"  面积是:"+a.format(this.area());
    }
    public String toString(){
        return getinfo();
    }
}

```

testcircle

```java
public class testcircle {
    public static void main(String[] args) {
        circle a=new circle(6);
        System.out.println(a);
        a.setR(9);
        System.out.println(a);
    }
}

```

