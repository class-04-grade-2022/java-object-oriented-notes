```java
package test.two;
//声明一个学生类，包含实例变量/属性：姓名和成绩
//在测试类的main中，创建2个学生类的对象，并给两个学生对象的姓名和成绩赋值，最后输出显示
public class student {
    String name;
    double score;
    public student(){

    }
    public student(String name, double score) {
        this.name = name;
        this.score = score;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    public void setScore(double score){
        this.score=score;
    }
    public double getScore(){
        return score;
    }
    public String toString(){
        return "学生的姓名是: "+name+"成绩是: "+score;
    }
}

```

mian

```java
package test.two;

public class score {
    public static void main(String[] args) {
        student a=new student("张三",99.5);
        System.out.println(a);
        a.setName("李四");
        a.setScore(99.2);
        System.out.println(a);
    }
}
```

