employee

```java
public class employee {
    String name;
    Mydate birthday;
    public employee(){

    }
    public employee(String name,Mydate birthday){
        this.birthday=birthday;
        this.name=name;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    public void setBirthday(Mydate brithday){
        this.birthday=brithday;
    }
    public Mydate getBirthday(){
        return birthday;
    }
    public String toString(){
        return "员工的姓名是:"+name+"生日是:"+ birthday.year+"-"+birthday.months+"-"+birthday.day;
    }

}
```

Mydate

```java
//声明一个MyDate类型，有属性：年，月，日
public class Mydate {
    int year;
    int months;
    int day;
    public Mydate(){

   }
    public Mydate(int year, int months, int day) {
        this.year = year;
        this.months = months;
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonths() {
        return months;
    }

    public void setMonths(int months) {
        this.months = months;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}

```

employeemydate

```java
//在测试类中的main中，创建两个员工对象，并为他们的姓名和生日赋值，并显示
public class empmydate {
    public static void main(String[] args) {
    employee a=new employee("张三",new Mydate(1999,2,12));
        System.out.println(a);
    a.setName("李四");
    a.setBirthday(new Mydate(2001,11,22));
        System.out.println(a);

    }
}

```

