Mylnt

```java
import java.util.Arrays;

//包含一个int类型的value属性
//包含一个方法boolean isNatural()方法，用于判断value属性值是否是自然数。自然数是大于等于0的整数。
//包含一个方法int approximateNumberCount()方法，用于返回value属性值的约数个数。在[1, value]之间可以把value整除的整数都是value的约数。
//包含一个方法boolean isPrimeNumber()方法，用于判断value属性值是否是素数。如果value值在[1, value]之间只有1和value本身两个约数，并且value是大于1的自然数，那么value就是素数。
//包含一个方法int[] getAllPrimeNumber()方法，用于返回value属性值的所有约数。返回[1, value]之间可以把value整除的所有整数。
//（2）测试类的main中调用测试
public class Mylnt {
    int value;
    public Mylnt(){

    }
    public Mylnt(int value){

        this.value=value;
    }
    public void setValue(int value){

        this.value=value;
    }
    public int getValue(){
        return value;
    }
    //包含一个方法boolean isNatural()方法，用于判断value属性值是否是自然数。自然数是大于等于0的整数。
    public boolean isNatural(){
        boolean f=false;
        if(value>=0){
            f=true;
        }
        return f;
    }
    //包含一个方法int approximateNumberCount()方法，用于返回value属性值的约数个数。
    // 在[1, value]之间可以把value整除的整数都是value的约数。
    public int approximateNumberCount(){
        int num=0;
        for(int i=1;i<=value;i++){
            if(value%i==0){
                num++;
            }
        }
        return num;
    }
    //包含一个方法boolean isPrimeNumber()方法，用于判断value属性值是否是素数。
    // 如果value值在[1, value]之间只有1和value本身两个约数，并且value是大于1的自然数，那么value就是素数。
    public boolean isPrimeNumber(){
        boolean ss=false;
        if( approximateNumberCount()==2){
            ss=true;
        }
        return ss;
    }
    //包含一个方法int[] getAllPrimeNumber()方法，用于返回value属性值的所有约数。返回[1, value]之间可以把value整除的所有整数。
    public int[] getAllPrimeNumber(){
        int[] arr=new int[approximateNumberCount()];
        int index=0;
        for (int i = 1; i <=value ; i++) {
            if (value%i==0){
                arr[index++]=i;

            }
        }
        return arr;
    }
    public String toString(){
        return "\n输入的数是:"+value+
                "\n是否是自然数:"+isNatural()+
                "\n约数个数:"+approximateNumberCount()+
                "\n是否是素数:"+isPrimeNumber()+
                "\n约数都有"+ Arrays.toString(getAllPrimeNumber());
    }

}

```

test

```java
public class test {
    public static void main(String[] args) {
        Mylnt a=new Mylnt(66);
        System.out.println(a);
    }
}

```

