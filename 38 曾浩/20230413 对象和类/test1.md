```java
import java.text.DecimalFormat;

//声明一个圆的图形类，包含实例变量/属性：半径
//在测试类的main中，创建2个圆类的对象，并给两个圆对象的半径属性赋值，最后显示两个圆的半径值、周长和面积值
// 提示：圆周率可以使用Math.PI
public class yuan {
    double r;
    public yuan(){

    }
    public yuan(double r){
        this.r=r;
    }
    public void setR(double r){
        this.r=r;
    }
    public double getR(){
        return r;
    }
    DecimalFormat a=new DecimalFormat("0.00");
    public String toString(){
        return "圆的半径是:"+r+"圆的周长是:"+a.format(Math.PI*r*2)+"圆的面积是:"+a.format(Math.PI*r*r);
    }
}
```

main

```
public class circle {
    public static void main(String[] args) {
        yuan neo=new yuan(3);
        System.out.println(neo);
        neo.setR(6);
        System.out.println(neo);
    }
}
```

