# 笔记



```java
方法的定义格式
    
public static 返回值类型 方法名称(参数类型 参数变量,...){
    方法体 (本方法要执行的若干操作);
    [return[返回值];]
}

方法的类型

1.无参无返回值

2.无参有返回值

3.有参无返回值

4.有参有返回值


形参和实参

	形参：定义方法时，写在（）里的变量，无实际值。

	实参：调用方法时，写在（）里变量，有实际的值。

Java的参数传递机制是什么样的？

	值传递，传输的是实参存储的值

### return 直接结束方法并调用



方法重载

	方法重载是方法名称进行重用的一种技术形式，其最主要的特点为 “方法名称相同，参数的类型或个数不同”，在调用时会根据传递的参数类型和个数不同执行不同的方法体。方法重载时考虑到标准性一般都建议统一返回值类型
	方法名称相同，参数的类型或个数不同，则此方法被称为重载



## 方法递归调用

	递归调用是一种特殊的调用形式，指的是方法自己调用自己的形式

    递归调用必须有条件

	每次调用时都需要根据需求改变传递的参数内容
```



# 作业

~~~java
package ly;

import java.llll.Scanner;

public class yyyyyy {
    public static void main(String[] args) {
        printInfo();
        printInfo2();
    }
    //    1,定义一个方法，接收一个数组，返回该数组的最大值和最小值；在主方法中打印最大，最小值
    public static void printInfo(){
        int arr[] = {8,9,45,61,34,87,25};
        int max = arr[0];
        int min = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        System.out.println("最大值是"+max);
        System.out.println("最小值是"+min);
    }
    //2,定义一个方法，接收长和宽，返回一个矩形的面积，在主方法中，打印这个面积
     public static void printInfo2(){
        Scanner sc = new Scanner(System.in);
         System.out.println("请输入长度");
        double longs = sc.nextDouble();
         System.out.println("请输入宽度");
        double wide = sc.nextDouble();
        double area = longs * wide;
         System.out.println("该矩形的面积为:"+area);
     }
}
```

	//3利用方法重载，定义多个同名方法，通过接收一个不同类型的参数，方法体里用扫描器接收一个对应类型的值，最后将该输入的值返回，在主方法中，输出该值。

    
    
    
    
~~~

