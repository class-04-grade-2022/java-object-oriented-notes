# 笔记

```java


 扫描器
     例子：
Scanner sc = new Scanner(System.in);
System.out.println("输入一个数字:");
int a = sc.nextInt();//根据类型变换
char sc=sc.next().charAt(0);

// 基本数据类型：
  //  整数：byte，short，int，long
  //  浮点数：float，double
  //  字符：char
  //  布尔：boolean

if语句：
    if(条件){
        满足条件的结果
    }else{
        为满足条件的结果
    }
```



# 作业

```java
package test.base;

import com.sun.deploy.security.SelectableSecurityManager;

import java.util.Scanner;

public class ly {
    public static void main(String[] args) {
//  **1、判断一个字符数据是否是数字字符 **
//	    1、需要判断一个字符是否是数字字符，首先需要提供一个字符数据
//	    2、字符是否为数字字符： 数字字符的范围 0 - 9 之间都属于数字字符，因此提供的字符只要大于或等于字符0，并且还要下于或等于字符9即可。
//	    3、判断完成之后，打印判断的结果。
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入一个数");
       char num= sc.next().charAt(0);
        if(num>=0 && num<=9)
            System.out.println(num+"是数字");
       else
            System.out.println(num+"不是数字");
 //  *2、判断一个字符数据是否是字母字符**
//	    1、需要判断一个字符是否是字母字符，首先需要提供一个字符数据
//	    2、字符是否为字母字符： 数字字符的范围 a - z 或者 A - Z 之间都属于字母字符，因此提供的字符只要大于或等于a，并且还要下于或等于z 或者 大于或等于A，并且还要下于或等于Z
//	    3、判断完成之后，打印判断的结果
        Scanner sc1=new Scanner(System.in);
        System.out.println("请输入一个字母");
        char number= sc1.next().charAt(0);
        if (number>=65 && number<=90 || number>=97 &&number<=122)
           System.out.println(number+"是字母");
        else
            System.out.println(number+"不是字母");

//   **3、判断指定的年份是否为闰年，请使用键盘录入**
//      1、闰年的判断公式为：能被4整除，但是不能被100整除 或者 能被400整除
//	    2、首先需要提供一个需要判断的年份，判断完成之后，打印判断的结果。int t;
        Scanner sc2=new Scanner(System.in);
        System.out.println("请输入年份");
        int year=sc2.nextInt();
       if(year%400==0||year%4==0 && year%100!=0) 
            System.out.println("是闰年");        
       else 
            System.out.println("不是闰年");
//        **4、判断一个数字是否为水仙花数,请使用键盘录入**
//        水仙花是指3位数字，表示的是每位上的数字的3次幂相加之后的和值和原数相等，则为水仙花数，
//      如：153  --->  1×1×*1 + 5*×5×*5 + 3×*3×3 = 153; 就是水仙花数
//		1、首先需要提供一个需要判断的3位数字，因此需要一个数值
//		2、判断的过程
//		a) 将3位数字的每一位上的数字拆分下来
//		b) 计算每位数字的3次幂之和
//		C) 用和值 和 原来的数字进行比较
//		D) 打印判断的比较结果即可
       Scanner sc=new Scanner(System.in);
        System.out.println("请输入一个三位数");
        int i= sc.nextInt();
        int x,y,z,a=0;
        y=i%10;
        x=i/10%10;
        z=i/100%10;
        if (i==y*y*y+x*x*x+z*z*z)
            System.out.println("是水仙花数");
        else
           System.out.println("不是水仙花数");        
//    **5、判断一个5位数字是否为回文数，使用键盘录入**
//       五位数的回文数是指最高位和最低位相等，次高位和次低位相等。如：12321 23732 56665
//	    1、首先需要提供一个需要判断的5位数字，因此需要一个数值
//	    2、判断的过程
//		a) 将5位数字的万、千、十、个位数拆分出来
//		b) 判断比较万位和个位 、 千位和十位是否相等
//	    3、判断完成之后，打印判断的结果。

Scanner sc3= new Scanner(System.in);
            int a=sc3.nextInt(),a1,a2,a3,a4,a5,a6;
            if(a>=10000 && a<=99999){
                a1 = a % 10;
                a2 = a / 10 % 10;
                a3 = a / 100 % 10;
                a4 = a / 1000%10;
                a5 = a /10000;
                a6= a1*10000+a2*1000+a3*100+a4*10+a5;
                if(a==a6){
                    System.out.println(a+"是回文数");
                }
                else{
                    System.out.println(a+"不是回文数");
                }
            }
```

