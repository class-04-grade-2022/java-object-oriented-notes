```java
public class Graphic {
    //- public double area()方法：返回0.0
    public double area() {
        return 0.0;
    }

    //- public double perimeter()方法：返回0.0
    public double perimeter() {
        return 0.0;
    }

    //- public String getInfo()方法，返回图形面积和图形周长
    public String getInfo() {
        String s = area() + "图形面积" + perimeter() + "图形周长";
        return s;
    }
}
class Circle extends Graphic{
    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    private double radius;

    @Override
    public double area() {
        return Math.PI*Math.pow(radius,2);
    }

    @Override
    public double perimeter() {
        return 2*Math.PI*radius;
    }

    @Override
    public String getInfo() {
        return radius+" 圆的半径 "+area()+" 圆的面积 "+perimeter()+" 圆的周长 ";
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
class Rectangle extends Graphic{
    public Rectangle() {
    }

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    private double length;
    private double width;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public double area() {
        return length*width;
    }

    @Override
    public double perimeter() {
        return (width+length)*2;
    }

    @Override
    public String getInfo() {
        return length+" 长 "+width+" 宽 "+area()+" 面积 "+perimeter()+" 周长 ";
    }
}
class Test{
    public static void main(String[] args) {
        Circle circle = new Circle(3);
        Rectangle rectangle = new Rectangle(3, 4);
        Rectangle rectangle1 = new Rectangle(3, 4);
        area(circle,rectangle);
        perimeter(rectangle,rectangle1);


    }
    static void area(Graphic a,Graphic b){
        if(a.perimeter()>b.perimeter()){
            System.out.println("第一个图形比第二个图形的面积大");
        }else if(b.perimeter()>a.perimeter()){
            System.out.println("第二个图形比第一个图形的面积大");
        }else System.out.println("两个图形一样大");
    }
    static void perimeter(Graphic a,Graphic b){
        if(a.perimeter()>b.perimeter()){
            System.out.println("第一个图形比第二个图形的周长大");
        }else if(b.perimeter()>a.perimeter()){
            System.out.println("第二个图形比第一个图形的周长大");
        }else System.out.println("两个图形一样大");
    }
}

```

