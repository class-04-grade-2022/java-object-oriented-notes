```java
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(true) {
            double a = 0;
            double b = 0;
            try {
                System.out.println("请输入第一个数");
                a = sc.nextDouble();
                System.out.println("请输入第二个数");
                b = sc.nextDouble();
            } catch (Exception e) {
                System.out.println("输入的数据类型有误");
                sc.next();
                break;
            }
            double c = a+b;
            System.out.println("和为:" + c);
            break;
        }
    }
}

```

