# 第5章 面向对象基础（上） 

## 实例变量练习题

### 1、圆类

（1）声明一个圆的图形类，包含实例变量/属性：半径

（2）在测试类的main中，创建2个圆类的对象，并给两个圆对象的半径属性赋值，最后显示两个圆的半径值、周长和面积值

​	提示：圆周率可以使用Math.PI

```java
public class cs1 {
    public static void main(String[] args) {
        Circle c = new Circle();
       c.setRadius(2.6);
       c.radius();
       c.perimeter();
       c.area();
        System.out.println();
        Circle c1 = new Circle(3.5);
        c1.radius();
        c1.perimeter();
        c1.area();
    }
}

```

```java
public class Circle {
    private double radius;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }

    public void radius(){
        System.out.println("半径值为:"+radius);
    }
    public void perimeter(){
        System.out.println("周长为:"+2* Math.PI*radius);
    }
    public void area(){
        System.out.printf("面积为:"+Math.PI*radius*radius);
    }
}
```

### 2、学生类

（1）声明一个学生类，包含实例变量/属性：姓名和成绩

（2）在测试类的main中，创建2个学生类的对象，并给两个学生对象的姓名和成绩赋值，最后输出显示

```java
public class cs2 {
    public static void main(String[] args) {
        Students s1 = new Students();
        s1.setName("小吴");
        s1.setScore(100);
        System.out.println(s1.toString());

        Students s2 = new Students("小薛",90);
        System.out.println(s2.toString());

    }
}

```

```java
public class Students {
    private String name;
    private int score;

    public Students() {
    }

    public Students(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return
                "name='" + name + '\'' +
                ", score=" + score
                ;
    }

}

```



### 3、日期和员工类

（1）声明一个MyDate类型，有属性：年，月，日

（2）声明另一个Employee类型，有属性：姓名（String类型），生日（MyDate类型）

（3）在测试类中的main中，创建两个员工对象，并为他们的姓名和生日赋值，并显示

```java
public class MyDate {
    private int year;
    private int month;
    private int day;

    public MyDate() {
    }

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @Override
    public String toString() {
        String s=year+"-"+month+"-"+day;
        return s;

    }

}

```

```java
public class Employee {
    private String name;
    private MyDate datetime;

    public Employee() {
    }

    public Employee(String name, MyDate datetime) {
        this.name = name;
        this.datetime = datetime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MyDate getDatetime() {
        return datetime;
    }

    public void setDatetime(MyDate datetime) {
        this.datetime = datetime;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", datetime=" + datetime +
                '}';
    }
    public void show(){
        System.out.println("姓名:"+ name+" "+"生日"+" "+datetime);
    }
}

```

```java
public class cs3 {
    public static void main(String[] args) {
        Employee e1 = new Employee();
        MyDate m1= new MyDate(2000,12,1);

        e1.setName("小袁");
        e1.setDatetime(m1);
        e1.show();
    }
}

```



## 实例方法的声明与调用练习

### 1、圆类改造

（1）声明一个圆的图形类，包含实例变量/属性：半径

（2）将圆求面积、求周长、返回圆对象信息分别封装为3个方法

double area()：求面积

double perimeter()：求周长

String getInfo()：返回圆对象信息，例如："半径：xx，周长：xx，面积：xx"

（3）在测试类的main中，创建2个圆类的对象，并给两个圆对象的半径属性赋值，最后显示两个圆的半径值、周长和面积值

​	提示：圆周率可以使用Math.PI

```java
public class Circle1 {
    private double radius;

    public Circle1() {
    }

    public Circle1(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle1{" +
                "radius=" + radius +
                '}';
    }
    public double area(){
        double d=Math.PI*radius*radius;
        return d;
    }
    public double perimeter(){
        double a=2*Math.PI*radius;
        return a;
    }
    public String getlnfo(){
        String s="半径："+ radius+"，"+"周长："+perimeter()+"，"+"面积："+ area();
        return s;
    }
}

```

```java
public class cs4 {
    public static void main(String[] args) {
        Circle1 c = new Circle1();
        c.setRadius(2.66);
         System.out.println(c.getlnfo());
        Circle1 c1 = new Circle1(3.5);
        System.out.println(c1.getlnfo());
    }
}

```

### 2、日期和员工类改造

（1）声明一个MyDate类型

- 有属性：年，月，日

- 增加一个String getDateInfo()方法，用于返回日期对象信息，例如：xx年xx月xx日

（2）声明另一个Employee类型，

- 有属性：姓名（String类型），生日（MyDate类型）

- 增加一个void setBirthday(int year, int month, int day)方法，用于给员工生日赋值
- 增加一个String getEmpInfo()方法，用于返回员工对象信息，例如：姓名：xx，生日：xx年xx月xx日

（3）在测试类中的main中，创建两个员工对象，并为他们的姓名和生日赋值，并显示

```java
package d;

public class MyDate {
    private int year;
    private int month;
    private int date;

    public MyDate() {
    }

    public MyDate(int year, int month, int date) {
        this.year = year;
        this.month = month;
        this.date = date;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    @Override
    public String toString() {
        String s=year+" "+"年"+month+" "+"月"+date+" "+"日";

        return s;
    }
    public String  getDateInfo(){
        String s=year+"年"+month+"月"+date+"日";
        return s;
    }
}

```

```java
package d;

public class Employee {
    private String name;
    private MyDate date;

    public Employee() {
    }

    public Employee(String name, MyDate date) {
        this.name = name;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MyDate getDate() {
        return date;
    }

    public void setDate(MyDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", date=" + date +
                '}';
    }
    public void setBirthday(int year, int month, int day){
       this.date=new MyDate(year,month,day);
    }
   public String getEmpInfo(){
        String s="姓名：" + name+" "+"生日"+date;
        return s;
   }
}

```

```java
package d;

public class cs5 {
    public static void main(String[] args) {
//        MyDate m = new MyDate(2012,11,20);

        Employee e = new Employee();
        e.setName("小吴");
        e.setBirthday(2012,11,20);
        System.out.println(e.getEmpInfo());

        Employee e1 = new Employee("小王",new MyDate(2000,10,25));


    }
}

```



### 3、MyInt类

（1）声明一个MyInt类，

- 包含一个int类型的value属性
- 包含一个方法boolean isNatural()方法，用于判断value属性值是否是自然数。自然数是大于等于0的整数。
- 包含一个方法int approximateNumberCount()方法，用于返回value属性值的约数个数。在[1, value]之间可以把value整除的整数都是value的约数。
- 包含一个方法boolean isPrimeNumber()方法，用于判断value属性值是否是素数。如果value值在[1, value]之间只有1和value本身两个约数，并且value是大于1的自然数，那么value就是素数。
- 包含一个方法int[] getAllPrimeNumber()方法，用于返回value属性值的所有约数。返回[1, value]之间可以把value整除的所有整数。

（2）测试类的main中调用测试

```java
package t6;

import java.util.Arrays;

public class Myint {
    private int value;

    public Myint() {
    }

    public Myint(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "MyInt{" +
                "\nvalue=" + value +
                "\n是不是自然数：" + isNatural() +
                "\n约数个数：" + approximateNumberCount() +
                "\n是不是素数：" + isPrimeNumber() +
                "\n所有的约数：" + Arrays.toString(getAllPrimeNumber()) +
                "\n}";
    }
    public boolean isNatural(){
        //自然数是大于等于0的整数
        // if(value>=0){
        //     return true;
        // }else{
        //     return false;
        // }
        return this.value>=0;
    }
    public int approximateNumberCount(){
        int count=0;
        for (int i=1;i<=value;i++){
            if (value%i==0){
                count++;
            }
        }
        return count;
    }
    public boolean isPrimeNumber(){
//            boolean flag=true;
//        if(approximateNumberCount()==2 && value>1){
//            return flag;
//        }else {
//            return flag;
//        }
        boolean flag = false;//是否是素数
        if (value>1 && isNatural()){  // 大于1的自然数
            flag=  approximateNumberCount()==2; // 约数个数：只有1和value本身两个约数
        }
        return flag;

    }
    public int[] getAllPrimeNumber(){
        int[] arr = new int[approximateNumberCount()];
        int index=0;// 数组初始存值的下标
        for (int i = 1; i <=value ; i++) {
            if (value%i==0){
                arr[index++]=i;

            }
        }

        return arr;
    }
}

```

```java
package t6;

public class cs6 {
    public static void main(String[] args) {
        Myint myInt = new Myint(10);
        System.out.println("myInt = " + myInt);
    }
}

```

## 参数练习

### 1、n个整数中的最小值和n个整数的最大公约数

（1）声明方法int min(int... nums)：返回n个整数中的最小值

（2）声明方法int maxApproximate(int... nums)：返回n个整数的最大公约数

```java
package t7;

public class Sz {
    private int nums;

    public Sz() {
    }

    public Sz(int nums) {
        this.nums = nums;
    }

    public int getNums() {
        return nums;
    }

    public void setNums(int nums) {
        this.nums = nums;
    }

    @Override
    public String toString() {
        return "sz{" +
                "nums=" + nums +
                '}';
    }

    public int min(int... nums) {
        int min = 100;
        for (int i = 0; i < nums.length; i++) {

            if (nums[i] < min) {
                min = nums[i];
            }

        }
        return min;

    }

    public int maxApproximate(int... nums) {
        int a=0;
        for (int i = 0; i < nums.length; i++) {


            while (nums[i] != nums[+1]) {
                if (nums[i] > nums[+1])
                    nums[i] -= nums[+1];
                else
                    nums[+1] -= nums[i];

            }
            a = nums[i];
        }
            return a;
    }
}



```

```java
package t7;

public class cs7 {
    public static void main(String[] args) {
        Sz s = new Sz();
        System.out.println(s.min(10,100,9,1));
        System.out.println(s.maxApproximate(12,16));
    }
}

```

### 2、判断程序运行结果

```java
public class Tools{
	public static void main(String[] args) {
		int i = 0;
		new Tools().change(i);
		i = i++;
		System.out.println("i = " + i);
	}

	void change(int i){
		i++;
	}
}
```

### 3、数组长度扩大2倍

（1）声明数组工具类ArraysTools

- 方法1：String toString(int[] arr)，遍历结果形式：[元素1，元素2，。。。]

- 方法2：int[] grow(int[] arr)，可以实现将一个数组扩大为原来的2倍


（2）在测试类的main中调用测试

```java
package t9;

import java.util.Arrays;

public class ArraysTools {
    private int []arr;
    private int []arr2;
    public ArraysTools() {
    }

    public ArraysTools(int[] arr) {
        this.arr = arr;
    }

    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }


    public String toString(int[]arr) {
        String s=  Arrays.toString(arr) ;
     return s;
    }
    public int[] grow(int[] arr){

        int[] arr2=new int[arr.length*2];
        for (int i=0;i<arr.length;i++){
            arr2[i]=arr[i];
        }
        arr=arr2;
        return arr;
    }
}

```

```java
package t9;

import java.util.Arrays;

public class cs9 {
    public static void main(String[] args) {
        int [] a={1,2,3,4,5};
        ArraysTools b = new ArraysTools();
        System.out.println(b.toString(a));
        System.out.println(Arrays.toString(b.grow((a))));

    }
}

```

## 方法重载练习

### 1、比较两个数大小关系

（1）声明MathTools工具类，包含：

- int compare(int a, int b)：比较两个整数大小关系，如果第一个整数比第二个整数大，则返回正整数，如果第一个整数比第二个整数小，则返回负整数，如果两个整数相等则返回0；
- int compare(double a, double b)：比较两个小数大小关系，如果第一个小数比第二个小数大，则返回正整数，如果第一个小数比第二个小数小，则返回负整数，如果两个小数相等则返回0；
- int compare(char a, char b)：比较两个字符大小关系，如果第一个字符比第二个字符编码值大，则返回正整数，如果第一个字符比第二个字符编码值小，则返回负整数，如果两个字符相等则返回0；

（2）在测试类的main方法中调用

```java
package t10;

public class MathTools {
    private int a;
    private int b;

    public MathTools() {
    }



    public MathTools(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    int compare(int a, int b){
        if (a>b){
            return a;
        } else if (a<b) {
            return b;
        }else {
            return 0;
        }
    }
    public int compare(double a, double b){
        if (a>b){
            return (int)a;
        } else if (a<b) {
            return (int)b;
        }else {
            return 0;
        }
    }
    public int compare(char a, char b){
        if (a>b){
            return (int)a;
        } else if (a<b) {
            return (int)b;
        }else {
            return 0;
        }
    }
    }


```

```java
package t10;

public class cs10 {
    public static void main(String[] args) {
        MathTools m = new MathTools();
        System.out.println(m.compare(12,13));
        System.out.println(m.compare(15,13));
        System.out.println(m.compare(12,12));

        System.out.println(m.compare(1.2,3.4));
        System.out.println(m.compare(1.2,1.2));

        System.out.println(m.compare('A','B'));
        System.out.println(m.compare('a','b'));
        System.out.println(m.compare('a','a'));


    }
}

```



### 2、数组排序和遍历

（1）声明一个数组工具类ArraysTools，包含几个重载方法

- 重载方法系列1：可以为int[]，double[]，char[]数组实现从小到大排序
  - void sort(int[] arr)
  - void sort(double[] arr)
  - void sort(char[] arr)

- 重载方法系列2：toString方法，可以遍历int[]，double[]，char[]数组，遍历结果形式：[元素1，元素2，。。。]
  - String toString(int[] arr)
  - String toString(double[] arr)
  - String toString(char[] arr)

（2）在测试类的main方法中调用

```java
package t11;

import java.util.Arrays;

public class ArraysTools {
    private int [] arr;

    public ArraysTools() {
    }

    public ArraysTools(int[] arr) {
        this.arr = arr;
    }

    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }

    @Override
    public String toString() {
        return "ArraysTools{" +
                "arr=" + Arrays.toString(arr) +
                '}';
    }
    public void sort(int[] arr){
        int a=0;
        for (int i=0;i<arr.length;i++){
            for (int j=0;j< arr.length-1;j++){
                if (arr[j]>arr[j+1]){
                    a=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=a;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
   public void sort(double[] arr){
       double a=0;
       for (int i=0;i<arr.length;i++){
           for (int j=0;j< arr.length-1;j++){
               if (arr[j]>arr[j+1]){
                   a=arr[j];
                   arr[j]=arr[j+1];
                   arr[j+1]=a;
               }
           }
       }
       System.out.println(Arrays.toString(arr));
    }
    public void sort(char[] arr){
        char a=0;
        for (int i=0;i<arr.length;i++){
            for (int j=0;j< arr.length-1;j++){
                if (arr[j]>arr[j+1]){
                    a=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=a;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
    public String toString(int[] arr){
//        String s=Arrays.toString(arr);
        String s="";
        for (int i=0;i< arr.length;i++){
            s +=+arr[i];
            if (i< arr.length-1){
                s +=",";
            }
        }
       return s;
    }
    public String toString(double[] arr){
        String s="";
        for (int i=0;i< arr.length;i++){
            s +=+arr[i];
            if (i< arr.length-1){
                s +=",";
            }
        }
        return s;
    }
    public String toString(char[] arr){
        String s = "[";
        for (int i = 0; i < arr.length; i++) {
            if(i<arr.length-1){
                s += arr[i] + ",";
            }else{
                s += arr[i] + "]";
            }
        }
        return s;
    }

    }


```

```java
package t11;

public class cs11 {
    public static void main(String[] args) {
        ArraysTools a = new ArraysTools();
        int []arr={4,3,6,7,2,3};
       a.sort(arr);
       double[]arr1={1.3,1.8,1.6,1.7,1.1};
       a.sort(arr1);
        char[]arr2={'b','w','a','s','l'};
        a.sort(arr2);


        System.out.println("["+a.toString(arr)+"]");
        System.out.println(( "["+a.toString(arr1)+"]"));
        System.out.println((a.toString(arr2)));


    }
}

```



### 3、求三角形面积

（1）声明一个图形工具类GraphicTools，包含两个重载方法

- 方法1：double triangleArea(double base ,double height)，根据底边和高，求三角形面积，
- 方法2：double triangleArea(double a,double b,double c)，根据三条边，求三角形面积，根据三角形三边求面积的海伦公式： 

![1577091140580](第5章 面向对象基础（上）.assets/1577091140580.png)

提示：Math.sqrt(x)，表示求x的平方根

（2）在测试类的main方法中调用

```java
package t12;

public class GraphicTools {
    private int a;
    private int b;
    private int c;
    private int base;
    private int height;

    public GraphicTools() {
    }

    public GraphicTools(int a, int b, int c, int base, int height) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.base = base;
        this.height = height;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double triangleArea(double base , double height){
        double a=base*height/2;
        return a;
   }
   public double triangleArea(double a,double b,double c){
        double p=(a+b+c)/2;
        double m=Math.sqrt(p*(p-a)*(p-b)*(p-c));
        return m;

   }
}

```

```java
package t12;

public class cs12 {
    public static void main(String[] args) {
        GraphicTools g= new GraphicTools();
        System.out.println(g.triangleArea(5,4));
        System.out.println(g.triangleArea(5,6,8));
    }
}

```

## 方法的递归调用练习

### 1、猴子吃桃

猴子吃桃子问题，猴子第一天摘下若干个桃子，当即吃了所有桃子的一半，还不过瘾，又多吃了一个。第二天又将仅剩下的桃子吃掉了一半，又多吃了一个。以后每天都吃了前一天剩下的一半多一个。到第十天，只剩下一个桃子。试求第一天共摘了多少桃子？

![1573725022751](第5章 面向对象基础（上）.assets/1573725022751.png)

```java
package t13;

public class hz {
    public static void main(String[] args) {
        int i,s=1;
        for(i=0;i<9;i++)
        {
            s=(s+1)*2;
        }
        System.out.println("第一天共摘了桃子为："+s);
        //在这里调用getPeath方法；通过传递参数可以获取每一天的桃子。
        System.out.println("第一天猴子摘了"+a(1)+"个桃");

    }
  public static int a(int day){
      int peach=0;
      //这里尽行约束，天数不能大于10或者小于0；
      if(day > 10 || day < 0) {
          System.out.println("我是猴子，桃子是什么东西？");
      }
      //我们已知的条件
      else if(day == 10) {
          peach =1;
      }else {
          //通过调用自己，体现递归思想
          peach = ((a(day+1)+1)*2);
      }
      return peach;

  }

    }


```



### 2、走台阶

有n级台阶，一次只能上1步或2步，共有多少种走法？

![1573724181996](第5章 面向对象基础（上）.assets/1573724181996.png)

```java
import java.util.Scanner;

public class t14 {
    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("请输入台阶个数是:");
            long n = scanner.nextLong();// 输入有多少节台阶
            System.out.println("共有: " + f(n) + " 种上台阶的方法");

        }


        //递归
        private static long f(long n) {
            if(n == 1) {
                return 1;
            }else if(n == 2) {
                return 2;
            }else {
                return (f(n-1)+f(n-2));
            }
        }
    }



```

### 3、求n!

![1573725058457](第5章 面向对象基础（上）.assets/1573725058457.png)

```java
package t15;


import java.util.Scanner;

public class n {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入n:");
        int n=sc.nextInt();
        int x=recursion(n);
        System.out.println(x);
    }
    //计算n的阶乘(n>0)
    public static int recursion(int num){//利用递归计算阶乘

        int sum=1;

        if(num < 0)

            throw new IllegalArgumentException("必须为正整数!");//抛出不合理参数异常

        if(num==1){

            return 1;//根据条件,跳出循环

        }else{

            sum=num * recursion(num-1);//运用递归计算

            return sum;

        }

    }




}

```

## 对象数组练习

### 1、学生对象数组

（1）定义学生类Student

- 声明姓名和成绩实例变量，

- String getInfo()方法：用于返回学生对象的信息


（2）测试类的main中创建一个可以装3个学生对象的数组，从键盘输入3个学生对象的信息，并且按照学生成绩排序，显示学生信息

```java
package t16;

import java.util.Arrays;

public class Student {
    private String name;
    private int score;

    public Student() {
    }

    public Student(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
     return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }
    public String getInfo(){
        String s="";
        for (int i = 0; i < 3 ; i++) {

                s = "姓名" +name +  "成绩"+score ;
            return s ;
            }
    return s;

    }
}

```

```java
package t16;

import java.util.Arrays;
import java.util.Scanner;

public class cs16 {
    public static void main(String[] args) {
      Student []arr=new Student[3];
        System.out.println("请输入:");
        Student s = new Student();
        Scanner sc=new Scanner(System.in);
        for (int i=0;i< arr.length;i++) {

                 arr[i] = new Student(sc.next(),sc.nextInt());

            }

        Student s1=null;//创建一个为空的对象进行数据交换
        //根据学生的成绩进行排序,这里运用冒泡排序
        for(int i=0;i<arr.length;i++) {
            for (int j =0; j < arr.length; j++) {
                if(j!=2)//这里必须加上这个条件，不然数字会报出越界的问题
                    if(arr[j].getScore()<arr[j+1].getScore()) {
                        s1=arr[j+1];
                        arr[j+1]=arr[j];
                        arr[j]=s1;
                    }
            }
        }


        for (int i = 0; i < arr.length; i++) {
            System.out.println("姓名"+arr[i].getName()+"\t"+"成绩"+arr[i].getScore());
        }

    }

}

```

### 2、请使用二维数组存储如下数据，并遍历显示

```java
		String[][] employees = {
		        {"10", "1", "段誉", "22", "3000"},
		        {"13", "2", "令狐冲", "32", "18000", "15000", "2000"},
		        {"11", "3", "任我行", "23", "7000"},
		        {"11", "4", "张三丰", "24", "7300"},
		        {"12", "5", "周芷若", "28", "10000", "5000"},
		        {"11", "6", "赵敏", "22", "6800"},
		        {"12", "7", "张无忌", "29", "10800","5200"},
		        {"13", "8", "韦小宝", "30", "19800", "15000", "2500"},
		        {"12", "9", "杨过", "26", "9800", "5500"},
		        {"11", "10", "小龙女", "21", "6600"},
		        {"11", "11", "郭靖", "25", "7100"},
		        {"12", "12", "黄蓉", "27", "9600", "4800"}
		    };
```

其中"10"代表普通职员，"11"代表程序员，"12"代表设计师，"13"代表架构师

![1561529559251](第5章 面向对象基础（上）.assets/1561529559251.png)



### 3、杨辉三角

* 使用二维数组打印一个 10 行杨辉三角.

  1

  1 1

  1 2 1

  1 3 3  1

  1 4 6  4  1

  1 5 10 10 5 1

   ....

* 开发提示

1. 第一行有 1 个元素, 第 n 行有 n 个元素

2. 每一行的第一个元素和最后一个元素都是 1

3. 从第三行开始, 对于非第一个元素和最后一个元素的元素. 

   ```
   yanghui[i][j] = yanghui[i-1][j-1] + yanghui[i-1][j];
   ```

   ![1558397196775](第5章 面向对象基础（上）.assets/1558397196775.png)



```java
package t18;

public class yhsj {

    public static void main(String[] args) {
        int [][] a= new int[10][];//初始化一个十行的二维数组，由于每列的列数不同，暂时无法确定，因此第二个[]内暂时不写;

        for(int i =0;i<10;i++) {
            a[i] = new int[i+1];//对内层数组初始化，规律可从图中分析发现，内层数组个数等于外层数组个数+1，即第0行有1个元素，第1行有两个元素
            a[i][0]=a[i][i]=1;//每行第一个元素和最后一个元素为1
            for(int j=1; j<a[i].length-1;j++) {//开始对内层除首尾为1以外的元素赋值
                a[i][j] = a[i-1][j-1]+a[i-1][j];//这里是杨辉三角的关系式
            }
            for(int j = 0; j<a[i].length;j++) {//开始遍历二维数组，输出元素

                System.out.print(a[i][j]+" ");
            }System.out.println();//每遍历完一轮内层循环（即完整的一行），输出回车
        }

    }

}
```
