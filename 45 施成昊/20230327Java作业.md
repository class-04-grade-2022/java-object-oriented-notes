```java
    import java.util.Scanner;

public class One {
    public static void main(String[] args) {
        one();
//        two();
//        three();
//        four();
//        five();
    }

public static void one(){
    Scanner sc=new Scanner(System.in);
    String x=sc.nextLine();
    char xy=x.charAt(0);
    /*
    if (x.length()==1) {
        if (xy >= 48 && xy <= 57) {
            System.out.println(xy);
        } else {
            System.out.println("不是数字");
        }
    }else {
        System.out.println("请输入一个字符");
    }

     */
// 第二个方法
    if (x.length()==1) {
        if (Character.isDigit(xy)) {
            System.out.println(xy);
        } else {
            System.out.println("不是数字");
        }
    }else{
        System.out.println("请输入一个字符");
    }
}
public static void two(){
    Scanner sc=new Scanner(System.in);
    String x=sc.nextLine();
    char xy=x.charAt(0);
    /*
    if (x.length()==1){
        if (xy>=65 && xy<=122){
            System.out.println(xy);
        }else {
            System.out.println(xy+"不是字母");
        }
    }else {
        System.out.println("请输入一个字符");
    }

     */
    //第二种方法
    if (Character.isLetter(xy)){
        System.out.println(xy);
    }else {
        System.out.println("不是字母");
    }
}
public static void three(){
    Scanner sc=new Scanner(System.in);
    int x=sc.nextInt();
    if ((x%4==0&&x%100!=0)||(x%400==0)){
        System.out.println(x+"是闰年");
    }else {
        System.out.println(x+"不是闰年");
    }
}
public static void four(){
    int sum=0;
    Scanner sc=new Scanner(System.in);
    int x= sc.nextInt();

    int g=x%10;
    int s=x/10%10;
    int b=x/100;
    if (g*g*g+s*s*s+b*b*b==x){
        System.out.println(x+"是水仙花数");
    }else {
        System.out.println(x+"不是水仙花数");
    }

}
public static void five(){
    Scanner sc=new Scanner(System.in);
    int x=sc.nextInt();
    int g=x%10;
    int s=x/10%10;
    int b=x/100%10;
    int q=x/1000%10;
    int w=x/10000;
    if (g==w&&s==q){
        System.out.println(x+"是回文数");
    }else {
        System.out.println(x+"不是回文数");
    }
}
```
}