```java
public abstract class Employee {
    String name;
    String workId;
    String dept;

    public abstract void showMsg();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public Employee(String name, String workId, String dept) {
        this.name = name;
        this.workId = workId;
        this.dept = dept;
    }

    public Employee() {
    }
}

    class Manager extends Employee {
        @Override
        public void showMsg() {
            System.out.println(getDept()+"的："+getName()+",员工编号："+getWorkId()+"\n他的职员是"+getClerk());
        }

        String Clerk;

        public String getClerk() {
            return Clerk;
        }

        public void setClerk(String clerk) {
            Clerk = clerk;
        }

        public Manager(String clerk,String name,String workId,String dept) {
            super(name,workId,dept);
            Clerk = clerk;
        }

        public Manager() {
        }
    }
    class Clerk extends Employee{
        @Override
        public void showMsg() {
            System.out.println(getDept()+"的："+getName()+",员工编号："+getWorkId()+"\n他的经理是"+getManager());
        }
        String Manager;

        public String getManager() {
            return Manager;
        }

        public void setManager(String manager) {
            Manager = manager;
        }

        public Clerk(String manager,String name,String workId,String dept) {
            super(name,workId,dept);
            Manager = manager;
        }

        public Clerk() {
        }
    }
    class Test{
        public static void main(String[] args) {
            new Manager("李小亮","张小强","M001", "销售部").showMsg();
            new Clerk("张小强","李小亮","C001","销售部").showMsg();

        }
    }
```