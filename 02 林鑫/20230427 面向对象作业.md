## 题目3

将以下描述信息转换为java代码：

1、定义员工Employee类,该类具有如下成员:

```java
(1) 属性：姓名(name,字符串类型)，工号(workId,字符串类型)，部门(dept,字符串类型),属性私有
(2) 方法:
        1. 空参数构造和满参数构造
        2. 提供属性的set/get方法
        3. 定义showMsg方法抽象方法
```



2、定义经理Manager类继承Employee类，该类具有如下成员:

```
(1) 属性:职员Clerk(该经理的职员)
(2) 方法:
        1. 空参数构造方法和满参数构造方法
        2. 属性的get和set方法
        3. 重写父类的showMsg方法，按照要求实现信息打印
```



3、定义职员Clerk类继承Employee类，该类具有如下成员:

```
(1) 属性:经理Manager(该职员的经理)
(2) 方法:
        1. 空参数构造方法和满参数构造方法
        2. 属性的get和set方法
        3. 重写父类的showMsg方法，按照要求实现信息打印
```



4、创建Test测试类，测试类中创建main方法，main方法中创建经理对象和职员对象，信息分别如下:  

```java
经理：工号为 M001,名字为 张小强，部门为 销售部
职员：工号为 C001,名字为 李小亮，部门为 销售部 

经理的职员为李小亮，职员的经理为张小强
```

分别调用经理的showMsg方法和职员的showMsg方法打印结果：

```java
销售部的：张小强，员工编号：M001
他的职员是李小亮
销售部的：李小亮，员工编号：C001
他的经理是张小强
```

```java
package TEXT;

public class text {
    public static void main(String[] args) {

        Clerk clerk = new Clerk("李小亮","C001","销售部");
        clerk.showMsq();
        Manager manager = new Manager("张小强","M001","销售部",
                clerk);
        manager.showMsq();
        System.out.println("经理的职员为:"+clerk.getName()+"职员的经理是"+manager.getName());

    }
}


    abstract class Employee{
        private String name;
        private String workid;
        private String dept;

        public Employee() {
        }

        public Employee(String name, String workid, String dept) {
            this.name = name;
            this.workid = workid;
            this.dept = dept;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getWorkid() {
            return workid;
        }

        public void setWorkid(String workid) {
            this.workid = workid;
        }

        public String getDept() {
            return dept;
        }

        public void setDept(String dept) {
            this.dept = dept;
        }
        public abstract void showMsq();
    }
    class Manager extends Employee{
       Clerk clerk;

        public Manager(String name, String workid, String dept, Clerk clerk) {
            super(name, workid, dept);
            this.clerk = clerk;
        }

        public Clerk getClerk() {
            return clerk;
        }

        public void setClerk(Clerk clerk) {
            this.clerk = clerk;
        }
        @Override
        public void showMsq() {
            System.out.println("经理：工号为 "+this.getWorkid()+
                    "名字为 "+this.getName()+"部门为"+this.getDept());
        }


    }

class Clerk extends Employee{
    Manager manager;
    @Override
    public void showMsq()
    {
        System.out.println("职员：工号为 "+this.getWorkid()+
                "名字为 "+this.getName()+"部门为"+this.getDept());
    }
    public Clerk() {}

    public Clerk(String name, String workId, String dept) {
        super(name, workId, dept);

    } public Clerk(String name, String workId, String dept, Manager manager) {
        super(name, workId, dept);
        this.manager = manager;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }
}




```
