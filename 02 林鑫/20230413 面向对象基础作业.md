# 作业

1、圆类

（1）声明一个圆的图形类，包含实例变量/属性：半径

（2）在测试类的main中，创建2个圆类的对象，并给两个圆对象的半径属性赋值，最后显示两个圆的半径值、周长和面积值

​ 提示：圆周率可以使用Math.PI



```java
public class Circle {
    private double radius;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {};

    @Override
    public String toString() {
        return "circle{" +
                "radius=" + radius +
                '}';
    }
}
```

```java
public class Circletext {
    public static void main(String[] args) {
        Circle circle = new Circle();
        circle.setRadius(5);
        System.out.println("圆的半径为："+circle.getRadius()+"圆的周长为："+2*Math.PI*circle.getRadius()+"圆的面积为："+Math.PI*(circle.getRadius()*circle.getRadius()));

        Circle circle1 = new Circle();
        circle1.setRadius(5.6);
        System.out.println("圆的半径为："+circle1.getRadius()+"圆的周长为："+2*Math.PI*circle1.getRadius()+"圆的面积为："+Math.PI*(circle1.getRadius()*circle1.getRadius()));



    }
}
```

2、学生类

（1）声明一个学生类，包含实例变量/属性：姓名和成绩

（2）在测试类的main中，创建2个学生类的对象，并给两个学生对象的姓名和成绩赋值，最后输出显示

```java
package student;

public class Stu {
   private String name;
   private double score;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getScore() {
        return score;

    }

    public void setScore(double score) {
        this.score = score;

    }

    public Stu(String name, double score) {
        this.name = name;
        this.score = score;


    }

    public Stu() {

    }

    @Override
    public String toString() {
        return "姓名："+name+"成绩:"+score;
    }
}
```

```java
package student;

public class Stutext {
    public static void main(String[] args) {
        Stu stu = new Stu("张三",985);
        System.out.println(stu.toString());

        Stu stu1 = new Stu("李四",211);
        System.out.println(stu1.toString());


    }
}
```

3、日期和员工类

（1）声明一个MyDate类型，有属性：年，月，日

（2）声明另一个Employee类型，有属性：姓名（String类型），生日（MyDate类型）

（3）在测试类中的main中，创建两个员工对象，并为他们的姓名和生日赋值，并显示

```java
public class Employee {

    private String name;
    private MyDate birthday;

    public Employee() {
    }

    public Employee(String name, MyDate birthday) {
        this.name = name;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MyDate getBirthday() {
        return birthday;
    }

    public void setBirthday(MyDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
```

```java
public class MyDate {

    private int year;
    private int month;
    private int day;

    public MyDate() {
    }
    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getMonth() {
        return month;
    }
    public void setMonth(int month) {
        this.month = month;
    }
    public int getDay() {
        return day;
    }
    public void setDay(int day) {
        this.day = day;
    }
    public String toString() {
        return year + "年" + month + "月" + day + "日";
    }
}
```

```java
public class Test3 {
    public static void main(String[] args) {
        Employee emp = new Employee();
        MyDate myDate = new MyDate();
        emp.setName("张三");
        myDate.setYear(2000);
        myDate.setMonth(5);
        myDate.setDay(13);
        System.out.println(emp);
    }
}
```

实例方法的声明与调用练习

### 1、圆类改造

（1）声明一个圆的图形类，包含实例变量/属性：半径

（2）将圆求面积、求周长、返回圆对象信息分别封装为3个方法

double area()：求面积

double perimeter()：求周长

String getInfo()：返回圆对象信息，例如："半径：xx，周长：xx，面积：xx"

（3）在测试类的main中，创建2个圆类的对象，并给两个圆对象的半径属性赋值，最后显示两个圆的半径值、周长和面积值

​ 提示：圆周率可以使用Math.PI

```java
package Circle;

import java.text.DecimalFormat;

public class Circle {
    private double radius;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double area(){
        return Math.PI*radius*radius;
    }

    public double perimeter(){
        return Math.PI*radius*2;
    }

    DecimalFormat df = new DecimalFormat("0.00");
    public String getInfo(){
        return "半径："+radius+"周长："+df.format(this.perimeter())+"面积："+df.format(this.area());
    }

    @Override
    public String toString() {
        return getInfo();
    }
}
```

```java
package Circle;

import Circle.Circle;

public class TestCirle {
    public static void main(String[] args) {
        Circle c = new Circle(5);
        System.out.println("圆:"+c);
    }
}
```

2、日期和员工类改造

（1）声明一个MyDate类型

* 有属性：年，月，日

* 增加一个String getDateInfo()方法，用于返回日期对象信息，例如：xx年xx月xx日

（2）声明另一个Employee类型，

* 有属性：姓名（String类型），生日（MyDate类型）

* 增加一个void setBirthday(int year, int month, int day)方法，用于给员工生日赋值

* 增加一个String getEmpInfo()方法，用于返回员工对象信息，例如：姓名：xx，生日：xx年xx月xx日

（3）在测试类中的main中，创建两个员工对象，并为他们的姓名和生日赋值，并显示

```java
package MyDate;

public class MyDate{
    int year;
    int month;
    int day;

    public MyDate() {
    }

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
    public String getInfo(){
        return year+"年"+month+"月"+day+"日";
    }

    @Override
    public String toString() {
        return getInfo();
    }
}
```

```java
package MyDate;

public class Employee {
    private String name;
    private MyDate birthday;

    public Employee() {
    }

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee(MyDate birthday) {
        this.birthday = birthday;
    }

    public MyDate getBirthday() {
        return birthday;
    }
    public void setBirthday(MyDate birthday){
        this.birthday = birthday;
    }
    public void setBirthday(int year, int month, int day){
        this.birthday = new MyDate(year,month,day);
    }

    public Employee(String name, MyDate birthday) {
        this.name = name;
        this.birthday = birthday;
    }

    public String getEmpInfo(){
        return "姓名："+getName()+" 生日："+ birthday;
    }
}
```

```java
package MyDate;

import MyDate.MyDate;

public class Test2 {
    public static void main(String[] args) {
        MyDate m = new MyDate(2000,1,23);
        System.out.println(m);
        Employee e =new Employee();
        e.setName("张三");
        e.setBirthday(2001,5,15);
        e.setBirthday(new MyDate(2000,1,1));
        System.out.println(e.getEmpInfo());
    }
}
```

3、MyInt类

（1）声明一个MyInt类，

* 包含一个int类型的value属性
* 包含一个方法boolean isNatural()方法，用于判断value属性值是否是自然数。自然数是大于等于0的整数。
* 包含一个方法int approximateNumberCount()方法，用于返回value属性值的约数个数。在[1, value]之间可以把value整除的整数都是value的约数。
* 包含一个方法boolean isPrimeNumber()方法，用于判断value属性值是否是素数。如果value值在[1, value]之间只有1和value本身两个约数，并且value是大于1的自然数，那么value就是素数。
* 包含一个方法int[] getAllPrimeNumber()方法，用于返回value属性值的所有约数。返回[1, value]之间可以把value整除的所有整数。

（2）测试类的main中调用测试

```java
package MyInt;

import java.util.Arrays;

public class MyInt {
    private int value;

    public MyInt(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }


    public MyInt() {
    }

    public boolean isNatural(){
        return this.value>=0;
    }
    public int approximateNumberCount(){
        int count = 0;
        for (int i = 0; i <= value; i++) {
            if (value%i==0){
                count++;
            }
        }
        return count;
    }
    public boolean isPrimeNumber() {
        boolean f = false;
        if (value > 1 && isNatural()) {
            f = approximateNumberCount() == 2;
        }return f;
    }
    public int[] getAllPrimeNumber(){
        int[] arr = new int[approximateNumberCount()];
        int index = 0;
        for (int i = 0; i <= value; i++) {
            if (value%i==0){
                arr[index++]=i;
            }
        }return arr;
    }

    @Override
    public String toString() {
        return "MyInt{" +
                "\nvalue=" + value +
                "\n是不是自然数：" + isNatural() +
                "\n约数个数：" + approximateNumberCount() +
                "\n是不是素数：" + isPrimeNumber() +
                "\n所有的约数：" + Arrays.toString(getAllPrimeNumber()) +
                "\n}";
    }
}
```

```java
package MyInt;

import MyInt.MyInt;

public class TestMyInt {
    public static void main(String[] args) {
        MyInt myInt = new MyInt(0);
        System.out.println("myInt = " + myInt);
    }
}
```

参数练习

### 1、n个整数中的最小值和n个整数的最大公约数

（1）声明方法int min(int... nums)：返回n个整数中的最小值

（2）声明方法int maxApproximate(int... nums)：返回n个整数的最大公约数



```Java

```

2、判断程序运行结果
    public class Tools{
        public static void main(String[] args) {
            int i = 0;
            new Tools().change(i);
            i = i++;
            System.out.println("i = " + i);
        }
        void change(int i){
            i++;
        }
    }

```java

```

3、数组长度扩大2倍

（1）声明数组工具类ArraysTools

* 方法1：String toString(int[] arr)，遍历结果形式：[元素1，元素2，。。。]

* 方法2：int[] grow(int[] arr)，可以实现将一个数组扩大为原来的2倍

（2）在测试类的main中调用测试

```java

```

方法重载练习

### 1、比较两个数大小关系

（1）声明MathTools工具类，包含：

* int compare(int a, int b)：比较两个整数大小关系，如果第一个整数比第二个整数大，则返回正整数，如果第一个整数比第二个整数小，则返回负整数，如果两个整数相等则返回0；
* int compare(double a, double b)：比较两个小数大小关系，如果第一个小数比第二个小数大，则返回正整数，如果第一个小数比第二个小数小，则返回负整数，如果两个小数相等则返回0；
* int compare(char a, char b)：比较两个字符大小关系，如果第一个字符比第二个字符编码值大，则返回正整数，如果第一个字符比第二个字符编码值小，则返回负整数，如果两个字符相等则返回0；

（2）在测试类的main方法中调用

```java
package MathTools;

public class MathTools {
    private int a;
    private int b;

    public MathTools() {
    }



    public MathTools(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    int compare(int a, int b){
        if (a>b){
            return a;
        } else if (a<b) {
            return b;
        }else {
            return 0;
        }
    }
    public int compare(double a, double b){
        if (a>b){
            return (int)a;
        } else if (a<b) {
            return (int)b;
        }else {
            return 0;
        }
        }
    public int compare(char a, char b){
        if (a>b){
            return (int)a;
        } else if (a<b) {
            return (int)b;
        }else {
            return 0;
        }
    }
}
```

```java
package MathTools;

public class TestMathTools {


    public static void main(String[] args) {
        MathTools m = new MathTools();
        System.out.println(m.compare(12,13));
        System.out.println(m.compare(15,13));
        System.out.println(m.compare(12,12));

        System.out.println(m.compare(1.2,3.4));
        System.out.println(m.compare(1.2,1.2));

        System.out.println(m.compare('A','B'));
        System.out.println(m.compare('a','b'));
        System.out.println(m.compare('a','a'));
    }
}
```

2、数组排序和遍历

（1）声明一个数组工具类ArraysTools，包含几个重载方法

* 重载方法系列1：可以为int[]，double[]，char[]数组实现从小到大排序
  
  * void sort(int[] arr)
  * void sort(double[] arr)
  * void sort(char[] arr)

* 重载方法系列2：toString方法，可以遍历int[]，double[]，char[]数组，遍历结果形式：[元素1，元素2，。。。]
  
  * String toString(int[] arr)
  * String toString(double[] arr)
  * String toString(char[] arr)

（2）在测试类的main方法中调用

```java
package ArraysTools2;

import java.util.Arrays;

public class ArraysTools {
private int [] arr;

    public ArraysTools() {
    }

    public ArraysTools(int[] arr) {
        this.arr = arr;
    }

    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }

    @Override
    public String toString() {
        return "ArraysTools{" +
                "arr=" + Arrays.toString(arr) +
                '}';
    }
    public void sort(int[] arr){
        int a=0;
        for (int i=0;i<arr.length;i++){
            for (int j=0;j< arr.length-1;j++){
                if (arr[j]>arr[j+1]){
                    a=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=a;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
    public void sort(double[] arr){
        double a=0;
        for (int i=0;i<arr.length;i++){
            for (int j=0;j< arr.length-1;j++){
                if (arr[j]>arr[j+1]){
                    a=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=a;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
    public void sort(char[] arr){
        char a=0;
        for (int i=0;i<arr.length;i++){
            for (int j=0;j< arr.length-1;j++){
                if (arr[j]>arr[j+1]){
                    a=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=a;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
    public String toString(int[] arr){
        String s=Arrays.toString(arr);
        String s= "";
        for (int i=0;i< arr.length;i++){
            s +=+arr[i];
            if (i< arr.length-1){
                s +=",";
            }
        }
        return s;
    }
    public String toString(double[] arr){
        String s= "";
        for (int i=0;i< arr.length;i++){
            s +=+arr[i];
            if (i< arr.length-1){
                s +=",";
            }
        }
        return s;
    }
    public String toString(char[] arr){
        String s = "";
        for (int i = 0; i < arr.length; i++) {
            if(i<arr.length-1){
                s += arr[i] + ",";
            }else{
                s += arr[i] + "]";
            }
        }
        return s;
    }

}
```

```java
package ArraysTools2;

public class TestArraysTools {
    public static void main(String[] args) {
        ArraysTools a = new ArraysTools();
        int []arr={4,3,6,7,2,3};
        a.sort(arr);
        double[]arr1={1.3,1.8,1.6,1.7,1.1};
        a.sort(arr1);
        char[]arr2={'b','w','a','s','l'};
        a.sort(arr2);
        System.out.println("["+a.toString(arr)+"]");
        System.out.println(( "["+a.toString(arr1)+"]"));
        System.out.println((a.toString(arr2)));
    }
}
```

3、求三角形面积

（1）声明一个图形工具类GraphicTools，包含两个重载方法

* 方法1：double triangleArea(double base ,double height)，根据底边和高，求三角形面积，
* 方法2：double triangleArea(double a,double b,double c)，根据三条边，求三角形面积，根据三角形三边求面积的海伦公式：

![1577091140580](/E:\JAVA\类和对象/第5章 面向对象基础（上）.assets/1577091140580.png)

提示：Math.sqrt(x)，表示求x的平方根

（2）在测试类的main方法中调用

```java
package GraphicTools;
public class GraphicTools {
    private int a;
    private int b;
    private int c;
    private int base;
    private int height;

    public GraphicTools() {
    }

    public GraphicTools(int a, int b, int c, int base, int height) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.base = base;
        this.height = height;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double triangleArea(double base , double height){
        double a=base*height/2;
        return a;
   }
   public double triangleArea(double a,double b,double c){
        double p=(a+b+c)/2;
        double m=Math.sqrt(p*(p-a)*(p-b)*(p-c));
        return m;

   }
}
```

```java
package GraphicTools;
public class  TestGraphicTools{
    public static void main(String[] args) {
        GraphicTools g= new GraphicTools();
        System.out.println(g.triangleArea(5,4));
        System.out.println(g.triangleArea(5,6,8));
    }
}
```

1、猴子吃桃

猴子吃桃子问题，猴子第一天摘下若干个桃子，当即吃了所有桃子的一半，还不过瘾，又多吃了一个。第二天又将仅剩下的桃子吃掉了一半，又多吃了一个。以后每天都吃了前一天剩下的一半多一个。到第十天，只剩下一个桃子。试求第一天共摘了多少桃子？

```java

```

2、走台阶

有n级台阶，一次只能上1步或2步，共有多少种走法？

```java
package tj;

import java.util.Scanner;

public class Tj{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入台阶个数是:");
        long n = scanner.nextLong();// 输入有多少节台阶
        System.out.println("共有: " + f(n) + " 种上台阶的方法");

    }

    private static long f(long n) {
        if(n == 1) {
            return 1;
        }else if(n == 2) {
            return 2;
        }else {
            return (f(n-1)+f(n-2));
        }
    }
}
```

3、求n!

```java
package n;

import java.util.Scanner;

public class N {
        public static void main(String[] args) {
            Scanner sc=new Scanner(System.in);
            System.out.println("请输入n:");
            int n=sc.nextInt();
            int x=recursion(n);
            System.out.println(x);
        }
            public static int recursion(int num){

            int sum=1;

            if(num < 0)

                throw new IllegalArgumentException("必须为正整数!");

            if(num==1){

                return 1;

            }else{

                sum=num * recursion(num-1);

                return sum;
            }
        }
}
```

对象数组练习

### 1、学生对象数组

（1）定义学生类Student

* 声明姓名和成绩实例变量，

* String getInfo()方法：用于返回学生对象的信息

（2）测试类的main中创建一个可以装3个学生对象的数组，从键盘输入3个学生对象的信息，并且按照学生成绩排序，显示学生信息

```java
package Student;

public class Student {
        private String name;
        private int score;

        public Student() {
        }

        public Student(String name, int score) {
            this.name = name;
            this.score = score;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", score=" + score +
                    '}';
        }
        public String getInfo(){
            String s="";
            for (int i = 0; i < 3 ; i++) {

                s = "姓名" +name +  "成绩"+score ;
                return s ;
            }
            return s;

        }
}
```

```java
package Student;

import java.util.Scanner;

public class TestStudent {
    public static void main(String[] args) {
        Student []arr=new Student[3];
        System.out.println("请输入:");
        Student s = new Student();
        Scanner sc=new Scanner(System.in);
        for (int i=0;i< arr.length;i++) {
            arr[i] = new Student(sc.next(),sc.nextInt());
        }

        Student s1=null;
        for(int i=0;i<arr.length;i++) {
            for (int j =0; j < arr.length; j++) {
                if(j!=2)
                    if(arr[j].getScore()<arr[j+1].getScore()) {
                        s1=arr[j+1];
                        arr[j+1]=arr[j];
                        arr[j]=s1;
            }
        }
    }
        for (int i = 0; i < arr.length; i++) {
            System.out.println("姓名"+arr[i].getName()+"\t"+"成绩"+arr[i].getScore());
        }

    }
}
```

2、请使用二维数组存储如下数据，并遍历显示

```java

```

3、杨辉三角

* 使用二维数组打印一个 10 行杨辉三角.11 11 2 11 3 3 11 4 6 4 11 5 10 10 5 1 ....

* 开发提示
1. 第一行有 1 个元素, 第 n 行有 n 个元素

2. 每一行的第一个元素和最后一个元素都是 1

3. 从第三行开始, 对于非第一个元素和最后一个元素的元素.
      yanghui[i][j] = yanghui[i-1][j-1] + yanghui[i-1][j];

4. ```java
   
   ```
