# 笔记

## 一、面向对象

三大特性：封装性，继承性，多态性

二、类与对象
------

一个类完成定义后，描述的只是一个广泛的概念，其具体操作必须通过对象来完成，格式如下：

`**类名称 对象名称 = new 类名称()**`

或者进行分步定义：

1. `类名称 对象名称 = null`
2. `对象名称 = new 类名称()`
   
   

# 三、封装：private

   能够在该类中进行调用，外部无法使用，此时应该提供getter，setter方法来获取和修改private属性。

四、this关键字
-------

this描述的是本类结构调用的关键字，在Java中this关键字可以描述3中结构的调用

1. 当前类中的属性：this.属性
2. 当前类中的方法（普通方法、构造方法）：this()、this.方法名()
3. 描述当前对象

# 作业

```java
public class employee {
    private int id;
    private  String name;
    private int age;
    private double salary;

    public void setId(int id){
        this.id=id;
    }
    public int getId(){
        return id;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }
    public void setAge(int age){
        this.age=age;
    }
    public int getAge(){
        return age;
    }
    public void setSalary(double salary){
        this.salary=salary;
    }
    public double getSalary(){
        return salary;
    }
    public employee(int i,String n,int a,double s){
        id = i;
        name = n;
        age = a;
        salary = s;
    }
    public employee(){};

    public String getInfo(){
        return id+","+"姓名:"+name+","+"年龄:"+age+","+"薪资:"+salary;
    }
}
```

```java
public class employees {
    public static void main(String[] args) {
        employee employee = new employee();
        employee.setId(1);
        employee.setName("张三");
        employee.setAge(23);
        employee.setSalary(10000.0);
        System.out.println("第一个员工的编号："+employee.getId()+","+"姓名:"+employee.getName()+","+"年龄:"+employee.getAge()+","+"薪资:"+employee.getSalary());

        employee employee1 = new employee(2,"李四",22,11000.0);
        System.out.println("第二个员工的编号："+employee1.getInfo());

    }
}
```
