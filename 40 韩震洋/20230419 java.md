````java
package com.q;

import java.util.ArrayList;

    public class ca {
    public static void main(String[] args) {
//        需求：创建一个存储字符串的集合，存储3个字符串元素，使用程序实现在控制台遍历该集合
ArrayList <String> list = new ArrayList<>();
list.add("撒的收到");
list.add("高危儿物管费");
list.add("和平光和热");
        for (int i = 0; i <list.size() ; i++) {
            String st = list.get(i);
            System.out.println(st);

        }

    }
}
package com.q1;

public class stu {
    private String stu;
    public stu(){

    }
    public stu(String stu){
        this.stu = stu;
    }
    public void setStu(String stu){
        this.stu = stu;
    }
    public String getStu(){
        return stu;
    }
    public String toString(){
        return "名字是"+stu;
    }

}

package com.q1;

import java.util.ArrayList;

public class teststu {
    public static void main(String[] args) {

        ArrayList <String> list =new ArrayList<>();
        stu s1 = new stu("武则天");
        stu s2 = new stu("王昭君");
        stu s3 = new stu("不知火舞");
        list.add(s1.toString());
        list.add(s2.toString());
        list.add(s3.toString());
        for (int i = 0; i <list.size() ; i++) {
            String a = list.get(i);
            System.out.println("a = " + a);

        }
    }
}
package com.q2;

public class stud {
//    需求：创建一个存储学生对象的集合，存储3个学生对象，使用程序实现在控制台遍历该集合
//            学生的姓名和年龄来自于键盘录入
    private String name;
    private String age;
    public stud(){

    }
    public stud(String name,String age){
        this.name = name;
        this.age =  age;
    }
    public void setName(String name){
        this.name = name ;
    }
    public void setAge(String age){
        this.age = age;
    }
    public String getName(){
        return name;
    }
    public String getAge(){
        return age;
    }
public String toString(){
        return "年龄为"+age+"姓名叫"+name;
}
}
package com.q2;

import java.util.ArrayList;

public class teststud {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        stud st = new stud("蔡文姬","10");
        stud s1 = new stud("大风车","18");
        stud stt = new stud();
        stt.setName("瑶瑶公主");
        stt.setAge("3");
        list.add(st.toString());
        list.add(stt.toString());
        list.add(s1.toString());
        for (int i = 0; i <list.size() ; i++) {
            String s = list.get(i);
            System.out.println("s = " + s);

        }

    }
}

package com.q3;

import java.util.ArrayList;

public class delete {
//    需求：创建一个存储String的集合，内部存储（test，张三，李四，test，test）字符串
//    删除所有的test字符串，删除后，将集合剩余元素打印在控制台
//    思路：
//    创建集合对象
//    调用add方法，添加字符串
//    遍历集合，取出每一个字符串元素
//    加入if判断，如果是test字符串，调用remove方法删除
//            打印集合元素
public static void main(String[] args) {
    ArrayList<String> list = new ArrayList<>();
    list.add("test");
    list.add("张三");
    list.add("李四");
    list.add("test");
    list.add("test");
    //    遍历集合，取出每一个字符串元素
//    加入if判断，如果是test字符串，调用remove方法删除
//            打印集合元素
    for (int i = 0; i <list.size() ; i++) {
        String s = list.get(i);
        System.out.println(s);
if (s.equals("test")){
    list.remove(s);
    System.out.println(list);
    i--;
}


    }
}

}

//定义一个方法，方法接收一个集合对象（泛型为Student），方法内部将年龄低于18的学生对象找出

//并存入新集合对象，方法返回新集合


import java.util.ArrayList;

public class Student {
    String name;
    int age;

    public Student(){

    }
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public static ArrayList list2(ArrayList<Student> list){
        ArrayList<Student> list2 = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).age<18){
                list2.add(list.get(i));
            }
        }
        return list2;
    }

}

```

```java
public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<>();
        Student s1 = new Student("张三",15);
        Student s2 = new Student("李四",10);
        Student s3 = new Student("王五",20);
        Student s4 = new Student("刘牛",17);
        Student s5 = new Student("李七",23);
        list.add(s1);
        list.add(s2);
        list.add(s3);
        list.add(s4);
        list.add(s5);
        System.out.println(Student.list2(list));
    }

````

