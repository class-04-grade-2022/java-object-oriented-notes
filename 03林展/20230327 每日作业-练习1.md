# 知识点

```
运算符 +加 -减 *乘 /除 %余

变量的定义
数据类型 名称 = 数据
if（条件，符号条件的才可以运行里面的函数）{函数}

else{函数} //除if内的所有剩下情况都跑这个函数，一般接在if后面
```

## 题目1(训练)

身高是具有遗传性的，子女的身高和父母的身高有一定的关系。假定，父母和子女的身高遗传关系如下：

​	儿子身高（厘米）＝(父亲身高＋母亲身高) ×1.08÷2

​	女儿身高（厘米）＝(父亲身高×0.923＋母亲身高) ÷2

现有父亲身高177 CM,母亲身高165 CM。求子女身高分别预计为多少？

```java
public class Text_01 {
    public static void main(String[] args) {
//身高是具有遗传性的，子女的身高和父母的身高有一定的关系。假定，父母和子女的身高遗传关系如下：
//儿子身高（厘米）＝(父亲身高＋母亲身高) ×1.08÷2
//女儿身高（厘米）＝(父亲身高×0.923＋母亲身高) ÷2
//现有父亲身高177CM,母亲身高165CM。求子女身高分别预计为多少？
        int f=177;
        int m=165;
        double man=(f+m)*1.08/2;
        double woman=(f*0.923+m)/2;
        System.out.println("儿子身高："+man);
        System.out.println("女儿身高："+woman);
    }
}
```



### 训练提示

1. 已知的父母身高如何用代码体现？

2. 题目中的公式如何转化为代码？

### 解题方案

1. 使用变量的定义和算术运算符完成本题

### 操作步骤

1. 定义小数变量代表父亲身高

2. 定义小数变量代表母亲身高

3. 通过儿子身高计算方式计算儿子身高

4. 通过女儿身高计算方式计算女人身高



## 题目2（训练）

红茶妹妹有21元钱，她攒了几天钱之后自己的钱比原来的两倍还多三块。绿茶妹妹有24元钱，她攒了几天钱之后自己的钱正好是原来的两倍。那么红茶和绿茶现在的钱一样多，请问对么？

### 训练提示

1. 用什么知识点来计算她们现在的钱有多少？
2. 如何对比两个人的钱数？

### 解题方案

1. 使用赋值运算符和算术运算符计算各自的钱，使用比较运算符对比大小

### 操作步骤

1. 定义红茶妹妹原来的钱为整数变量
2. 定义绿茶妹妹原来的钱为整数变量
3. 使用赋值运算符和算术运算符计算其现有的钱
4. 使用比较运算符对数值做出比较

```java
public class Text_02 {
    public static void main(String[] args) {
/*红茶妹妹有21元钱，
她攒了几天钱之后自己的钱比原来的两倍还多三块。
绿茶妹妹有24元钱，
她攒了几天钱之后自己的钱正好是原来的两倍。
那么红茶和绿茶现在的钱一样多，请问对么？*/
        int h0=21;
        int h1=h0*2+3;

        int l0=24;
        int l1=24*2;
        if (h1==l1){
            System.out.println("红茶妹妹和绿茶妹妹钱一样多，都是"+h1);
        }
        else{
            System.out.println("红茶妹妹钱和绿茶妹妹钱不一样多");
            System.out.println("红茶妹妹有"+h1);
            System.out.println("绿茶妹妹有"+l1);
        }
    }
}
```

## 题目3（综合）

某小伙想定一份外卖，商家的优惠方式如下：鱼香肉丝单点24元，油炸花生米单点8元，米饭单点3元。订单满30元8折优惠。鱼香肉丝优惠价16元，但是优惠价和折扣不能同时使用。那么这个小伙要点这三样东西，最少要花多少钱？

### 训练提示

1. 有两种购买方式，一种是不使用优惠价，另一种是使用优惠价。分别计算花费后，对两种方式的花费作对比。

### 解题方案

1. 使用算术运算符、赋值运算符和三元运算符联合完成本题

### 操作步骤

1. 使用算术运算符求出不使用优惠时的总价
2. 使用三元运算符判断总价是否满足打折条件，并求出折后总价
3. 使用算术运算符求出使用优惠价时的总价
4. 使用三元运算符判断最终更合算的购买方式和花费

### 参考答案

```java
public class Text_03 {
    public static void main(String[] args) {
/*某小伙想定一份外卖，商家的优惠方式如下：
鱼香肉丝单点24元，油炸花生米单点8元，米饭单点3元。
订单满30元8折优惠。鱼香肉丝优惠价16元，但是优惠价和折扣不能同时使用。
那么这个小伙要点这三样东西，最少要花多少钱？*/
        int y1=24;
        int y2=16;
        int h=8;
        int m=3;
        double a1=y1+h+m;
        int a2=y2+h+m;
        if (a1>30){
            a1=a1 * 0.8;
            if (a1>a2){
                System.out.println("至少花"+a2 );
            }else {
                System.out.println("至少花"+a1);
            }
        }
    }
}

```

# 判断一个字符数据是否是数字字符 

**分析：**

​	1、需要判断一个字符是否是数字字符，首先需要提供一个字符数据

​	2、字符是否为数字字符： 数字字符的范围 0 - 9 之间都属于数字字符，因此提供的字符只要大于或等于字符0，并且还要下于或等于字符9即可。

​	3、判断完成之后，打印判断的结果。

```java
import java.util.Scanner;

public class Text_04 {
    public static void main(String[] args) {
//1、判断一个字符数据是否是数字字符
        System.out.println("输入判断的数字");
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        if (a>=0 && a<=9){
            System.out.println("是数字字符");
        }
        else {
            System.out.println("不是数字字符");
        }
    }
}
```

# 判断一个字符数据是否是字母字符

**分析：**

​	1、需要判断一个字符是否是字母字符，首先需要提供一个字符数据

​	2、字符是否为字母字符： 数字字符的范围 a - z 或者 A - Z 之间都属于字母字符，因此提供的字符只要大于或等于a，并且还要下于或等于z 或者 大于或等于A，并且还要下于或等于Z

​	3、判断完成之后，打印判断的结果。

```java
import java.util.Scanner;

public class Text_05 {
    public static void main(String[] args) {
//        判断一个字符数据是否是字母字符
        Scanner sc=new Scanner(System.in);
        char a=sc.next().charAt(0);
        if (a>='a'|| a<='z' || a>='A' || a<='Z'){
            System.out.println("是字母字符");
        }
        else {
            System.out.println("不是字母字符");
        }
    }
}
```

# 判断指定的年份是否为闰年，请使用键盘录入 

**分析：**

​	1、闰年的判断公式为：能被4整除，但是不能被100整除 或者 能被400整除

​	2、首先需要提供一个需要判断的年份，判断完成之后，打印判断的结果。

```java
import java.util.Scanner;

public class Text_06 {
    public static void main(String[] args) {
//        判断指定的年份是否为闰年,能被4整除，但是不能被100整除 或者 能被400整除
        System.out.println("输入年份");
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        if (a%4==0 && a%100!=0 || a%400==0){
            System.out.println(a+"该年份是闰年");
        }else {
            System.out.println(a+"该年份不是是闰年");
        }
    }
}
```

# 判断一个数字是否为水仙花数,请使用键盘录入

水仙花是指3位数字，表示的是每位上的数字的3次幂相加之后的和值和原数相等，则为水仙花数，

**分析：**

​	如：153  --->  1×1×*1 + 5*×5×*5 + 3×*3×3 = 153; 就是水仙花数

​		1、首先需要提供一个需要判断的3位数字，因此需要一个数值

​		2、判断的过程 

​			a) 将3位数字的每一位上的数字拆分下来 

​			b) 计算每位数字的3次幂之和

​			C) 用和值 和 原来的数字进行比较 

​		D) 打印判断的比较结果即可

```java
import java.util.Scanner;

public class Text_07 {
    public static void main(String[] args) {
//        判断一个数字是否为水仙花数 153  --->  1×1×*1 + 5*×5×*5 + 3×*3×3 = 153
        System.out.println("输入数字");
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int a1=a/100;
        int a2=a%100/10;
        int a3=a%10;
        if (a1*a1*a1+a2*a2*a2+a3*a3*a3==a){
            System.out.println(a+"是水仙花数");
        }else {
            System.out.println(a+"不是水仙花数");
        }
    }
}

```

# 判断一个5位数字是否为回文数，使用键盘录入

五位数的回文数是指最高位和最低位相等，次高位和次低位相等。如：12321  23732  56665 

**分析：**

​	1、首先需要提供一个需要判断的5位数字，因此需要一个数值

​	2、判断的过程 

​		a) 将5位数字的万、千、十、个位数拆分出来

​		b) 判断比较万位和个位 、 千位和十位是否相等

​	3、判断完成之后，打印判断的结果。

```java
import java.util.Scanner;

public class Text_08 {
    public static void main(String[] args) {
//        判断一个5位数字是否为回文数 9999 10000 a 99999 100000
        System.out.println("输入数字");
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        if (a<=9999 || a>99999){
            System.out.println("你是故意找茬是不是");
        }

        if (a>9999 && a<=99999){
            int a1=a%10;
            int a2=a%100/10;
            int a3=a%1000/100;
            int a4=a%10000/1000;
            int a5=a/10000;

            if(a1==a5 && a2==a4){
                System.out.println(a+"为回文数");
        }else{
                System.out.println(a+"不为回文数");
            }
        }

    }
}

```

