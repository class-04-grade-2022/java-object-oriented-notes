```java
作业规范【必读】

命名要求：
	1. 类名，接口名，枚举名，注解名使用大驼峰
	2. 变量名，方法名，包名均使用小驼峰
	3. 常量名全大写，多个单词下划线分割
	4. 名字要见名知意，如果不知道对应的英文，可以使用拼音代替。不可使用无意义字符
  
代码规范：
	格式要良好，使用IDEA格式化缩进（快捷键：Ctrl+Alt+L）
  
答题规范：
	1. 每道题完整代码请贴入对应题目中的代码区。
    2. 如果有运行结果的，请把代码运行结果放到文档中
```



# 【继承】

## 题目1

我们计划为一个电器销售公司制作一套系统，公司的主要业务是销售一些家用电器，例如：电冰箱、洗衣机、电视机产品。

类的设计为：

```
冰箱类
	属性：品牌、型号、颜色、售价、门款式、制冷方式

洗衣机类
	属性：品牌、型号、颜色、售价、电机类型、洗涤容量

电视类
	属性：品牌、型号、颜色、售价、屏幕尺寸、分辨率
```

**要求：**

请根据上述类的设计，抽取父类“电器类”，并代码实现父类“电器类”、子类“冰箱类”，“洗衣机类”、“电视类”



答题：

```java
//我们计划为一个电器销售公司制作一套系统，公司的主要业务是销售一些家用电器，例如：电冰箱、洗衣机、电视机产品。
// 冰箱类RefrigeratorCategory
// 属性：品牌、型号、颜色、售价、  门款式 DoorStyle 、制冷方式 RefrigerationMethod
// 洗衣机类 WashingMachines
// 属性：品牌、型号、颜色、售价、  电机类型 MotorType、洗涤容量 WashingCapacity
// 电视类 TELEVISION
// 属性：品牌、型号、颜色、售价、  屏幕尺寸 ScreenSize、分辨率 ResolutionRatio
//父  品牌 brand、型号 model、颜色 color、售价 price
public class ElectricAppliance {
   String brand;
   String model;
   String color;
   int price;
}

class RefrigeratorCategory extends ElectricAppliance{ //门款式 DoorStyle 、制冷方式 RefrigerationMethod
    String DoorStyle;
    String RefrigerationMethod;
}

class WashingMachines extends ElectricAppliance{//电机类型 MotorType、洗涤容量 WashingCapacity
    String MotorType;
    String WashingCapacity;
}

class TELEVISION extends ElectricAppliance{//屏幕尺寸 ScreenSize、分辨率 ResolutionRatio
    String ScreenSize;
    String ResolutionRatio;
}
```



## 题目2

我们计划为一个动物园制作一套信息管理系统，根据与甲方沟通，我们归纳了有以下几种动物需要记录到系统中：

```java
鸟类：		 鹦鹉、猫头鹰、喜鹊
哺乳类：	大象、狼、长颈鹿
爬行类：	鳄鱼、蛇、乌龟
```



**要求：**

请根据以上需求，使用“继承”设计出三层的类结构

```java
|--动物类
    |--鸟类：
        |--鹦鹉类
        |--猫头鹰类
        |--喜鹊类
  
    |--哺乳类：
        |--大象类
        |--狼类
        |--长颈鹿类
  
    |--爬行类：
        |--鳄鱼类
        |--蛇类
        |--乌龟类
```

作为父类的类都应该定义一些共性内容，每种子类也都有一些特定的内容。 重点是类的层次结构，类成员简单表示即可。



**答案：**

```java
//|--动物类
class Animal{


}
//-----------------------------------------------
//    |--鸟类：Bird
//        |--鹦鹉类Parrot
//        |--猫头鹰类Owl
//        |--喜鹊类Magpie
//
class Bird extends Animal{

}
class Parrot extends Bird{
}
class Owl extends Bird{
}
class Magpie extends Bird{
}
//-----------------------------------------------
//    |--哺乳类：Mammals
//        |--大象类Elephant
//        |--狼类Wolf
//        |--长颈鹿类Giraffe
//
class Mammals extends Animal{

}

class Elephant extends Mammals{
}
class Wolf extends Mammals{
}
class Giraffe extends Mammals{
}
//-----------------------------------------------
//    |--爬行类：Reptiles
//        |--鳄鱼类Crocodile
//        |--蛇类Snake
//        |--乌龟类Turtle
class Reptiles extends Animal{

}
class Crocodile extends Reptiles{
}
class Snake extends Reptiles{
}
class Turtle extends Reptiles{
}
```