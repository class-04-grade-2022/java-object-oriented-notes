第一题

```Java
import java.lang.reflect.Array;
import java.util.ArrayList;

public class a {
    public static void main(String[] args) {
        //需求：创建一个存储字符串的集合，存储3个字符串元素，使用程序实现在控制台遍历该集合
        //思路：
        //创建集合对象
                //往集合中添加字符串对象
        //遍历集合，首先要能够获取到集合中的每一个元素，这个通过get(int index)方法实现
        //遍历集合，其次要能够获取到集合的长度，这个通过size()方法实现
                //遍历集合的通用格式

        //for(int i=0; i<集合对象.size(); i++) {集合对象.get(i)  就是指定索引处的元素}
        //for(int i=0; i<数组.length; i++) {数组名[索引];}
        ArrayList<String> a = new ArrayList<>();
        a.add("梦泪");
        a.add("韩信");
        a.add("真菜");
        for (int i = 0; i < a.size(); i++) {
            System.out.println(a.get(i));
        }
    }
}

```

第二题

```java
import java.util.ArrayList;

public class b {
    public static void main(String[] args) {
        //需求：创建一个存储学生对象的集合，存储3个学生对象，使用程序实现在控制台遍历该集合
        //思路：
        //定义学生类
        //创建集合对象
        //创建学生对象
        //添加学生对象到集合中
        //遍历集合，采用通用遍历格式实现
        ArrayList<Student> a = new ArrayList<>();
        Student stu1 = new Student("梦泪");
        Student stu2 = new Student("闪电");
        Student stu3 = new Student("元批");
        a.add(stu1);
        a.add(stu2);
        a.add(stu3);
        for (int i = 0; i < a.size(); i++) {
            System.out.println(a.get(i).getName());
        }
    }
}


```

```java
public class Student {
    private String name;

    public Student() {
    }

    public Student(String name) {
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }
}

```

第三题

```java
package adad;

public class Student {
    //需求：创建一个存储学生对象的集合，存储3个学生对象，使用程序实现在控制台遍历该集合
    //          学生的姓名和年龄来自于键盘录入
    //思路：
    //定义学生类，为了键盘录入数据方便，把学生类中的成员变量都定义为String类型
    //创建集合对象
    //键盘录入学生对象所需要的数据
    //创建学生对象，把键盘录入的数据赋值给学生对象的成员变量
    //往集合中添加学生对象
    //遍历集合，采用通用遍历格式实现
    private String name;
    private String age;

    public Student() {
    }

    public Student(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "c{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}

```

```java
package adad;

import java.util.ArrayList;
import java.util.Scanner;

public class c {
    public static void main(String[] args) {
        ArrayList<Student> a = new ArrayList<>();
        addStudent(a);
        addStudent(a);
        addStudent(a);
        for (int i = 0; i < a.size(); i++) {
            System.out.println("名字为:"+a.get(i).getName()+",年龄为:"+a.get(i).getAge());
        }

    }
    public static void addStudent(ArrayList<Student> a){
        Scanner sc = new Scanner(System.in);
        System.out.println("输入名字");
        String name = sc.next();
        System.out.println("输入年龄");
        String age = sc.next();
        Student stu = new Student();
        stu.setName(name);
        stu.setAge(age);
        a.add(stu);
    }
}


```

第四题

```java
package adad;

import java.util.ArrayList;

public class d {
    public static void main(String[] args) {
        //需求：创建一个存储String的集合，内部存储（test，张三，李四，test，test）字符串
        //删除所有的test字符串，删除后，将集合剩余元素打印在控制台
        //思路：
        //创建集合对象
        //调用add方法，添加字符串
        //遍历集合，取出每一个字符串元素
        //加入if判断，如果是test字符串，调用remove方法删除
        //打印集合元素
        ArrayList<String> a = new ArrayList<>();
        a.add("test");
        a.add("张三");
        a.add("李四");
        a.add("test");
        a.add("test");
        for (int i = 0; i < a.size(); i++) {
            if ("test".equals(a.get(i))) {
                a.remove(i);
                i--;
            }
        }
            System.out.println(a);
    }
}

```

第五题

```java
import java.util.ArrayList;

public class Student {
    String name;
    int age;

    public Student(){

    }
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public static ArrayList list2(ArrayList<Student> list){
        ArrayList<Student> list2 = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).age<18){
                list2.add(list.get(i));
            }
        }
        return list2;
    }

}
public static void main(String[] args) {
    //需求：定义一个方法，方法接收一个集合对象（泛型为Student），方法内部将年龄低于18的学生对象找出
    //并存入新集合对象，方法返回新集合。
    //思路：
//定义方法，方法的形参定义为ArrayList<Student> list
//方法内部定义新集合，准备存储筛选出的学生对象 ArrayList<Student> newList
//遍历原集合，获取每一个学生对象
//通过学生对象调用getAge方法获取年龄，并判断年龄是否低于18
//将年龄低于18的学生对象存入新集合
//返回新集合
//main方法中测试该方法

        ArrayList<Student> list = new ArrayList<>();
        Student s1 = new Student("张三",15);
        Student s2 = new Student("李四",10);
        Student s3 = new Student("王五",20);
        Student s4 = new Student("刘牛",17);
        Student s5 = new Student("李七",23);
        list.add(s1);
        list.add(s2);
        list.add(s3);
        list.add(s4);
        list.add(s5);
        System.out.println(Student.list2(list));
    }
```

