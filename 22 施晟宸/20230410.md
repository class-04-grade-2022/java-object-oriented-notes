1.笔记

类的定义
public class 类名 {
          // 成员变量
          变量1的数据类型 变量1；
          变量2的数据类型 变量2;
          …
          // 成员方法
          方法1;
          方法2;
          …	
}

对象的创建
    类名 对象名=new 类名();

注：测试对象的方法是main，所以类里面开头不要接main



2、作业

```java
1.定义工具类，可以用来求和，求最大值，最小值，给数组从小到大排序，给数组大到小排序。从数组中找一个元素的位置
    package 作业;

import java.util.Arrays;

public class Way {
    int[]arr;
    public Way(int[]arr){
        this.arr=arr;
    }
public void sum(int[] arr){
    int num=0;
    for (int i = 0; i < arr.length; i++) {
        num+=arr[i];
    }
    System.out.println("数组的和为"+num);
}
public void than(int[] arr){
    int max=this.arr[0];
    int min=this.arr[0];

    for (int i = 0; i < this.arr.length; i++) {
        if (this.arr[i]>=max){
            max=this.arr[i];
        } if (this.arr[i]<=min) {
            min=this.arr[i];
        }
    }
    System.out.println("数组的最大值为:"+max);
    System.out.println("数组的最小值为:"+min);
}
public void asc(int[]arr){
    for (int i = 0; i < arr.length-1; i++) {
        for (int j = 0; j < arr.length-1-i; j++) {
            if (arr[j]> arr[j+1]){
                int temp= arr[j];
                arr[j]= arr[j+1];
                arr[j+1]=temp;
            }
        }
    }
    System.out.println( Arrays.toString(arr));
    }



    public void desc(int []arr){
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = 0; j < arr.length-1; j++) {
                if (arr[j]< arr[j+1]){
                    int temp= arr[j+1];
                    arr[j+1]= arr[j];
                    arr[j]=temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
}

}

2.再定义一个测试类，测试上面的方法
package 作业;

import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        System.out.println("输入一个数组");
        int []arr =new int[5];
        for (int i = 0; i < arr.length; i++) {
            arr[i]= sc.nextInt();
        }
        System.out.println(Arrays.toString(arr));

        Way sz=new Way(arr);

        sz.sum(arr);
        sz.than(arr);
        sz.asc(arr);
        sz.desc(arr);

    }



}

```

