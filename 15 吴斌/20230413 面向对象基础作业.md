# 第5章 面向对象基础（上） 

## 实例变量练习题

### 1、圆类

（1）声明一个圆的图形类，包含实例变量/属性：半径

（2）在测试类的main中，创建2个圆类的对象，并给两个圆对象的半径属性赋值，最后显示两个圆的半径值、周长和面积值

​	提示：圆周率可以使用Math.PI

```java
package t1;

import java.text.DecimalFormat;

public class Round {
    private double radius;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Round() {
    }

    public Round(double radius) {
        this.radius = radius;
    }

    DecimalFormat df = new DecimalFormat("0.00");
    @Override
    public String toString() {
        return "这个圆的半径是：" + radius + ",周长是：" + df.format(2*radius*Math.PI )+ ",面积是：" + df.format(radius*radius*Math.PI);
    }
}
```

```java
package t1;

public class Main {
    public static void main(String[] args) {
        round round = new round();
        round.setRadius(5);
        System.out.println(round);

        round round1 = new round(4);
        System.out.println(round1);
    }
}
```

### 2、学生类

（1）声明一个学生类，包含实例变量/属性：姓名和成绩

（2）在测试类的main中，创建2个学生类的对象，并给两个学生对象的姓名和成绩赋值，最后输出显示

```java
package t2;

public class Student {
    private String name;
    private double score;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public Student() {
    }

    public Student(String name, double score) {
        this.name = name;
        this.score = score;
    }

    @Override
    public String toString() {
        return "姓名为："+name+"的同学这次的考试成绩是："+score;
    }
}
```

```java
package t2;

public class Main {
    public static void main(String[] args) {
        Student student = new Student();
        student.setName("杨旭东");
        student.setScore(61);
        System.out.println(student);

        Student student1 = new Student("施晟宸",59);
        System.out.println(student1);
    }
}
```



### 3、日期和员工类

（1）声明一个MyDate类型，有属性：年，月，日

（2）声明另一个Employee类型，有属性：姓名（String类型），生日（MyDate类型）

（3）在测试类中的main中，创建两个员工对象，并为他们的姓名和生日赋值，并显示

```java
package t3;

public class MyDate {
    private int year;
    private int month;
    private int day;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public MyDate() {
    }

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @Override
    public String toString() {
        return year+ "年" +month+ "月" + day + "日";
    }
}
```

```java
package t3;

public class Employee {
    private String name;
    private MyDate myDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MyDate getMyDate() {
        return myDate;
    }

    public void setMyDate(MyDate myDate) {
        this.myDate = myDate;
    }

    public Employee() {
    }

    public Employee(String name, MyDate myDate) {
        this.name = name;
        this.myDate = myDate;
    }

    @Override
    public String toString() {
        return name + "的生日是" + myDate;
    }
}
```

```java
package t3;

public class Main {
    public static void main(String[] args) {
        MyDate myDate = new MyDate();
        myDate.setYear(1994);
        myDate.setMonth(7);
        myDate.setDay(20);
        Employee employee = new Employee();
        employee.setName("及川彻");
        employee.setMyDate(myDate);
        System.out.println(employee);

        MyDate myDate1 = new MyDate(1996,10,10);
        Employee employee1 = new Employee("西谷夕",myDate1);
        System.out.println(employee1);
    }
}
```



## 实例方法的声明与调用练习

### 1、圆类改造

（1）声明一个圆的图形类，包含实例变量/属性：半径

（2）将圆求面积、求周长、返回圆对象信息分别封装为3个方法

double area()：求面积

double perimeter()：求周长

String getInfo()：返回圆对象信息，例如："半径：xx，周长：xx，面积：xx"

（3）在测试类的main中，创建2个圆类的对象，并给两个圆对象的半径属性赋值，最后显示两个圆的半径值、周长和面积值

​	提示：圆周率可以使用Math.PI

```java
package t4;

import java.text.DecimalFormat;

public class Round {
    private double radius;

    public double getRadius() {
        return radius;
    }

    public Round() {
    }

    public Round(double radius) {
        this.radius = radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double area(){
        return radius*radius*Math.PI;
    }

    public double perimeter(){
        return 2*radius*Math.PI;
    }
    DecimalFormat decimalFormat = new DecimalFormat("0.00");
    public String getInfo(){
        return "这个圆的半径是：" + radius + "周长是：" + decimalFormat.format(perimeter()) + "面积是：" + decimalFormat.format(area());
    }
    @Override
    public String toString() {
        return getInfo();
    }
}
```

```java
package t4;

public class Main {
    public static void main(String[] args) {
        Round round = new Round();
        round.setRadius(5);
        System.out.println(round);

        Round round1 = new Round(10);
        System.out.println(round1);
    }
}
```

### 2、日期和员工类改造

（1）声明一个MyDate类型

- 有属性：年，月，日

- 增加一个String getDateInfo()方法，用于返回日期对象信息，例如：xx年xx月xx日

（2）声明另一个Employee类型，

- 有属性：姓名（String类型），生日（MyDate类型）

- 增加一个void setBirthday(int year, int month, int day)方法，用于给员工生日赋值
- 增加一个String getEmpInfo()方法，用于返回员工对象信息，例如：姓名：xx，生日：xx年xx月xx日

（3）在测试类中的main中，创建两个员工对象，并为他们的姓名和生日赋值，并显示

```java
package t5;

public class Employee {
    private String name;
    private MyDate myDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MyDate getMyDate() {
        return myDate;
    }

    public void setMyDate(MyDate myDate) {
        this.myDate = myDate;
    }

    public void setBirthday(int year, int month, int day){

    }
    public Employee() {
    }

    public Employee(String name, MyDate myDate) {
        this.name = name;
        this.myDate = myDate;
    }


    public String getEmpInfo(){
        return "姓名："+ getName() + "，生日：" + myDate;
    }

    @Override
    public String toString() {
        return getEmpInfo();
    }
}
```

```java
package t5;

public class MyDate {
    private int year;
    private int month;
    private int day;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public MyDate() {
    }

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public String getDateInfo(){
        return year+ "年" +month+ "月" + day + "日";
    }

    @Override
    public String toString() {
        return getDateInfo();
    }
}
```

```java
package t5;

public class Main {
    public static void main(String[] args) {
        MyDate myDate = new MyDate();
        myDate.setYear(1994);
        myDate.setMonth(7);
        myDate.setDay(20);
        Employee employee = new Employee();
        employee.setName("及川彻");
        employee.setMyDate(myDate);
        System.out.println(employee.getEmpInfo());

        MyDate myDate1 = new MyDate(1996,10,10);
        Employee employee1 = new Employee();
        employee1.setName("西谷夕");
        employee1.setMyDate(myDate1);
        System.out.println(employee1.getEmpInfo());
    }
}
```



### 3、MyInt类

（1）声明一个MyInt类，

- 包含一个int类型的value属性
- 包含一个方法boolean isNatural()方法，用于判断value属性值是否是自然数。自然数是大于等于0的整数。
- 包含一个方法int approximateNumberCount()方法，用于返回value属性值的约数个数。在[1, value]之间可以把value整除的整数都是value的约数。
- 包含一个方法boolean isPrimeNumber()方法，用于判断value属性值是否是素数。如果value值在[1, value]之间只有1和value本身两个约数，并且value是大于1的自然数，那么value就是素数。
- 包含一个方法int[] getAllPrimeNumber()方法，用于返回value属性值的所有约数。返回[1, value]之间可以把value整除的所有整数。

（2）测试类的main中调用测试

```java
package t6;

import java.util.Arrays;

public class MyInt  {
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public MyInt() {
    }

    public MyInt(int value) {
        this.value = value;
    }

    public boolean isNatural(){
        return this.value>=0;
    }

    public  int approximateNumberCount(){
        int ge=0;
        for (int i = 1; i <= value; i++) {
            if(value%i==0){
                ge++;
            }
        }
        return ge;
    }

    public boolean isPrimeNumber() {
        boolean flag = false;
        if (value>1 && isNatural()){
            flag=  approximateNumberCount()==2;
        }
        return flag;
    }

    public int[] getAllPrimeNumber(){
        int[] arr = new int[approximateNumberCount()];
        int index=0;
        for (int i = 1; i <=value ; i++) {
            if (value%i==0){
                arr[index++]=i;
            }
        }
        return arr;
    }

    public String toString() {
        return "MyInt{" +
                "\nvalue=" + value +
                "\n是不是自然数：" + isNatural() +
                "\n约数个数：" + approximateNumberCount() +
                "\n是不是素数：" + isPrimeNumber() +
                "\n所有的约数：" + Arrays.toString(getAllPrimeNumber()) +
                "\n}";
    }
}
```

```java
package t6;

public class Main {
    public static void main(String[] args) {
        MyInt myInt = new MyInt(233);
        System.out.println("myInt = " + myInt);
    }
}
```

## 参数练习

### 1、n个整数中的最小值和n个整数的最大公约数

（1）声明方法int min(int... nums)：返回n个整数中的最小值

（2）声明方法int maxApproximate(int... nums)：返回n个整数的最大公约数

```java
package t7;

public class Text {
    public static void main(String[] args) {
        System.out.println(min(15, 10, 20, 55, 80));
        int x=maxApproximate(15,10,20,55,80);
        System.out.println(x);
    }
    public static int min(int... nums){
        int min=nums[0];
        for (int i = 0; i < nums.length; i++) {
            if (nums[i]<min)
                min=nums[i];
        }
       // System.out.println(min);
        return min;
    }
    public static int maxApproximate(int... nums){
        int min=min(nums);
        int maxApp=0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i]%min==0){
                maxApp=min;
            }else {
                maxApp = 1;
                break;
            }
        }
        //System.out.println(maxApproximate);
        return maxApp;
    }
}
```

```java

```

### 2、判断程序运行结果

```java
public class Tools{
	public static void main(String[] args) {
		int i = 0;
		new Tools().change(i);
		i = i++;
		System.out.println("i = " + i);
	}

	void change(int i){
		i++;
	}
}

错
i=0
```

### 3、数组长度扩大2倍

（1）声明数组工具类ArraysTools

- 方法1：String toString(int[] arr)，遍历结果形式：[元素1，元素2，。。。]

- 方法2：int[] grow(int[] arr)，可以实现将一个数组扩大为原来的2倍


（2）在测试类的main中调用测试

```java
package t8;

import java.util.Arrays;

public class ArraysTools {

    public String toString(int[] arr){
        return Arrays.toString(arr);
    }
    public int[] grow(int[] arr){
        int x[]=new int[arr.length*2];
        for (int i = 0; i < arr.length; i++) {
            x[i]=arr[i];
        }
        return x;
    }
}
```

```java
package t8;

public class Main {
    public static void main(String[] args) {
        ArraysTools a=new ArraysTools();
        int x[]={1,2,3,4,5,6};
        System.out.println(a.toString(x));
        System.out.println("------");
        int arr[]=a.grow(x);
        x=arr;
        System.out.println(a.toString(x));
        System.out.println(x.length);
    }
}
```

## 方法重载练习

### 1、比较两个数大小关系

（1）声明MathTools工具类，包含：

- int compare(int a, int b)：比较两个整数大小关系，如果第一个整数比第二个整数大，则返回正整数，如果第一个整数比第二个整数小，则返回负整数，如果两个整数相等则返回0；
- int compare(double a, double b)：比较两个小数大小关系，如果第一个小数比第二个小数大，则返回正整数，如果第一个小数比第二个小数小，则返回负整数，如果两个小数相等则返回0；
- int compare(char a, char b)：比较两个字符大小关系，如果第一个字符比第二个字符编码值大，则返回正整数，如果第一个字符比第二个字符编码值小，则返回负整数，如果两个字符相等则返回0；

（2）在测试类的main方法中调用

```java
package t9;

public class MathTools {
    int x;
    public int compare(int a, int b){
        if (a>b){
            x=1;
        } else if (a<b) {
            x=-1;
        }else
            x=0;
        return x;
    }

    public int compare(double a, double b){
        if (a>b){
            x=1;
        } else if (a<b) {
            x=-1;
        }else
            x=0;
        return x;
    }

    public int compare(char a, char b){
        if (a>b){
            x=1;
        } else if (a<b) {
            x=-1;
        }else
            x=0;
        return x;
    }
}
```

```java
package t9;

public class Main {
    public static void main(String[] args) {
        MathTools mathTools = new MathTools();
        mathTools.compare(10,20);
        System.out.println(mathTools.x);

        MathTools mathTools1 = new MathTools();
        mathTools1.compare(20,10);
        System.out.println(mathTools1.x);

        MathTools mathTools2 = new MathTools();
        mathTools2.compare('0', '0');
        System.out.println(mathTools2.x);
    }
}
```



### 2、数组排序和遍历

（1）声明一个数组工具类ArraysTools，包含几个重载方法

- 重载方法系列1：可以为int[]，double[]，char[]数组实现从小到大排序
  - void sort(int[] arr)
  - void sort(double[] arr)
  - void sort(char[] arr)

- 重载方法系列2：toString方法，可以遍历int[]，double[]，char[]数组，遍历结果形式：[元素1，元素2，。。。]
  - String toString(int[] arr)
  - String toString(double[] arr)
  - String toString(char[] arr)

（2）在测试类的main方法中调用

```java
package t10;

import java.util.Arrays;

public class ArraysTools {
    private int [] arr;

    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }
    public ArraysTools() {
    }

    public ArraysTools(int[] arr) {
        this.arr = arr;
    }

    @Override
    public String toString() {
        return "ArraysTools{" +
                "arr=" + Arrays.toString(arr) +
                '}';
    }
    public void sort(int[] arr){
        int x=0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if (arr[i]>arr[j]){
                    x=arr[j];
                    arr[j]=arr[i];
                    arr[i]=x;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }

    public void sort(double[] arr){
        double x=0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if (arr[i]>arr[j]){
                    x=arr[j];
                    arr[j]=arr[i];
                    arr[i]=x;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }

    public void sort(char[] arr){
        char x=0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if (arr[i]>arr[j]){
                    x=arr[j];
                    arr[j]=arr[i];
                    arr[i]=x;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }

    public String toString(int[] arr){
//        String s=Arrays.toString(arr);
        String s="";
        for (int i=0;i< arr.length;i++){
            s +=+arr[i];
            if (i< arr.length-1){
                s +=",";
            }
        }
        return s;
    }
    public String toString(double[] arr){
        String s="";
        for (int i=0;i< arr.length;i++){
            s +=+arr[i];
            if (i< arr.length-1){
                s +=",";
            }
        }
        return s;
    }
    public String toString(char[] arr){
        String s = "[";
        for (int i = 0; i < arr.length; i++) {
            if(i<arr.length-1){
                s += arr[i] + ",";
            }else{
                s += arr[i] + "]";
            }
        }
        return s;
    }
}
```

```java
package t10;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        ArraysTools a = new ArraysTools();
        int []arr = {3,1,2};
        a.sort(arr);

        double []arr1={3.1,1.1,2.1,5.1,4.1};
        a.sort(arr1);

        char []arr2={'x','t','h','a'};
        a.sort(arr2);

        ArraysTools a3 = new ArraysTools();
        a3.toString(new int[]{3,1,2});
        System.out.println();

        System.out.println(a.toString(arr));
        System.out.println(a.toString(arr1));
        System.out.println(a.toString(arr2));
    }
}
```



### 三、求三角形面积

（1）声明一个图形工具类GraphicTools，包含两个重载方法

- 方法1：double triangleArea(double base ,double height)，根据底边和高，求三角形面积，
- 方法2：double triangleArea(double a,double b,double c)，根据三条边，求三角形面积，根据三角形三边求面积的海伦公式： 

![1577091140580](第5章 面向对象基础（上）.assets/1577091140580.png)

提示：Math.sqrt(x)，表示求x的平方根

（2）在测试类的main方法中调用

```java
package t11;

public class GraphicTools {
    double x;
    public double triangleArea(double base ,double height){
        x=base*height/2;
        return x;
    }

    public double triangleArea(double a,double b,double c){
        double p = (a+b+c)/2;
        x=Math.sqrt(p*(p-a)*(p-b)*(p-c));
        return x;
    }
}
```

```java
package t11;

public class Main {
    public static void main(String[] args) {
        GraphicTools g = new GraphicTools();
        g.triangleArea(10, 12);
        System.out.println(g.x);

        GraphicTools g2 = new GraphicTools();
        g2.triangleArea(13, 13, 10);
        System.out.println(g2.x);
    }
}
```

## 方法的递归调用练习

### 1、猴子吃桃

猴子吃桃子问题，猴子第一天摘下若干个桃子，当即吃了所有桃子的一半，还不过瘾，又多吃了一个。第二天又将仅剩下的桃子吃掉了一半，又多吃了一个。以后每天都吃了前一天剩下的一半多一个。到第十天，只剩下一个桃子。试求第一天共摘了多少桃子？

![1573725022751](第5章 面向对象基础（上）.assets/1573725022751.png)

```java

```



### 2、走台阶

有n级台阶，一次只能上1步或2步，共有多少种走法？

![1573724181996](第5章 面向对象基础（上）.assets/1573724181996.png)

```java

```

### 3、求n!

![1573725058457](第5章 面向对象基础（上）.assets/1573725058457.png)

```java

```

## 对象数组练习

### 1、学生对象数组

（1）定义学生类Student

- 声明姓名和成绩实例变量，

- String getInfo()方法：用于返回学生对象的信息


（2）测试类的main中创建一个可以装3个学生对象的数组，从键盘输入3个学生对象的信息，并且按照学生成绩排序，显示学生信息

```java

```

```java

```

### 2、请使用二维数组存储如下数据，并遍历显示

```java
		String[][] employees = {
		        {"10", "1", "段誉", "22", "3000"},
		        {"13", "2", "令狐冲", "32", "18000", "15000", "2000"},
		        {"11", "3", "任我行", "23", "7000"},
		        {"11", "4", "张三丰", "24", "7300"},
		        {"12", "5", "周芷若", "28", "10000", "5000"},
		        {"11", "6", "赵敏", "22", "6800"},
		        {"12", "7", "张无忌", "29", "10800","5200"},
		        {"13", "8", "韦小宝", "30", "19800", "15000", "2500"},
		        {"12", "9", "杨过", "26", "9800", "5500"},
		        {"11", "10", "小龙女", "21", "6600"},
		        {"11", "11", "郭靖", "25", "7100"},
		        {"12", "12", "黄蓉", "27", "9600", "4800"}
		    };
```

其中"10"代表普通职员，"11"代表程序员，"12"代表设计师，"13"代表架构师

![1561529559251](第5章 面向对象基础（上）.assets/1561529559251.png)



### 3、杨辉三角

* 使用二维数组打印一个 10 行杨辉三角.

  1

  1 1

  1 2 1

  1 3 3  1

  1 4 6  4  1

  1 5 10 10 5 1

   ....

* 开发提示

1. 第一行有 1 个元素, 第 n 行有 n 个元素

2. 每一行的第一个元素和最后一个元素都是 1

3. 从第三行开始, 对于非第一个元素和最后一个元素的元素. 

   ```
   yanghui[i][j] = yanghui[i-1][j-1] + yanghui[i-1][j];
   ```

   ![1558397196775](第5章 面向对象基础（上）.assets/1558397196775.png)





