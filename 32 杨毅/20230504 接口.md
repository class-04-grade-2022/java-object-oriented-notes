## 接口题目2

1. 请定义“员工(类)”：
   属性：姓名、性别、年龄(全部私有)
   行为：工作(抽象)
   无参、全参构造方法
   get/set方法

2. 请定义“绘画(接口)”
   抽象方法：绘画
3. 请定义“Java讲师类”继承自“员工类”
4. 请定义”UI讲师类”，继承自“员工类”，并实现“绘画”接口。

**要求**：

1. 请按上述要求设计出类结构，并实现相关的方法。
2. 测试类中创建对象测试，属性可自定义：
   - 创建一个Java讲师类对象，调用工作的方法。
   - 创建一个UI讲师类对象，调用工作方法，和绘画方法。

**答案：**

```java
package Emp;

public abstract class Emp {
    private String name;
    private String sex;
    private int age;

    public Emp(String name, String sex, int age) {
        this.name = name;
        this.sex = sex;
        this.age = age;
    }

    public Emp() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public abstract void play();

    @Override
    public String toString() {
        return "Emp{" +
                "name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                '}';
    }
}



package Emp;

public interface draw {
    void draw();
}



package Emp;

public class JavaTea extends Emp{
    public JavaTea(String name, String sex, int age) {
        super(name, sex, age);
    }

    public JavaTea() {
    }

    @Override
    public void play() {
        System.out.println(getName()+"讲Java课");
    }

}



package Emp;

public class UiTea extends Emp implements draw {
    public UiTea(String name, String sex, int age) {
        super(name, sex, age);
    }

    public UiTea() {
    }

    @Override
    public void play() {
        System.out.println(getName()+"讲ui设计课");
    }

    @Override
    public void draw() {
        System.out.println(getName() + "画画");
    }
}



package Emp;

public class test {
    public static void main(String[] args) {
        JavaTea jt = new JavaTea("小明","男",25);
        UiTea uj = new UiTea("小红","女",18);
        jt.toString();
        jt.play();
        uj.toString();
        uj.play();
        uj.draw();
    }
}


```



**运行结果：**

```java
JavaTeacher{name='小明', gender='男', age=25}
小明讲Java课
UITeacher{name='小红', gender='女', age=18}
小红讲UI设计课
小红画画
```



