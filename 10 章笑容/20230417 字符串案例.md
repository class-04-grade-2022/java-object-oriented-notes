## 1、用户登录

```java
import java.util.Scanner;

public class Test01 {
    // 用户登录
    // 需求：已知用户名和密码，请用程序实现模拟用户登录。总共给三次机会，登录之后，给出相应的提示
    public static void main(String[] args) {
        String userName = "mockQ";
        String userPass = "123456mockQ";
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.print("输入你的用户名：");
            String inName = sc.next();
            System.out.print("输入你的密码统计字符次数
            String inPass = sc.next();
            if (inName.equals(userName) && inPass.equals(userPass)) {
                System.out.println("登录成功");
                break;
            } else {
                System.out.println("密码错误，还有 " + (2 - i) + " 次机会");
            }
        }
    }
}

```

## 2、遍历字符串

```java
import java.util.Arrays;
import java.util.Scanner;

public class Test02 {
    // 遍历字符串
    // 需求：键盘录入一个字符串，使用程序实现在控制台遍历该字符串
    // 思路：
    //键盘录入一个字符串，用 Scanner 实现
    //遍历字符串，首先要能够获取到字符串中的每一个字符
    // public char charAt(int index)：返回指定索引处的char值，字符串的索引也是从0开始的
    //遍历字符串，其次要能够获取到字符串的长度
    // public int length()：返回此字符串的长度
    // 数组的长度：数组名.length
    // 字符串的长度：字符串对象.length()
    //遍历
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入一串字符:");
        String str = sc.next();
        char[] arr = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            arr[i] = str.charAt(i);
        }
        System.out.println(Arrays.toString(arr));
                // 思路：
        //键盘录入一个字符串，用 Scanner 实现
        //将字符串拆分为字符数组
        // public char[] toCharArray( )：将当前字符串拆分为字符数组并返回
        //遍历字符数组
        System.out.println(Arrays.toString(str.toCharArray()));
    }
}

```

## 3、统计字符次数

```java
import java.util.Scanner;

public class Test03 {
    // 统计字符次数
    // 需求：键盘录入一个字符串，统计该字符串中大写字母字符，小写字母字符，数字字符出现的次数(不考虑其他字符)
    // 思路：
    //键盘录入一个字符串，用 Scanner 实现
    //要统计三种类型的字符个数，需定义三个统计变量，初始值都为0
    //遍历字符串，得到每一个字符
    //判断该字符属于哪种类型，然后对应类型的统计变量+1
    //假如ch是一个字符，我要判断它属于大写字母，小写字母，还是数字，直接判断该字符是否在对应的范围即可
    //大写字母：ch>='A' && ch<='Z'
    //小写字母： ch>='a' && ch<='z'
    //数字： ch>='0' && ch<='9'
    //输出三种类型的字符个数
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入一串字符：");
        String str = sc.next();
        int big = 0;
        int small = 0;
        int num = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
                small++;
            } else if (str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') {
                big++;
            } else if (str.charAt(i) >= '0' && str.charAt(i) <= '9') {
                num++;
            }
        }
        System.out.println("小写字母字符有 " + small + " 个");
        System.out.println("大写字母字符有 " + big + " 个");
        System.out.println("数字字符有 " + num + " 个");
    }
}

```

## 4、手机号屏蔽

```java
import java.util.Scanner;

public class Test04 {
    // 手机号屏蔽
    // 需求：以字符串的形式从键盘接受一个手机号，将中间四位号码屏蔽
    //最终效果为：156****1234
    // 思路：
    //键盘录入一个字符串，用 Scanner 实现
    //截取字符串前三位
    //String substring(int beginIndex, int endIndex) ：从beginIndex索引位置开始截取，截取到endIndex索引位置，得到新字符串并返回（包含头，不包含尾）
    //String s1 = telString.substring(0,3);
    //截取字符串后四位
    //String substring(int beginIndex) : 从传入的索引位置处，向后截取，一直截取到末尾，得到新的字符串并返回
    //String s2 = telString.substring(7);
    //将截取后的两个字符串，中间加上****进行拼接，输出结果
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入一个手机号码：");
        String tel = sc.next();
        System.out.println(tel.substring(0, 3) + "****"+ tel.substring(7));
    }
}

```

## 5、敏感词替换

```java
import java.util.Scanner;

public class Test05 {
    // 敏感词替换
    // 需求：键盘录入一个 字符串，如果字符串中包含（TMD），则使用***替换
    // 思路：
    //键盘录入一个字符串，用 Scanner 实现
    //替换敏感词
    //String replace(CharSequence target, CharSequence replacement)
    //将当前字符串中的target (被替换的旧值 )内容，使用replacement (替换的新值) 进行替换
    //返回新的字符串
    //输出结果
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入一串字符：");
        String str = sc.next();
        System.out.println(str.replace("TMD","***"));
    }
}

```

## 6、切割字符串

```java
import java.util.Arrays;
import java.util.Scanner;

public class Test06 {
    // 切割字符串
    // 需求：以字符串的形式从键盘录入学生信息，例如：“张三 , 23”从该字符串中切割出有效数据
    //封装为Student学生对象
    // 思路：
    //编写Student类，用于封装数据
    //键盘录入一个字符串，用 Scanner 实现
    //根据逗号切割字符串，得到（张三）（23）
    //String[] split(String regex) ：根据传入的字符串作为规则进行切割，将切割后的内容存入字符串数组中，并将字符串数组返回
    //从得到的字符串数组中取出元素内容，通过Student类的有参构造方法封装为对象
    //调用对象getXxx方法，取出数据并打印。
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入学生信息(中间用，隔开):");
        String str = sc.next();
        System.out.println(Arrays.toString(str.split(",")));
    }
}

```

