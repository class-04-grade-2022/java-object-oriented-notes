# 1、笔记

```java
public class Test02 {
    public static void main(String[] args) {
        // 调用无参数无返回值的方法
        printHello();
        // 调用有参数无返回值的方法
        getSum(1,2);
        
        // 无返回值都用void
        
        // 调用无参数有返回值的方法（调用返回值的第一种方式）
        int count = getCount();
        System.out.println(count);
        // 调用有参数有返回值的方法（调用返回值的第二种方式）
        System.out.println(getDiv(5,2));
    }
    // 无参数无返回值
    public static void printHello(){ // 无参数无返回值
        System.out.println("Hello");
    }
    // 有参数无返回值
    public static void getSum(int a,int b){ // 有参数无返回值
        System.out.println(a + b);
    }
    // 无参数有返回值
    public static int getCount(){
        return 10;
    }
    // 有参数有返回值
    public static double getDiv(double a,double b){ 
        return a/b;
    }
}
```

基本类型在方法之间传输的是所赋值的值，而不是变量，被调用的方法中对值进行的操作不影响调用它的方法中的值

```java
public class Test02 {
    public static void main(String[] args) {
        int i = 10;
        System.out.println(i);// 10
        num(i);
        System.out.println(i);// 10
    }
    public static void num(int a){
        System.out.println(a);// 10
        a = 20;
        System.out.println(a);// 20
    }
}
```

引用类型（例如数组）在方法之间传输的是所储存的地址，被调用的方法中对其地址所对应的值进行操作时，调用它的方法中的所对应的值也会跟着改变

```java
public class Test02 {
    public static void main(String[] args) {
        int[] arr = {1,2,3};
        System.out.println("main方法中 " + Arrays.toString(arr));// [1,2,3]
        num(arr);
        System.out.println("main方法中 " + Arrays.toString(arr));// [10,2,3]
    }
    public static void num(int[] a){
        System.out.println("num方法中 " + Arrays.toString(a));// [1,2,3]
        a[0] = 10;
        System.out.println("num方法中 " + Arrays.toString(a));// [10,2,3]
    }
}
```



# 2、作业

1,定义一个方法，接收一个数组，返回该数组的最大值和最小值；在主方法中打印最大，最小值

```java
import java.util.Arrays;

public class Test02 {
    public static void main(String[] args) {
        int[] arr = {1,2,234,343,11,34,12};
        System.out.println(Arrays.toString(arr));
        int max = arrMax(arr);
        System.out.println("数组中的最大值是" + max);

        System.out.println("数组中的最小值是" + arrMin(arr));
    }
    public static int arrMax(int[] arr){
        int max = arr[0];
        for (int i : arr) {
            if (i > max) {
                max = i;
            }
        }
        return max;
    }
    public static int arrMin(int[] arr){
        int min = arr[0];
        for (int i : arr) {
            if (i < min) {
                min = i;
            }
        }
        return min;
    }
}
```

2,定义一个方法，接收长和宽，返回一个矩	形的面积，在主方法中，打印这个面积

```java
public class Test02 {
    public static void main(String[] args) {
        int l = 2;
        int w = 3;
        int area = area(l,w);
        System.out.println(area);
    }
    public static int area(int l,int w){
        return l * w;
    }
}
```

3，利用方法重载，定义多个同名方法，通过接收一个不同类型的参数，方法体里用扫描器接收一个对应类型的值，最后将该输入的值返回，在主方法中，输出该值。

```java
import java.util.Scanner;

public class Test03 {
	public static void main(String[] args) {
//		3，利用方法重载，定义多个同名方法，通过接收一个不同类型的参数，方法体
//		里用扫描器接收一个对应类型的值，最后将该输入的值返回，在主方法中，输出该值。
		Scanner sc = new Scanner(System.in);
		System.out.print("请输入一个字符:");
		char str = sc.next().charAt(0);
		if (str >= '0' && str <= '9') {
			System.out.println(name(str));
		} else if ((str >= 'A' && str <= 'Z') || (str >= 'a' && str <= 'z')) {
			System.out.println(name(str));
		} else {
			System.out.println(name());
		}
	}
	public static int name() {
		int i = 0;
		return i;
	}
	public static int name(int a) {
		return a;
	}
	public static String name(String a) {
		return a;
	}
}
```

