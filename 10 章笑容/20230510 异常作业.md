键盘接受两个数，判断这两个数是否异常，如无异常，两数相加，有异常，输出提示并重新输入

```java
import java.util.Scanner;

public class Num {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            try {
                System.out.println("请输入一个数：");
                int num1 = sc.nextInt();
                System.out.println("请再输入一个数：");
                int num2 = sc.nextInt();
                System.out.println("两数之和为：" + num1 + num2);
                break;
            } catch (Exception e) {
                System.out.println("输入数据类型不符，请重新输入！");
                sc.next();
            }
        }
    }
}

```

