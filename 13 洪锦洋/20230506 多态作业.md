~~~java
## 多态练习

### 1、图形

（1）父类Graphic图形

- public double area()方法：返回0.0

- public double perimeter()方法：返回0.0

- public String getInfo()方法，返回图形面积和图形周长

- ```java
  //父类 Graphic 图形
  //- public double area()方法：返回0.0
  //- public double perimeter()方法：返回0.0
  //- public String getInfo()方法，返回图形面积和图形周长
  
  public class Graphic {
      public double area(){
          return 0.0;
      }
  
      public double perimeter(){
          return 0.0;
      }
  
      public String getInfo(){
          return "面积为="+area()+""+""+"周长为"+perimeter();
      }
  }
  ```

  

（2）子类Circle圆继承Graphic图形

- 包含属性：radius，属性私有化

- 包含get/set方法

- 重写area()求面积方法

- 重写perimeter()求周长方法

- 重写getInfo()方法，返回圆的半径，面积和周长

- ```java
  //子类Circle圆继承Graphic图形
  //- 包含属性：radius，属性私有化
  //- 包含get/set方法
  //- 重写area()求面积方法
  //- 重写perimeter()求周长方法
  //- 重写getInfo()方法，返回圆的半径，面积和周长
  
  public class Circle extends Graphic{
  
      private double radius;
  
      double pai=3.14;
  
      public double getRadius() {
          return radius;
      }
  
      public void setRadius(double radius) {
          this.radius = radius;
      }
  
      @Override
      public double area() {
          return pai*radius*radius;
      }
  
      @Override
      public double perimeter() {
          return 2*pai*radius;
      }
  
      @Override
      public String getInfo() {
          return "面积为="+area()+""+""+"周长为"+perimeter();
      }
  }
  
  ```

  

（3）子类矩形Rectangle继承Graphic图形

- 包含属性：length、width，属性私有化
- 包含get/set方法
- 重写area()求面积方法
- 重写perimeter()求周长方法
- 重写getInfo()方法，返回长和宽，面积、周长信息
- ```java
  //（3）子类矩形Rectangle继承Graphic图形
  //- 包含属性：length、width，属性私有化
  //- 包含get/set方法
  //- 重写area()求面积方法
  //- 重写perimeter()求周长方法
  //- 重写getInfo()方法，返回长和宽，面积、周长信息
  public class Rectangle extends Graphic{
      private int length;
      private int width;
  
      public int getLength() {
          return length;
      }
  
      public void setLength(int length) {
          this.length = length;
      }
  
      public int getWidth() {
          return width;
      }
  
      public void setWidth(int width) {
          this.width = width;
      }
  
      @Override
      public double area() {
          return length*width;
      }
  
      @Override
      public double perimeter() {
          return (length+width)*2;
      }
  
      @Override
      public String getInfo() {
          return "长为："+length+""+"宽"+width+super.getInfo();
      }
  }
  
  ```

  

（4）在测试类中，新建一个比较图形面积的方法，再建一个比较图形周长的方法，main方法中创建多个圆和矩形对象,再调用方法比较他们的周长或面积。

```java
public class Main {
    //（4）在测试类中，新建一个比较图形面积的方法，再建一个比较图形周长的方法，main方法中创建多个圆和矩形对象,再调用方法比较他们的周长或面积。
    public static void main(String[] args) {
        Circle c = new Circle();
        c.setRadius(3);
        System.out.println(c.area());
        System.out.println(c.perimeter());
        System.out.println("===================================");
        Rectangle r = new Rectangle();
        r.setLength(2);
        r.setWidth(4);
        System.out.println(r.area());
        System.out.println(r.perimeter());
        System.out.println("===================================");

        area(c,r);
        perimeter(c,r);
    }

    static void area(Graphic a, Graphic b){
        if (a.area()>b.area()){
            System.out.println("该 圆形的周长 比 该 矩形的周长 大 " +a.area());
        }
        else if (a.area()>b.area()){
            System.out.println("该 圆形的周长 比 该 矩形的周长 小 " +b.area());
        } else if (a.area()>b.area()) {
            System.out.println("该 圆形的周长 和 该 矩形的周长 一样 " +a.area());
        }
    }


    static void perimeter(Graphic a, Graphic b){
        if (a.perimeter()>b.perimeter()){
            System.out.println("该 圆形的面积 比 该 矩形的面积 大 " +a.perimeter());
        }
        else if (a.perimeter()<b.perimeter()){
            System.out.println("该 圆形的面积 比 该 矩形的面积 小 " +b.perimeter());
        } else if (a.perimeter()==b.perimeter()) {
            System.out.println("该 圆形的面积 和 该 矩形的面积 一样 " +a.perimeter());
        }
    }
}

```

~~~

