```java
public class Employee {


    private int id;
   private String name;
    private int age;
    private double salary;

 public Employee(int id,String name,int age,double salary){
     this.id =id;
     this.name = name;
     this.age = age;
     this.salary = salary;
 }

    public Employee() {

    }

    public void setName(String name){
     this.name = name;
 }
 public void setId(int id){
     this.id = id;

 }
 public void setAge(int age){
     this.age = age;
 }
 public void setSalary(double salary){
     this.salary = salary;
 }
 public String toString(){
     System.out.println("我是"+name+"学号为"+id+"年龄"+age+"工资为"+salary);

     return null;
 }
}



```

```java
import java.util.Set;

public class testEmployee {
    public static void main(String[] args) {
        Employee em = new Employee(1,"张三",23,10000.01);
        System.out.println("em = " + em);
        Employee ee = new Employee();
        ee.setAge(22);
        ee.setId(2);
        ee.setName("李四");
        ee.setSalary(10000.00);
        System.out.println("ee = " + ee);




    }
}


```

# 笔记

```
Java中面向对象的三大主要特征：
1.封装
2.继承
3.多态
封装的好处：
1、提高了代码的安全性
2、提高了代码的复用性

成员变量：类中方法外的变量
局部变量：方法中的变量

private
是一个权限修饰符
可以修饰成员（成员变量和成员方法）
被private修饰的成员只能在本类中才能访问

JavaBean(标准)
成员变量 
使用private修饰
构造方法 
提供一个无参构造方法
提供一个带多个参数的构造方法
成员方法 
提供每一个成员变量对应的setXxx()/getXxx()
提供一个显示对象信息的show()

创建对象并为其成员变量赋值的两种方式
无参构造方法创建对象后使用setXxx()赋值
使用带参构造方法直接创建带有属性值的对象




this
可以调用本类的成员(变量, 方法)解决局部变量和成员变量的重名问题

```

