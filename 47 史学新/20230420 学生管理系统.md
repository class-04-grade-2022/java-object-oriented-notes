```java
public class Student {

        private String id;
        private String name;
        private int age;
        private String sex;
        private String address;

        public Student(String id, String name, int age, String sex, String address) {
            this.id = id;
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.address = address;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Student() {
        }

    }
```



```java
import java.util.ArrayList;
import java.util.Scanner;

public class Student1 {
        public static void main(String[] args) {
            ArrayList<Student> stu = new ArrayList<>();
            Scanner sc = new Scanner(System.in);
            out:
            while (true) {
                // 在字符串\n 表示回车换行  \t 制表符，表示一个空格 \ 是转义符
                System.out.println("==============欢迎来到学生管理系统==================\n" +
                        "\t1 添加学生\n" +
                        "\t2 删除学生\n" +
                        "\t3 修改学生\n" +
                        "\t4 查看所有学生\n" +
                        "\t5 退出系统\n" +
                        "\t请选择相应操作：");
                String choice = sc.next();
                switch (choice) {
                    case "1" :
                        addStudent(stu, sc); //添加学生信息
                        break;
                    case "2" :
                        deleteStudent(stu, sc); //删除学生信息
                        break;
                    case "3" :
                        updateStudent(stu, sc); //修改学生信息
                        break;
                    case "4" :
                        showStudents(stu); //查看学生信息
                        break;
                    case "5" :
                        System.out.println("退出成功！");
                        break out;

                    //退出系统
                    default : System.out.println("无该选项,请重新输入！");
                }
            }
        }
        public static boolean decide(ArrayList<Student> stu,String id){
            boolean f = false;
            for (int i = 0; i < stu.size(); i++) {
                Student s = stu.get(i);
                if (s.getId().equals(id)){
                    f = true;
                    break;
                }
            } return f;
        }
        public static void addStudent(ArrayList<Student> stu,Scanner sc) {
            String id;
            while (true) {
                System.out.println("请输入学号：");
                id = sc.next();
                boolean f = decide(stu, id);
                if (f) {
                    System.out.println("该学号已存在，请重新输入！");
                } else {
                    break;
                }
            }
            System.out.println("请输入姓名：");
            String name = sc.next();
            System.out.println("请输入年龄：");
            int age = sc.nextInt();
            System.out.println("请输入性别：");
            String sex = sc.next();
            System.out.println("请输入地址：");
            String address = sc.next();
            Student s = new Student(id, name, age, sex, address);
            stu.add(s);
            System.out.println("学号：" + id + " 姓名：" + name + " 年龄：" + age + " 性别：" + sex + " 住址：" + address);
            System.out.println("添加成功！");
        }
        public static void deleteStudent(ArrayList<Student> stu,Scanner sc){
            System.out.println("请输入需要删除的学生的学号：");
            String id = sc.next();
            int index = -1;
            for (int i = 0; i < stu.size(); i++) {
                Student s = stu.get(i);
                if (s.getId().equals(id)){
                    index = i;
                    break;
                }
            }
            if (index==-1){
                System.out.println("需要删除的学生学号不存在，请重新输入!");
            }else{
                stu.remove(index);
                System.out.println("删除成功！");
            }
        }


        public static void updateStudent(ArrayList<Student> stu,Scanner sc){
            String id;
            while (true) {
                System.out.println("请输入需要修改的学生的学号：");
                id = sc.next();
                boolean f = decide(stu, id);
                if (f) {
                    break;
                } else {
                    System.out.println("未查询到该学号，请重新输入！");
                    return;
                }
            }
            System.out.println("请输入姓名：");
            String name = sc.next();
            System.out.println("请输入年龄：");
            int age = sc.nextInt();
            System.out.println("请输入性别：");
            String sex = sc.next();
            System.out.println("请输入地址：");
            String address = sc.next();
            Student s = new Student(id,name,age,sex,address);
            for (int i = 0; i < stu.size(); i++) {
                Student s1 = stu.get(i);
                if (s1.getId().equals(id)){
                    stu.set(i,s);
                    break;
                }
            }
            System.out.println("学号："+id+"姓名：" + name + "年龄：" +age+ "性别："+sex+"住址："+address);
            System.out.println("修改成功！");
        }

        public static void showStudents(ArrayList<Student> stu){
            for (int i = 0; i < stu.size(); i++) {
                Student s = stu.get(i);
                System.out.println("学号："+s.getId()+" 姓名："+s.getName() +" 性别："+s.getSex()+" 年龄："+s.getSex()+" 地址："+s.getAddress());
            }
        }
}
```

