# 作业

```java
public class sdf {
    public static void main(String[] args) {
        //# 巩固题
//
//        ## 1、判断5的倍数
//
//        从键盘输入一个整数，判断它是否是5的倍数
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入一个数");
        int a =scan.nextInt();
        if (a%5==0){
            System.out.println(a+"是5的倍数");
        }else{
            System.out.println(a+"不是5的倍数");
        }
//
//        ## 2、判断字符类型
//
//        从键盘输入一个字符，判断它是字母还是数字，还是其他字符
        
//
//        ## 3、计算折扣后金额
//
//        从键盘输入订单总价格totalPrice（总价格必须>=0），根据优惠政策计算打折后的总价格。
//
//        编写步骤：
//
//        1. 判断当`totalPrice >=500` ,discount赋值为0.8
//        2. 判断当`totalPrice >=400` 且`<500`时,discount赋值为0.85
//        3. 判断当`totalPrice >=300` 且`<400`时,discount赋值为0.9
//        4. 判断当`totalPrice >=200` 且`<300`时,discount赋值为0.95
//        5. 判断当`totalPrice >=0` 且`<200`时,不打折，即discount赋值为1
//        6. 判断当`totalPrice<0`时，显示输入有误
//        7. 输出结果
         Scanner scan = new Scanner(System.in);
        System.out.println("请输入一个数");
        double a=scan.nextInt();
        double discount=0;
        if (a>=500){
             discount=0.8;
             a=a*discount;
            System.out.println("打折后的价格为"+a);
        }else if(a>=400){
            discount=0.85;
            a=a*discount;
            System.out.println("打折后的价格为"+a);
        }else if (a>=300){
            discount=0.9;
            a=a*discount;
            System.out.println("打折后的价格为"+a);
        }else if (a>=200){
            discount=0.95;
            a=a*discount;
            System.out.println("打折后的价格为"+a);
        }else if (a>=0){
            discount=1;
            a=a*discount;
            System.out.println("打折后的价格为"+a);
        }else if (a<0){
            System.out.println("输入有误");
        }
//
//        ## 4、输出月份对应的英语单词
//
//        从键盘输入月份值，输出对应的英语单词
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入一个数");
        int a=scan.nextInt();
        switch (a){
            case 1:
                System.out.println("January");
                break;
            case 2:
                System.out.println("February");
                break;
            case 3:
                System.out.println("March");
                break;
            case 4:
                System.out.println("April");
                break;
            case 5:
                System.out.println("May");
                break;
            case 6:
                System.out.println("June");
                break;
            case 7:
                System.out.println("July");
                break;
            case 8:
                System.out.println("August");
                break;
            case 9:
                System.out.println("September");
                break;
            case 10:
                System.out.println("October");
                break;
            case 11:
                System.out.println("November");
                break;
            case 12:
                System.out.println("December");
                break;
            default:
                System.out.println("输入错误");




        }

//
//        ## 5、计算今天是星期几
//
//        定义变量week赋值为上一年12月31日的星期值（可以通过查询日历获取），定义变量year、month、day，分别赋值今天日期年、月、日值。计算今天是星期几
                Calendar d = Calendar.getInstance();
        int week = d.get(Calendar.DAY_OF_WEEK) - 1;
        switch (week) {
            case 1:
                System.out.println("星期一");
                break;
            case 2:
                System.out.println("星期二");
                break;
            case 3:
                System.out.println("星期三");
                break;
            case 4:
                System.out.println("星期四");
                break;
            case 5:
                System.out.println("星期五");
                break;
            case 6:
                System.out.println("星期六");
                break;
            case 7:
                System.out.println("星期七");
                break;
            default:
                System.out.println("你TM会不会");


        }
//
//        # 拔高题
//
//        ## 1、判断年、月、日是否合法
//
//        从键盘输入年、月、日，要求年份必须是正整数，月份范围是[1,12]，日期也必须在本月总天数范围内，如果输入正确，输出“年-月-日”结果，否则提示输入错误。
//
//        ## 2、判断打鱼还是晒网
//
//        从键盘输入年、月、日，假设从这一年的1月1日开始执行三天打鱼两天晒网，那么你输入的这一天是在打鱼还是晒网。
//
//        ## 3、判断星座
//
//        声明变量month和day，用来存储出生的月份和日期，判断属于什么星座，各个星座的日期范围如下：
//
//        ![1558000604568](03_Java流程控制语句结构作业图片/1558000604568.png)
//
//        # 简答题
//
//        1、switch是否能作用在byte上，是否能作用在long上，是否能作用在String上？
//

    }
}

```

