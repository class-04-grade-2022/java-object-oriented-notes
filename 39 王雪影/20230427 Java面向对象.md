```java
public abstract class Employee {
    private String name;
    private String workId;
    private String dept;

    public Employee() {
    }

    public Employee(String name, String workId, String dept) {
        this.name = name;
        this.workId = workId;
        this.dept = dept;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }
    public abstract void showMsg();

}
class Manager extends Employee{
     String Clerk;

    public String getClerk() {
        return Clerk;
    }

    public void setClerk(String clerk) {
        Clerk = clerk;
    }

    public Manager() {
    }

    public Manager(String Clerk,String name,String workId,String dept) {
        super(name,workId,dept);
        this.Clerk=Clerk;
    }

    @Override
    public void showMsg() {
        System.out.printf("经理:工号为 "+getWorkId()+",名字为 "+getName()+",部门为 "+getDept());
    }

}
class Clerk extends Employee{
    String Manager;

    public Clerk() {
    }

    public Clerk(String Manager,String name,String workId,String dept) {
        super(name,workId,dept);
        this.Manager=Manager;
    }

    @Override
    public void showMsg() {
        System.out.printf("职员:工号为 "+getWorkId()+",名字为 "+getName()+",部门为 "+getDept());
    }



    class test{
}

    public static void main(String[] args) {
        new Manager("张小强","M001","销售部","李小亮").showMsg();
        new Clerk("李小亮","C001","销售部","张小强").showMsg();

    }
}


```

