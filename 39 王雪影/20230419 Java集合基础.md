```java
public class one {
    public static void main(String[] args) {
        StringBuilder a = new StringBuilder("wang");
        StringBuilder b = new StringBuilder("xue");
        StringBuilder c = new StringBuilder("ying");
        a.append(b);
        a.append(c);
        System.out.println(a);
    }
}

```

```java
import java.util.Scanner;

public class one2 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入一串字符串");
        String a =sc.next();
        StringBuilder sb = new StringBuilder(a);
        sb.reverse();
        if (sb.toString().equals(a)){
            System.out.println("a="+a+"是对称的");
        }else {
            System.out.println("a="+a+"不是对称的");
        }
    }
}

```

```java
import java.util.ArrayList;

public class two {
    public static void main(String[] args) {
        ArrayList<String>a=new ArrayList<>();
        a.add("1");
        a.add("2");
        a.add("3");
        a.add("4");
        a.add("5");
        for (int i = 0; i < a.size(); i++) {
            String b = a.get(i);
            System.out.println(b);
        }
    }
}

```

```java
import java.util.ArrayList;

public class Student {
    public static void main(String[] args) {
        ArrayList<String> a = new ArrayList<>();
        a.add("学生一号");
        a.add("学生二号");
        a.add("学生三号");
        System.out.println(a);
        for (int i = 0; i < a.size(); i++) {
            String b =a.get(i);
            System.out.println(b);
        }
    }
}

```

```java
public class five {
    public static void main(String[] args) {
        ArrayList<String> a=new ArrayList<>();
        a.add("test");
        a.add("张三");
        a.add("李四");
        a.add("test");
        a.add("test");
        for (int i = 0; i <a.size() ; i++) {
            String s= a.get(i);
            if (s.equals("test")){
                a.remove(i);
                i--;
            }

        }
        System.out.println(a);
    }
}
```

