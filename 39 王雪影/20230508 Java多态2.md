```java
import java.util.Scanner;

public class Employee {
    private String name;

    public Employee() {
    }

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public double earning(){
        return 0.0;
    }
    public String getInfo(){
        return null;
    }
}
class MyDate{
    private int year;
    private int month;
    private int day;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public MyDate() {
    }

    @Override
    public String toString() {
        return "MyDate{" +
                "year=" + year +
                ", month=" + month +
                ", day=" + day +
                '}';
    }

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }
    public String getInfo(){
        return getMonth()+"年"+getMonth()+"月"+getDay()+"日";
    }

}
class SalaryEmployee extends Employee{
    private double salary;
    private MyDate birthday;


    public SalaryEmployee() {
    }

    public SalaryEmployee(String name, double salary, MyDate birthday) {
        super(name);
        this.salary = salary;
        this.birthday = birthday;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public MyDate getBirthday() {
        return birthday;
    }

    public void setBirthday(MyDate birthday) {
        this.birthday = birthday;
    }


    @Override
    public double earning() {
        return salary;
    }

    @Override
    public String getInfo() {
        return getName()+"的生日是 "+getBirthday().getInfo()+" 实发工资是 "+earning();
    }
}
class HourEmployee extends Employee{
    double workhour;
    double workmoney;

    public HourEmployee() {
    }

    public HourEmployee(String name, double workhour, double workmoney) {
        super(name);
        this.workhour = workhour;
        this.workmoney = workmoney;
    }

    @Override
    public double earning() {
        return workhour*workmoney;
    }

    @Override
    public String getInfo() {
        return getName()+"的实发工资是 "+earning()+" 时薪是 "+workmoney+" 工作小时数是 "+workhour;
    }

}
class Manager extends SalaryEmployee{
    private double bonusratio;

    public Manager() {
        super();
    }

    public Manager(String name, double salary, MyDate birthday, double bonusratio) {
        super(name, salary, birthday);
        this.bonusratio = bonusratio;
    }


    public double getBonusratio() {
        return bonusratio;
    }

    public void setBonusratio(double bonusratio) {
        this.bonusratio = bonusratio;
    }

    @Override
    public double earning() {
        return getSalary()*(1+bonusratio);
    }

    @Override
    public String getInfo() {
        return getName()+"的实发工资是 "+earning()+" 生日是 "+getBirthday().getInfo()+" 奖金比例是 "+bonusratio;
    }
}
class text{
    public static void main(String[] args) {
        Employee[] qq=new Employee[10];
        MyDate my=new MyDate(2023,13,32);
        MyDate mm=new MyDate(2023,14,33);
        qq [0]=new SalaryEmployee("田雪琼",10000.00,my);
        qq [1]=new HourEmployee("闫雪莲",20,15);
        qq [2]=new Manager("陈灵钰",2000.00,mm,0.8);
        System.out.println(qq[0].getInfo());
        System.out.println(qq[1].getInfo());
        System.out.println(qq[2].getInfo());
        double sumsalary=0;
        for (int i = 0; i < qq.length; i++) {
            System.out.println(qq[i].getInfo());
            sumsalary+=qq[i].earning();
        }
        System.out.println("实发工资总额为："+sumsalary);
        Scanner sc = new Scanner(System.in);
        
    }
}


```

