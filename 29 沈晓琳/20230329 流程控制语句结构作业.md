## 笔记

```java
运算符
    1.自增自减运算符
++	自增	变量的值加1   
–—	自减	变量的值减1   
参与操作的时候，如果放在变量的后边，先拿变量参与操作，后拿变量做++或者--。
参与操作的时候，如果放在变量的前边，先拿变量做++或者--，后拿变量参与操作。
    2.赋值运算符(优先级最小，（）最大)
    3.关系运算符
 &：过程是前一个条件false，后一个条件仍执行
 &&：前一个条件false，后一个条件不执行
    4.三元运算符
```



## 作业

```java
package z;

import java.util.Scanner;

public class z2 {
    public static void main(String[] args) {
//        # 巩固题
//
//## 1、判断5的倍数
//
//        从键盘输入一个整数，判断它是否是5的倍数
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入一个整数");
        int a=sc.nextInt();
        if(a%5==0){
            System.out.println(a+"是5的倍数");
        }else{
            System.out.println(a+"不是5的倍数");
        }
//
//## 2、判断字符类型
//
//        从键盘输入一个字符，判断它是字母还是数字，还是其他字符
        System.out.println("请输入一个字符");
        char b=sc.next().charAt(0);
        if(b>='0' && b<='9'){
            System.out.println(b+"是数字");
        }else if(b>='a' && b<='z'){
            System.out.println(b+"是字母");
        }else{
            System.out.println(b+"是其他字符");
        }
//## 3、计算折扣后金额
//        从键盘输入订单总价格totalPrice（总价格必须>=0），根据优惠政策计算打折后的总价格。
//        编写步骤：
//        1. 判断当`totalPrice >=500` ,discount赋值为0.8
//        2. 判断当`totalPrice >=400` 且`<500`时,discount赋值为0.85
//        3. 判断当`totalPrice >=300` 且`<400`时,discount赋值为0.9
//        4. 判断当`totalPrice >=200` 且`<300`时,discount赋值为0.95
//        5. 判断当`totalPrice >=0` 且`<200`时,不打折，即discount赋值为1
//        6. 判断当`totalPrice<0`时，显示输入有误
//        7. 输出结果
        System.out.println("请输入订单总价格");
        int totalPrice=sc.nextInt();
        if(totalPrice >=500){
            System.out.println("折扣后总价为："+totalPrice*0.8);
        } else if (totalPrice>=400 && totalPrice<500) {
            System.out.println("折扣后总价为："+totalPrice*0.85);
        } else if (totalPrice>=300 && totalPrice<400) {
            System.out.println("折扣后总价为："+totalPrice*0.9);
        } else if (totalPrice >=200 && totalPrice<300) {
            System.out.println("折扣后总价为："+totalPrice*0.95);
        } else if (totalPrice >=0 && totalPrice<200) {
            System.out.println("折扣后总价为："+totalPrice*1);
        }else {
            System.out.println("输入有误");
        }

//## 4、输出月份对应的英语单词
//
//        从键盘输入月份值，输出对应的英语单词
//
        System.out.println("输入月份");
        int month=sc.nextInt();
        if (month==1){
            System.out.println("January");
        } else if (month==2) {
            System.out.println("February");
        } else if (month==3) {
            System.out.println("March");
        } else if (month==4) {
            System.out.println("April");
        } else if (month==5) {
            System.out.println("May");
        } else if (month==6) {
            System.out.println("June");
        } else if (month==7) {
            System.out.println("July");
        } else if (month==8) {
            System.out.println("August");
        } else if (month==9) {
            System.out.println("September");
        } else if (month==10) {
            System.out.println("October");
        } else if (month==11) {
            System.out.println("November");
        } else if (month==12) {
            System.out.println("December");
        }else{
            System.out.println("输入有误");
        }


//## 5、计算今天是星期几
//
//        定义变量week赋值为上一年12月31日的星期值（可以通过查询日历获取），定义变量year、month、day，分别赋值今天日期年、月、日值。计算今天是星期几。
//
//        System.out.println("年：");
//        int year = sc.nextInt();
//        System.out.println("月：");
//        int monthh = sc.nextInt();
        //不会
//# 拔高题
//
//## 1、判断年、月、日是否合法
//
//        从键盘输入年、月、日，要求年份必须是正整数，月份范围是[1,12]，日期也必须在本月总天数范围内，如果输入正确，输出“年-月-日”结果，否则提示输入错误。
        System.out.println("请输入年：");
        int yearr=sc.nextInt();
        System.out.println("请输入月：");
        int month2=sc.nextInt();
        System.out.println("请输入日：");
        int day2=sc.nextInt();
        if (yearr>0 && (month2>=1 && month2<=12) && (day2>=1 && day2<=31)){
            System.out.println(yearr+"年"+month2+"月"+day2+"日");
        }else {
            System.out.println("输入错误");
        }
// 应该是写错了
//
//## 2、判断打鱼还是晒网
//
//        从键盘输入年、月、日，假设从这一年的1月1日开始执行三天打鱼两天晒网，那么你输入的这一天是在打鱼还是晒网。
//        System.out.println("年：");
//        int year2=sc.nextInt();
//        System.out.println("月：");
//        int month3=sc.nextInt();
//        System.out.println("日：");
//        int day3=sc.nextInt();
//        不会
//
//## 3、判断星座
//
//        声明变量month和day，用来存储出生的月份和日期，判断属于什么星座，各个星座的日期范围如下：
//
//        ![1558000604568](03_Java流程控制语句结构作业图片/1558000604568.png)
        System.out.println("请输入出生的月份");
        int month4=sc.nextInt();
        System.out.println("请输入出生的日期");
        int day4=sc.nextInt();
        if ((month4==3 && day4>=21) || (month4==4 && day4<=19)){
            System.out.println("生日"+month4+"月"+day4+"日是白羊座");
        } else if ((month4==4 && day4>=20) || (month4==5 && day4<=20)) {
            System.out.println("生日"+month4+"月"+day4+"日是金牛座");
        } else if ((month4==5 && day4>=21) || (month4==6 && day4<=21)) {
            System.out.println("生日"+month4+"月"+day4+"日是双子座");
        } else if ((month4==6 && day4>=22) || (month4==7 && day4<=22)) {
            System.out.println("生日"+month4+"月"+day4+"日是巨蟹座");
        } else if ((month4==7 && day4>=23) || (month4==8 && day4<=22)) {
            System.out.println("生日"+month4+"月"+day4+"日是狮子座");
        } else if ((month4==8 && day4>=23) || (month4==2 && day4<=22)) {
            System.out.println("生日"+month4+"月"+day4+"日是处女座");
        } else if ((month4==9 && day4>=23) || (month4==10 && day4<=23)) {
            System.out.println("生日"+month4+"月"+day4+"日是天秤座");
        } else if ((month4==10 && day4>=24) || (month4==11 && day4<=22)) {
            System.out.println("生日"+month4+"月"+day4+"日是天蝎座");
        } else if ((month4==11 && day4>=23) || (month4==12 && day4<=21)) {
            System.out.println("生日"+month4+"月"+day4+"日是射手座");
        } else if ((month4==12 && day4>=22) || (month4==1 && day4<=19)) {
            System.out.println("生日"+month4+"月"+day4+"日是摩羯座");
        } else if ((month4==1 && day4>=20) || (month4==2 && day4<=18)) {
            System.out.println("生日"+month4+"月"+day4+"日是水瓶座");
        } else if ((month4==2 && day4>=19) || (month4==3 && day4<=20)) {
            System.out.println("生日"+month4+"月"+day4+"日是双鱼座");
        }else {
            System.out.println("输入错误");
        }
//
//# 简答题
//
//        1、switch是否能作用在byte上，是否能作用在long上，是否能作用在String上？
// switch可以作用在byte上，不能作用在long上，JDK7开始支持作用在string上。

    }
}

```

