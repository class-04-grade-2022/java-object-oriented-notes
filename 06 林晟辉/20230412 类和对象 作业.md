# 1、笔记

1. ​	类

   1. 变量

      1. 成员变量(类中方法外的变量)

         1. this代表成员变量

         2. private是修饰符表示是私有的变量

            ```Java
            // 为私有变量赋值
            public setXxx(形参列表){
                
            }    
            // 获取私有变量的值
            public 返回值 getXxx(){
                
            }
            ```

            

      2. 局部变量(方法中的变量)

   2. 方法

      ```java
      // 转字符串
      public String toString(){
          
      }
      ```

      

# 2、作业

![image-20230407173437631](./类和对象 作业/image-20230407173437631.png)



题目：1，创建一个员工类（标准类），

```java
public class Employee {
    int number;
    int id;
    String name;
    int age;
    double salary;
    public Employee(int number,int id,String name,int age,double salary){
        this.number=number;
        this.id=id;
        this.name=name;
        this.age=age;
        this.salary=salary;
    }
    public Employee(){

    }
    public String toString() {
        String str="第"+number+"个员工的编号是："+id+"，姓名："+name+"，年龄"+age+"，薪资"+salary;
        return str;
    }
}
```

2.在测试类中用无参和有参两种构造器。分别创建一个对象。并打印对象

```java
public class testEmp {
    public static void main(String[] args) {
        Employee id1 = new Employee(1,1,"张三",23,10000.0);
        System.out.println(id1);
        Employee id2 = new Employee();
        id2.number=2;
        id2.id=2;
        id2.name="李四";
        id2.age=22;
        id2.salary=11000.0;
        System.out.println(id2);

    }
}
```

