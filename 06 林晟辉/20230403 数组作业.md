# 1、笔记

```Java
// 静态数组定义
	数据类型[] 变量名={元素};
// 动态数组定义
	数据类型[] 变量名=new 数据类型{索引数};
// 随机数定义
	Random 定义名=new Random();
	数据类型 变量名=定义名.nextint(X); // X表示获取0到X-1的随机数
```

# 1、作业

### **1.定义一个含有五个元素的数组,并为每个元素赋值,求数组中所有元素的最小值**

**操作步骤：**

​	1.定义5个元素数组

​	2.可以使用初始化数组的两种方式之一为数组元素赋值

​	3.遍历数组求数组中的最小值

```java
public static void main(String[] args) {
        int[] num = {4,2,6,3,1};
        int min=num[0];
        for (int i = 0; i < num.length; i++) {
            if(num[i]<min){
                min=num[i];
            }
        }
        System.out.println("数组num的最小值是："+min);
    }
```

### **2．需求：求出数组中索引与索引对应的元素都是奇数的元素**

**分析：**

​	1、遍历数组

​	2、判断索引是否是奇数（索引 % 2 != 0）

​	3、判断索引对应的元素是否是奇数(arr[索引] % 2 != 0)

​	4、满足条件输出结果

```java
public static void main(String[] args) {
        int[] num = {4,2,6,3,1};
        for (int i = 0; i < num.length; i++) {
            if (i %2!=0 && num[i]%2!=0){
                System.out.println("索引与索引对应的元素都是奇数的元素:"+num[i]);
            }
        }
    }
```



### **3.按要求在main方法中完成以下功能：**

​	a.   定义一个长度为5的int型数组arr，提示用户输入5个1-60之间的数字作为数组元素

​	b.  生成2-10（范围包含2和10）之间的随机数num

​	c.   遍历数组arr,筛选出数组中不是num倍数的元素并输出

​	**PS：输入的数组元素范围包括1和60，不需要代码判断**

```java
public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("输入5个1-60之间的数字");
        int[] arr = new int[5];
        for (int i = 0; i < 5; i++) {
            arr[i]= sc.nextInt();
        }
        Random ran = new Random();
        int num= ran.nextInt(9)+2;
        for (int i = 0; i < 5; i++) {
            if (arr[i]%num!=0){
                System.out.println("数组中不是"+num+"倍数的元素是："+arr[i]);
            }
        }
    }
```

### **4.有一个数组int[] arr = {9,1,3,4,54,56,23,22,20,43,45,78};,要求打印数组中能被6整除的元素。**

```java
public static void main(String[] args) {
        int[] arr = {9,1,3,4,54,56,23,22,20,43,45,78};
        System.out.print("数组中能被6整除的元素有：");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]%6==0){
                System.out.print(arr[i]+ " ");
            }
        }
    }
```

### **5.定义一个长度为20的数组，元素为20-40的随机数，要求判断指定元素在数组中出现的次**数，指定元素为键盘录入范围为20-40之间。

```java
public static void main(String[] args) {
        int[] arr = new int[20];
        int a=0;
        Random ran = new Random();
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < arr.length; i++) {
            arr[i]= ran.nextInt(21)+20;
        }
        System.out.print("输入一个20-40之间的数：");
        int num= sc.nextInt();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]==num){
                a++;
            }
        }
        System.out.println(num+"在数组中出现"+a+"次");
    }
```



