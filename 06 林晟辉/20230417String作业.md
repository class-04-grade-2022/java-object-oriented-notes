# 作业

1. ​	用户登入

   需求：已知用户名和密码，请用程序实现模拟用户登录。总共给三次机会，登录之后，给出相应的提示

   ```Java
   package t1;
   
   import java.util.Scanner;
   
   public class password {
       public static void main(String[] args) {
           String user="lin";
           String password="1008611";
           int a=1;
           Scanner sc = new Scanner(System.in);
           while (a<4){
               System.out.print("请输入用户名：");
               String users= sc.next();
               System.out.print("请输入密码：");
               String passwords=sc.next();
               if (user.equals(users) && password.equals(passwords)){
                   System.out.println("登入成功");
                   break;
               }else{
                   System.out.println("用户名或者密码"+a+++"次输入错误");
               }
           }
       }
   }
   ```

2. 遍历字符串（打印字符）

   需求：键盘录入一个字符串，使用程序实现在控制台遍历该字符串

   ```Java
   package t2;
   
   import java.util.Scanner;
   
   public class t2 {
       public static void main(String[] args) {
           Scanner sc = new Scanner(System.in);
           System.out.print("请输入字符串：");
           String test= sc.next();
           for (int i = 0; i < test.length(); i++) {
               System.out.println(test.charAt(i));
           }
       }
   }
   ```

3. 遍历数组（打印数组）

   需求：键盘录入一个字符串，使用程序实现在控制台遍历该字符串

   ```java
   package t3;
   
   import java.util.Arrays;
   import java.util.Scanner;
   
   public class t3 {
       public static void main(String[] args) {
           Scanner sc = new Scanner(System.in);
           System.out.print("请输入字符串：");
           String test= sc.next();
           char[] arr= new char[test.length()];
           for (int i = 0; i < test.length(); i++) {
               arr[i]=test.charAt(i);
           }
           System.out.println(Arrays.toString(arr));
       }
   }
   
   ```

4. 统记字符次数

   需求：键盘录入一个字符串，统计该字符串中大写字母字符，小写字母字符，数字字符出现的次数(不考虑其他字符)

   ```Java
   package t4;
   
   import java.util.Scanner;
   
   public class t4 {
       public static void main(String[] args) {
           Scanner sc = new Scanner(System.in);
           System.out.print("请输入一个字符串：");
           String a= sc.next();
           int big=0;
           int small=0;
           int num=0;
           for (int i = 0; i < a.length(); i++) {
               if(a.charAt(i)>='a' && a.charAt(i)<='z'){
                   small++;
               } else if (a.charAt(i)>='A' && a.charAt(i)<='Z') {
                   big++;
               } else if (a.charAt(i)>='0' && a.charAt(i)<='9') {
                   num++;
               }
           }
           System.out.println("大写字母有："+big+"个，小写字母有："+small+"个，数字有："+num+"个");
       }
   }
   ```

5. 手机号屏蔽

   需求：以字符串的形式从键盘接受一个手机号，将中间四位号码屏蔽

   最终效果为：156****1234

   ```Java
   package t5;
   
   import java.util.Scanner;
   
   public class t5 {
       public static void main(String[] args) {
           Scanner sc = new Scanner(System.in);
           System.out.print("请输入一个电话号码：");
           String tel=sc.next();
           String a=tel.substring(0,3);
           String b=tel.substring(7);
           System.out.println("tel="+a+"****"+b);
       }
   }
   
   ```

6. 敏感词替换

   需求：键盘录入一个 字符串，如果字符串中包含（TMD），则使用***替换

   ```Java
   package t6;
   
   import java.util.Scanner;
   
   public class t6 {
       public static void main(String[] args) {
           Scanner sc = new Scanner(System.in);
           System.out.print("请输入一个字符串：");
           String text1=sc.next();
           System.out.println(text1.replace("TMD","***"));
       }
   }
   ```

7. 切割字符串

   需求：以字符串的形式从键盘录入学生信息，例如：“张三 , 23”从该字符串中切割出有效数据
   封装为Student学生对象

   ```java
   package t7;
   
   import java.util.Scanner;
   
   public class t7 {
       public static void main(String[] args) {
   
           Scanner sc = new Scanner(System.in);
           System.out.print("请输入字符串：");
           Student stu= new Student();
           stu.setStu(sc.next());
           for (int i = 0; i < stu.getStu().length; i++) {
               System.out.println(stu.getStu());
           }
   
       }
   }
   
   ```

   ```java
   package t7;
   
   import java.util.Arrays;
   
   import static java.lang.String.*;
   
   public class Student {
       private String[] stu;
   
       public String[] getStu() {
           return stu;
       }
   
       public void setStu(String stu) {
           this.stu = stu.split(",");
       }
   
   }
   
   ```

   