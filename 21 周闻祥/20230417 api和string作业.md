# 用户登录

需求：已知用户名和密码，请用程序实现模拟用户登录。总共给三次机会，登录之后，给出相应的提示

```java
 public static void main(String[] args) {
        String password="123";
        int x=3;
        for (int i = 0; i <3 ; i++) {
            Scanner miao=new Scanner(System.in);
            System.out.println("请输入密码:");
            String a=miao.next();
            if(a.equals(password) && x>=0){
                System.out.println("输入正确");
                break;
            }else{
                x--;
                System.out.println("输入错误你还有"+x+"次机会");
            }
            if (x==0){
                System.out.println("已锁定");
            }
        }
    }
```



# 遍历字符串

```java
  public static void main(String[] args) {
        Scanner abc=new Scanner(System.in);
        System.out.println("请输入字符串");
        String arr=abc.next();
        char[] chars=arr.toCharArray();
        System.out.println(Arrays.toString(chars));
    }
```



# 统计字符串次数

```java
    public static void main(String[] args) {
        String str="ApplePlease520";
        int big=0;
        int small=0;
        int shuzi=0;

        for (int i = 0; i <str.length(); i++) {
            char x=str.charAt(i);
            if (x>='A' && x<='Z'){
                big++;
            }else if(x>='a' && x<='z'){
                small++;
            }else if(x>='0' && x<='9'){
                shuzi++;
            }
        }
        System.out.println(str);
        System.out.println("大写字母有:"+big);
        System.out.println("小写字母有:"+small);
        System.out.println("数字有:"+shuzi);
    }
```



# 手机号屏蔽

```java
  public static void main(String[] args) {
        String ipone = "13960621539";
        String one = ipone.substring(0, 3);
        String two = ipone.substring(7, 11);
        System.out.println(ipone);
        System.out.println(one+"****"+two);
  }
```



# 字符串替换文字

```java
public static void main(String[] args) {
    String str="你不爱我，我不爱你，蜜雪冰城甜蜜蜜";
    String av="";
        av = str.replace("不爱", "爱");
    System.out.println(av);
}
```



# 切割字符串

```java
   public static void main(String[] args) {
        String str="li.jun.yang";
        String[] arr=str.split("\\.");
        System.out.println(Arrays.toString(arr));
   }
```



