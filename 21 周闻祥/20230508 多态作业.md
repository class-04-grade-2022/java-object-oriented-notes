### 2、员工

（1）声明一个父类Employee员工类型，

- 有姓名属性，私有化，提供get/set方法
- public double earning()：代表实发工资，返回0.0
- public String getInfo()：显示姓名和实发工资

（2）声明MyDate类型

- 有int类型的年，月，日属性，私有化，提供get/set方法
- 提供public String getInfo()，返回“xxxx年xx月xx日”

（3）声明一个子类SalaryEmployee正式工，继承父类Employee

- 增加属性，double类型的薪资，MyDate类型的出生日期，私有化，提供get/set方法
- 重写方法，public double earning()返回实发工资， 实发工资 = 薪资
- 重写方法，public String getInfo()：显示姓名和实发工资、生日

（4）声明一个子类HourEmployee小时工，继承父类Employee

- 有属性，double类型的工作小时数和每小时多少钱
- 重写方法，public double earning()返回实发工资， 实发工资 = 每小时多少钱 * 小时数
- 重写方法，public String getInfo()：显示姓名和实发工资，时薪，工作小时数

（5）声明一个子类Manager经理，继承SalaryEmployee

- 增加属性：奖金比例，私有化，提供get/set方法
- 重写方法，public double earning()返回实发工资， 实发工资 = 薪资 *(1+奖金比例)
- 重写方法，public String getInfo()：显示姓名和实发工资，生日，奖金比例

（6）声明一个员工数组，存储各种员工，你现在是人事，遍历查看每个人的详细信息，并统计实发工资总额，通知财务准备资金。

（7）从键盘输入当期月份值，如果他是正式工（包括SalaryEmployee和Manager），并且是本月生日的，通知领取生日礼物。

```java
import java.util.ArrayList;
import java.util.Scanner;

public class Employee {
    private String name;

    public Employee(String name) {
        this.name = name;
    }

    public Employee() {
    }

    public double earning() {
        return 0.0;
    }

    public String getInfo() {
        return name + " 实发工资"+earning()+" ";
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class Mydate {
    private int year, month, day;

    public String getInfo() {
        return year + "年" + month + "月" + day + "日";
    }

    public Mydate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public Mydate() {
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}

class SalaryEmployee extends Employee {
    private double salary;
    private Mydate birthday;



    @java.lang.Override
    public double earning() {
        return salary;
    }

    public SalaryEmployee(String name, double salary, Mydate birthday) {
        super(name);
        this.salary = salary;
        this.birthday = birthday;
    }

    public SalaryEmployee() {
    }



    @java.lang.Override
    public String getInfo() {
        return super.getInfo()+"生日"+birthday.getInfo();
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Mydate getBirthday() {
        return birthday;
    }

    public void setBirthday(Mydate birthday) {
        this.birthday = birthday;
    }
}

class HourEmployee extends Employee {
    double wage;
    double hour;
    public HourEmployee(String name, double wage, double hour) {
        super(name);
        this.wage = wage;
        this.hour = hour;
    }

    public HourEmployee() {
    }



    @Override
    public double earning() {
        return wage * hour;
    }

    @Override
    public String getInfo() {
        return super.getInfo() + "时薪"+wage +" 时长"+ hour;
    }
}

class Manager extends SalaryEmployee {
    private double bonus;

    public Manager(String name, double salary, Mydate birthday, double bonus) {
        super(name, salary, birthday);
        this.bonus = bonus;
    }


    public Manager() {
    }

    @Override
    public double earning() {
        return getSalary() * (1 + bonus);
    }

    @Override
    public String getInfo() {
        return super.getInfo() +" 奖金比例" +bonus;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
}

class test {
    static double sumsal=0;
    static String empmon="";
    public static void main(String[] args) {
        ArrayList emp = new ArrayList<Employee>();
        emp.add(new SalaryEmployee("张三",2500.0,new Mydate(2001,1,1)));
        emp.add(new HourEmployee("李四",15.0,20.0));
        emp.add(new Manager("王五",5000.0,new Mydate(2003,1,1),0.3));
        System.out.println("输入月份");
        Scanner sc = new Scanner(System.in);
        int mont= sc.nextInt();

        for (Object o : emp) {
            Emp(o);
            emmont(o,mont);
        }
        System.out.println("实发工资总额"+sumsal);
        System.out.println("请 "+empmon+"领取生日礼物");
    }
    public static void Emp(Object o){
        if (o instanceof SalaryEmployee){
            SalaryEmployee sal= (SalaryEmployee) o;
            System.out.println(sal.getInfo());
            sumsal+=sal.earning();
        }else if (o instanceof HourEmployee){
            HourEmployee hou= (HourEmployee) o;
            System.out.println(hou.getInfo());
            sumsal+=hou.earning();
        }else if (o instanceof Manager){
            Manager man= (Manager) o;
            System.out.println(man.getInfo());
            sumsal+=man.earning();
        }
    }
    public static void emmont(Object o,int mont){
        if(o instanceof Manager || o instanceof SalaryEmployee){
            SalaryEmployee sal=(SalaryEmployee) o;
            if(sal.getBirthday().getMonth()==mont){
                empmon += (sal.getName()+" ");
            }
        }
    }
}
```

