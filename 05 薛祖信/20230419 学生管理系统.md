主体

```java
import java.util.ArrayList;
import java.util.Scanner;

public class Student {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        ArrayList<Studentsystem> list=new ArrayList<>();
        while (true) {
            System.out.println("-----欢迎进入学生信息管理系统-----");
            System.out.println("1.添加学生");
            System.out.println("2.删除学生");
            System.out.println("3.修改学生");
            System.out.println("4.查询学生");
            System.out.println("5.退出");
            System.out.println("请输入你的选择:");
            int number=sc.nextInt();
            switch (number){
                case 1:
                    add(list,sc);
                    break;
                case 2:
                    del(list,sc);
                    break;
                case 3:
                    update(list,sc);
                    break;
                case 4:
                    query(list);
                    break;
                case 5:
                    System.out.println("谢谢使用");
                    System.exit(0);
            }
        }
    }
    public static void add(ArrayList<Studentsystem> a,Scanner sc){
        System.out.println("请输入学生的学号");
        String id= sc.next();
        System.out.println("请输入学生的姓名");
        String name= sc.next();
        System.out.println("请输入学生的性别");
        String sex= sc.next();
        System.out.println("请输入学生的生日");
        String birthday= sc.next();
        Studentsystem s=new Studentsystem(id,name,sex,birthday);
        a.add(s);
        System.out.println();
    }
    public static void del(ArrayList<Studentsystem> a,Scanner sc){
        System.out.println("请输入学生学号");
        String id=sc.next();
        for (int i = 0; i < a.size(); i++) {
            Studentsystem s=a.get(i);
            if (s.getId().equals(id)){
                a.remove(i);
                break;
            }
        }
        System.out.println("删除成功");
        System.out.println();

    }
    public static void update(ArrayList<Studentsystem> a,Scanner sc){
        System.out.println("请输入要修改的学生学号");
        String stuid=sc.next();

        System.out.println("请输入学生新学号");
        String id= sc.next();
        System.out.println("请输入学生新姓名");
        String name= sc.next();
        System.out.println("请输入学生新性别");
        String sex= sc.next();
        System.out.println("请输入学生新生日");
        String birthday= sc.next();
        Studentsystem s=new Studentsystem(id,name,sex,birthday);
        for (int i = 0; i < a.size(); i++) {
            Studentsystem s1=a.get(i);
           if (s1.getId().equals(stuid)){
                a.set(i,s);
                break;
            }
        }
        System.out.println("修改完成");

    }
    public static void query(ArrayList<Studentsystem> a){
        if (a.size()==0){
            System.out.println("无信息,请添加数据后查询");
            return;
        }
        System.out.println("信息如下");
        System.out.println("学号"+"\t\t\t"+"姓名"+"\t\t\t"+"性别"+"\t\t\t"+"生日");
        for (Studentsystem s : a) {
            System.out.println(s);
        }
        System.out.println();
    }
}

```

类

```java
public class Studentsystem {
    private String id;
    private String name;
    private String sex;
    private String birthday;

    public Studentsystem() {
    }

    public Studentsystem(String id, String name, String sex, String birthday) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    public String toString(){
        return id+"\t\t\t"+name+"\t\t\t"+sex+"\t\t\t"+birthday;
    }
}

```

