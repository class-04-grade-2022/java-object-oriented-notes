```java
import java.util.Scanner;

public class Employee {
    private String name;

    public Employee(String name) {
        this.name = name;
    }
    public Employee(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public double earning(){
        return 0.0;
    }
    public String getInfo(){
        return "Mea";
    }
}
class MyDate{
    private int Year,Mouth,day;

    public MyDate(int year, int mouth, int day) {
        Year = year;
        Mouth = mouth;
        this.day = day;
    }
    public MyDate(){}

    public int getYear() {
        return Year;
    }

    public void setYear(int year) {
        Year = year;
    }

    public int getMouth() {
        return Mouth;
    }

    public void setMouth(int mouth) {
        Mouth = mouth;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
    public String getInfo(){
        return getYear()+"年"+getMouth()+"月"+getDay()+"日";
    }
}
class SalaryEmployee extends Employee{
    private double salary;
    private MyDate birthday;

    public SalaryEmployee(String name, double salary, MyDate birthday) {
        super(name);
        this.salary = salary;
        this.birthday = birthday;
    }

    public SalaryEmployee(double salary, MyDate birthday) {
        this.salary = salary;
        this.birthday = birthday;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public MyDate getBirthday() {
        return birthday;
    }

    public void setBirthday(MyDate birthday) {
        this.birthday = birthday;
    }

    public SalaryEmployee(){}

    @Override
    public double earning() {
        return  salary;
    }

    @Override
    public String getInfo() {
        return "姓名:"+getName()+" 实发工资:"+salary+" 生日:"+birthday.getInfo();
    }
}
class HourEmployee extends Employee{
    private double workingHours;
    private double hourlyWage;

    public HourEmployee(String name, double workingHours, double hourlyWage) {
        super(name);
        this.workingHours = workingHours;
        this.hourlyWage = hourlyWage;
    }

    public HourEmployee(double workingHours, double hourlyWage) {
        this.workingHours = workingHours;
        this.hourlyWage = hourlyWage;
    }
    public HourEmployee(){}

    public double getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(double workingHours) {
        this.workingHours = workingHours;
    }

    public double getHourlyWage() {
        return hourlyWage;
    }

    public void setHourlyWage(double hourlyWage) {
        this.hourlyWage = hourlyWage;
    }

    @Override
    public double earning() {
        double salary = workingHours*hourlyWage;
        return salary;
    }

    @Override
    public String getInfo() {
        double salary = workingHours*hourlyWage;
        return "姓名:"+getName()+" 实发工资:"+salary+" 时薪:"+hourlyWage+" 工作小时数:"+workingHours;
    }
}
class Manager extends SalaryEmployee{
    private double BonusRatio;

    public Manager(String name, double salary, MyDate birthday, double bonusRatio) {
        super(name, salary, birthday);
        BonusRatio = bonusRatio;
    }

    public Manager(double salary, MyDate birthday, double bonusRatio) {
        super(salary, birthday);
        BonusRatio = bonusRatio;
    }

    public Manager(double bonusRatio) {
        BonusRatio = bonusRatio;
    }
    public Manager(){}

    public double getBonusRatio() {
        return BonusRatio;
    }

    public void setBonusRatio(double bonusRatio) {
        BonusRatio = bonusRatio;
    }

    @Override
    public double earning() {
        double trueSalary = super.earning()*(1+getBonusRatio());
        return trueSalary;
    }

    @Override
    public String getInfo() {
        return "姓名:"+getName()+" 实发工资:"+earning()+" 生日:"+getBirthday().getInfo()+" 奖金比例:"+getBonusRatio();
    }
}
class test{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Employee[] emp = {new SalaryEmployee("ふわふわさん",114.514,new MyDate(1949,5,1)),
                          new HourEmployee("佐藤さん",372,7.5),
                          new Manager("佐田さん",191919,new MyDate(2077,6,4),44.5),
                          new SalaryEmployee("ビンラディンさん",3025,new MyDate(2001,9,11)),
                          new SalaryEmployee("ヒトラーさん",4640,new MyDate(1889,4,30)),
        };
        double salary = 0;
        System.out.println("请输入本月月份");
        int mouth = sc.nextInt();
        for (int i=0;i< emp.length;i++){
            System.out.println(emp[i].getInfo());
            salary += emp[i].earning();

            if (emp[i] instanceof SalaryEmployee || emp[i] instanceof Manager){
                if (((SalaryEmployee) emp[i]).getBirthday().getMouth() == mouth){
                    System.out.println(emp[i].getName()+"今月の誕生日 おめでとう!!!!!");
                }
            }
        }
        System.out.println("财务需准备金额："+salary);
    }
}
```

